<?php
if (!defined('ABSPATH')) exit();

/**
 *	Вывод в списках одного элемента - статьи.
 */

$img_url = get_the_post_thumbnail_url( get_the_ID(), 'catalog-thumb' );
$img_url = ($img_url) ? $img_url : wp_get_attachment_image_url(carbon_get_theme_option('kedrm_default_catalog_default_thumb'), 'catalog-thumb');
$alt = get_post_meta(get_the_ID(), '_wp_attachment_image_alt', true);
?>

<li class="knowledge__item">
	<article class="knowledge__article article-knowledge">
		<a href="<?php echo get_the_permalink() ?>" class="article-knowledge__link"></a>
		<div class="article-knowledge__image-wrapper">
			<img src="<?php echo $img_url ?>" alt="<?php echo $alt ?>" class="article-knowledge__image">
		</div>
		<h3 class="article-knowledge__title"><?php echo get_the_title() ?></h3>
		<p class="article-knowledge__text"><?php echo get_the_excerpt() ?></p>
	</article>
</li>
