<?php
if (!defined('ABSPATH')) exit();

$id = $args[0];

$rel_product = wc_get_product($args[0]);

if ($rel_product):


	$is_not_available = get_post_meta($id, '_stock_status', true) == 'outofstock';
	$is_sale_class = ($rel_product->is_on_sale()) ? ' product-body--sale' : '';
	$is_available_class = ( $is_not_available ) ? ' product-body--no' : '';
	$add_to_cart_text = ( $is_not_available ) ? 'Сообщить о появлении' : 'Купить';

	$img_url = get_the_post_thumbnail_url( $id, 'catalog-thumb' );
	$img_url = ($img_url) ? $img_url : wp_get_attachment_image_url(carbon_get_theme_option('kedrm_default_catalog_default_thumb'), 'catalog-thumb');
	$alt = get_post_meta($id, '_wp_attachment_image_alt', true);

	?>

	<li class="catalog__item swiper-slide">
		<article class="catalog__product product-body <?php echo $is_available_class; echo $is_sale_class;?>">
			<div class="product-body__image-wrapper">
				<a href="<?php the_permalink($id) ?>">
					<img src="<?php echo $img_url; ?>" alt="<?php echo $alt;?>" class="product-body__image">
				</a>
			</div>
			<div class="product-body__column">
				<?php
					include( get_template_directory() . '/woocommerce/includes/parts/wc-rating-stars.php');
				?>
				<h3 class="product-body__title">
					<a href="<?php the_permalink($id) ?>"><?php echo get_the_title($id);?></a>
				</h3>
				<p class="product-body__text"><?php echo get_the_excerpt($id);?></p>
				<div class="product-body__buy-wrapper">
					<span class="product-body__price"><?php echo $rel_product->get_price() . get_woocommerce_currency_symbol() ?></span>

					<?php if (!$is_not_available):?>

						<a href="?add-to-cart=<?php echo $id ?>" data-quantity="1" class="button product_type_simple add_to_cart_button ajax_add_to_cart product-body__buy" data-product_id="<?php echo $id?>" data-product_sku="<?php echo $rel_product->get_sku()?>" aria-label="" rel="nofollow"><?php echo $add_to_cart_text;?></a>

					<?php else:?>

						<div class="xoo-wl-btn-container xoo-wl-btc-simple xoo-wl-btc-popup ">
							<button type="button" data-product_id="<?php echo $id?>" class="xoo-wl-action-btn xoo-wl-open-form-btn button btn xoo-wl-btn-popup product-body__buy"><?php echo $add_to_cart_text;?></button>
						</div>

					<?php endif; ?>
				</div>
			</div>
		</article>
	</li>
<?php endif;?>