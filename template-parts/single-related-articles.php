<?php
if (!defined('ABSPATH')) exit;

global $product;

$search_value = 'post:product:' . $product->get_id();;

// Получаем список статей, в которых есть ссылка на данный товар
$query = new WP_Query([
	'post_type' 		=> 'kedrm_article',
	'status'				=> 'publish',
	'orderby'		 	=> 'rand',
	'posts_per_page' 	=> '4',
	'meta_query' 		=> [
		[	'key' => 'kedrm_article_products',
			'value' => $search_value,
		],
	]
]);

if ($query->have_posts()):?>
	<section class="page-card-product__knowledge knowledge">
		<div class="container">
			<h2 class="knowledge__title knowledge__title">Статьи о товаре</h2>
			<div class="knowledge__slider-container swiper-container knowledge-slider">
				<ul class="knowledge__list swiper-wrapper">

<?php
	while($query->have_posts()):
		$query->the_post();

		$img_url = get_the_post_thumbnail_url(get_the_ID(), 'catalog-thumb');
		$img_url = ($img_url) ? $img_url : wp_get_attachment_image_url(carbon_get_theme_option('kedrm_default_catalog_default_thumb'), 'catalog-thumb');
		$alt = get_post_meta(get_the_ID(), '_wp_attachment_image_alt', true);
		?>

		<li class="knowledge__item swiper-slide">
			<article class="knowledge__article article-knowledge">
				<a href="<?php the_permalink();?>" class="article-knowledge__link"></a>
				<div class="article-knowledge__image-wrapper">
					<img src="<?php echo $img_url;?>" alt="<?php echo $alt;?>" class="article-knowledge__image">
				</div>
				<h3 class="article-knowledge__title"><?php echo get_the_title(); ?></h3>
				<p class="article-knowledge__text">
					<?php echo get_the_excerpt(); ?>
				</p>
			</article>
		</li>

	<?php
	endwhile;?>
				</ul>
				<div class="knowledge__slider-buttons-container">
				</div>
			</div>
		</div>
	</section>
<?php
endif;

wp_reset_query();

?>


