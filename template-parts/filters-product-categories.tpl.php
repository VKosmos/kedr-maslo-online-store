<?php
if (!defined('ABSPATH')) exit;

// $cat_id = -1;
// if (is_shop()){
// 	$cat_id = (isset($_GET['cats'])) ? (int)(sanitize_text_field($_GET['cats'])) : -1;
// }

// $cat_id = (isset($_GET['cats'])) ? (int)(sanitize_text_field($_GET['cats'])) : -1;

$category = get_queried_object();
$cat_id = (isset($category->term_id)) ? (int)$category->term_id : -1;

$args = [
	'taxonomy'		=> 'product_cat',
	'hide_empty' 	=> 'false',
	'exclude'		=> 15,
];
$terms = get_terms( $args );
if ($terms):?>
	<ul class="filters__list">
	<?php foreach ( $terms as $v ):

		$is_selected_class = '';
		if ($v->term_id === $cat_id) {
			$is_selected_class = ' _selected';
		}
		?>

		<li class="filters__item">
			<a href="<?php echo get_category_link( $v->term_id );?>" class="<?php echo $is_selected_class; ?>" data-cat-id="<?php echo $v->term_id;?>">
				<span class="filters__check"></span>
				<?php echo $v->name;?>
			</a>
			<span class="filters__quantity"><?php echo $v->count;?></span>
		</li>

	<?php endforeach;?>
	</ul>

	<div class="filters__scroll-list scroll-list swiper-container">
		<ul class="scroll-list__list swiper-wrapper">

	<?php foreach ($terms as $v):

		$is_selected_class = '';
		if ($v->term_id === $cat_id) {
			$is_selected_class = ' _selected';
		}
		?>

		<li class="scroll-list__item swiper-slide">
			<a class="scroll-list__category-link<?php echo $is_selected_class; ?>" href="<?php echo get_category_link( $v->term_id );?>" data-cat-id="<?php echo $v->term_id;?>"></a>
			<div class="scroll-list__image-wrapper">
				<?php
					echo carbon_get_term_meta( $v->term_id, 'kedrm_category_svg' );
				?>
			</div>
			<h3 class="scroll-list__title"><?php echo $v->name;?></h3>
		</li>

	<?php endforeach;?>

		</ul>
		<div class="scroll-list__pagination swiper-pagination"></div>
	</div>

<?php endif;?>