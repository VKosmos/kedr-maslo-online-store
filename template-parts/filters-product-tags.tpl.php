<?php
if (!defined('ABSPATH')) exit;

$tagsStr = (isset($_GET['tags'])) ? (sanitize_text_field( $_GET['tags'] )) : '';
if ('all' === $tagsStr) {
	$tags = 'all';
} else {
	$tags = explode(',', $tagsStr);
}


$args = [
	'taxonomy'		=> 'product_tag',
	'hide_empty'	=> 'false',
];
$terms = get_terms($args);?>

<?php if ($terms):?>
	<div class="filters__filter">
		<h3 class="filters__headline">Теги</h3>

		<button type="button" class="filters__selector">
			Теги
			<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
				<path d="M18 9L12 15L6 9" stroke="#212121" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
			</svg>
		</button>

		<div class="filters__body filters__body--tags">
			<div class="filters__search-wrapper">
				<input type="text" class="filters__search" placeholder="Поиск по тегам" aria-label="Поиск по тегам">
			</div>
			<ul class="filters__list filters__list--tags filters__tags">

	<?php foreach ( $terms as $v ):

		$is_selected_class = '';
		if ('all' === $tags) {
			$is_selected_class = ' _selected';
		} else {
			if (in_array($v->term_id, $tags)) {
				$is_selected_class = ' _selected';
			}
		}

		?>

		<li class="filters__item">
			<input type="button" class="filters__tag<?php echo $is_selected_class;?>" data-tag-id="<?php echo $v->term_id;?>" value="<?php echo $v->name; ?>"></input>
		</li>

	<?php endforeach;?>

			</ul>
		</div>
	</div>
<?php endif;
