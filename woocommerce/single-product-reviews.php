<?php
/**
 * Display single product reviews (comments)
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product-reviews.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 4.3.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

if ( ! comments_open() ) {
	return;
}

?>

<section class="page-card-product__reviews reviews">
	<div class="container">

		<h3 data-target="single-reviews" class="reviews__title">Отзывы и оценки</h3>

		<?php if ( get_option( 'woocommerce_review_rating_verification_required' ) === 'no' || wc_customer_bought_product( '', get_current_user_id(), $product->get_id() ) ) : ?>
		<div id="review_form_wrapper">
			<div id="review_form">
				<?php
				$commenter    = wp_get_current_commenter();

				$commenter_class =  (is_user_logged_in()) ? ' logged-commenter' : '';

				$comment_form = array(
					'comment_notes_after' 	=> '',
					'label_submit'        	=> 'Оставить свой комментарий',
					'class_submit'				=> 'form-reviews__submit',
					'submit_field'				=> '</div><div class="form-reviews__text-wrapper form-reviews__text-wrapper--submit">%1$s %2$s</div>',
					'logged_in_as'        	=> '',
					'comment_field'       	=> '',
					'class_form'				=> 'reviews__form form-reviews' . $commenter_class,
					'title_reply'				=> '',
				);

				$name_email_required = (bool) get_option( 'require_name_email', 1 );

				$fields              = array(
					'author' => array(
						'type'     		=> 'text',
						'value'    		=> $commenter['comment_author'],
						'required' 		=> $name_email_required,
						'placeholder' 	=> 'Имя',
						'aria'			=> 'Ваше Имя',
					),
					'email'  => array(
						'type'     		=> 'email',
						'value'    		=> $commenter['comment_author_email'],
						'required' 		=> $name_email_required,
						'placeholder' 	=> 'Е-мейл',
						'aria'			=> 'Адрес электронной почты',
					),
				);

				$comment_form['fields'] = array();

				foreach ( $fields as $key => $field ) {
					$field_html = '';
					if ('author' == $key) {
						$field_html = '<div class="form-reviews__input-wrapper"><p class="form-reviews__text">Оставь свой комментарий:</p>';
					}

					$field_html .= '<input id="' . esc_attr( $key ) . '" class="form-reviews__input"' . '" name="' . esc_attr( $key ) . '" type="' . esc_attr( $field['type'] ) . '" value="' . esc_attr( $field['value'] ) . '" ' . ( $field['required'] ? 'required' : '' ) . ' aria-label="' . esc_attr($field['aria']) . '" placeholder="' . esc_attr($field['placeholder']) . '" />';

					if ('email' == $key) {
						$field_html .= '</div>';
					}

					$comment_form['fields'][ $key ] = $field_html;
				}

				$account_page_url = wc_get_page_permalink( 'myaccount' );
				if ( $account_page_url ) {
					/* translators: %s opening and closing link tags respectively */
					$comment_form['must_log_in'] = '<p class="must-log-in">' . esc_html__( 'You must be %1$slogged in%2$s to post a review.', 'woocommerce' ) . '</p>';
				}

				if ( wc_review_ratings_enabled() ) {

					$comment_form['comment_field'] = '
					<div class="form-reviews__stars-wrapper">
						<p class="form-reviews__text">Твоя оценка:</p>
						<div class="card-product__testimonials testimonials testimonials--no-margin">
							<select name="rating" id="rating" required>
								<option value="">' . esc_html__( 'Rate&hellip;', 'woocommerce' ) . '</option>
								<option value="5">' . esc_html__( 'Perfect', 'woocommerce' ) . '</option>
								<option value="4">' . esc_html__( 'Good', 'woocommerce' ) . '</option>
								<option value="3">' . esc_html__( 'Average', 'woocommerce' ) . '</option>
								<option value="2">' . esc_html__( 'Not that bad', 'woocommerce' ) . '</option>
								<option value="1">' . esc_html__( 'Very poor', 'woocommerce' ) . '</option>
							</select>
							<div class="testimonials__bg">
								<span class="testimonials__text _comment-rating-number">0</span>
							</div>
						</div>
					</div>';
				}

				$comment_form['comment_field'] .= '<div class="form-reviews__text-wrapper"><textarea id="comment" name="comment"  class="form-reviews__textarea" placeholder="Твой комментарий" aria-label="Ваш комментарий" required></textarea></div>';

				// comment_form( apply_filters( 'woocommerce_product_review_comment_form_args', $comment_form ) );
				kedrm_single_comment_form( apply_filters( 'woocommerce_product_review_comment_form_args', $comment_form ) );
				?>
				<button class="form-reviews__mobile-open" type="submit" data-vkpath="comment">Оставить свой комментарий</button>
			</div>
		</div>
	<?php else : ?>
		<p class="woocommerce-verification-required"><?php esc_html_e( 'Only logged in customers who have purchased this product may leave a review.', 'woocommerce' ); ?></p>
	<?php endif; ?>
	</div>
</section>

<section class="page-card-product__comment comment">
	<div class="container">
		<div class="comment__flex-wrapper">
			<div class="comment__flex-testimonials">

			<?php

				if ( wc_review_ratings_enabled() ) {
					global $product;

					$rating_count = $product->get_rating_count();
					$review_count = $product->get_review_count();
					$average      = $product->get_average_rating();

					if ( $rating_count > 0 ) : ?>

						<div class="woocommerce-product-rating">
							<?php
								echo wc_get_rating_html( $average, $rating_count );
								?>
								<span class="count testimonials__text testimonials__text--dark"><?php echo number_format($average, 1, '.', ''); ?></span>
						</div>

					<?php endif;
				}

			?>

				<?php
					if (have_comments()):

						$comments = get_comments([
							'post_id' => $product->get_id(),
							'status'	=> 'approve',
						]);

						$comments_count = count($comments);
						if ($comments_count < 0):

							$rates = [];
							foreach($comments as $v):
								$rates[] = intval( get_comment_meta( $v->comment_ID, 'rating', true ) );
							endforeach;

							$w_rates = [];
							foreach ($rates as $v) {
								if (isset($w_rates[$v])) {
									$w_rates[$v] = $w_rates[$v] + 1;
								} else {
									$w_rates[$v] = 1;
								}
							}
					?>

							<div class="comment__line-wrapper">
							<?php
								$i = 5;
								while ( $i >= 1 ):
									$cnt = (isset($w_rates[$i])) ? $w_rates[$i] : 0;
									$percentage = round($cnt / $comments_count, 2) * 100;

									$str =
									sprintf(
										_n('%s customer review', '%s customer reviews', $cnt, 'woocommerce' ),
										'<span class="count testimonials__text">' . esc_html( $cnt ) . '</span>&nbsp;'
									);
									$str = preg_replace('=\s\S+$=', '', $str);
								?>

									<div class="comment__line">
										<span class="comment__rating"><?php echo number_format($i, 1, '.', '') ?></span>
										<span class="comment__line-grey"><span style="width: <?= $percentage; ?>%;"></span></span>
										<span class="comment__reviews"><?php echo $str; ?></span>
									</div>

								<?php
									$i--;
								endwhile;?>
							</div>

				<?php
						endif;
					endif;
				?>

			</div><!-- .comment__flex-testimonials -->

			<div class="comment__flex-reviews">

				<?php if ( have_comments() ) : ?>

					<ul class="comment__list aa2">

						<?php
						wp_list_comments( apply_filters( 'woocommerce_product_review_list_args', array(
							'callback' => 'woocommerce_comments',
							'per_page' => 12,
							'status'	=> 'approve') )
						);
						?>
					</ul>

					<?php

					if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) :
						echo '<nav class="woocommerce-pagination">';
						paginate_comments_links(
							apply_filters(
								'woocommerce_comment_pagination_args',
								array(
									'prev_text' => is_rtl() ? '&rarr;' : '&larr;',
									'next_text' => is_rtl() ? '&larr;' : '&rarr;',
									'type'      => 'list',
								)
							)
						);
						echo '</nav>';
					endif;
					?>

				<?php /* else: ?>
					<p class="woocommerce-noreviews testimonials__text"><?php esc_html_e( 'There are no reviews yet.', 'woocommerce' ); ?></p>
				<?php */endif; ?>

			</div>
		</div>



	</div>
</section>


