<?php
if (!defined('ABSPATH')) exit;

// add_action('woocommerce_sidebar', 'kedrm_add_sidebar_only_archives' );
// function kedrm_add_sidebar_only_archives()
// {
// 	if (!is_product()){
// 		woocommerce_get_sidebar( );
// 	}
// }

remove_action( 'woocommerce_archive_description', 'woocommerce_taxonomy_archive_description', 10 );
remove_action( 'woocommerce_archive_description', 'woocommerce_product_archive_description', 10 );


// Отключаем вывод сообщений, чтобы перенести его внутрь блока основного содержания позже
remove_action( 'woocommerce_before_shop_loop', 'woocommerce_output_all_notices', 10 );
add_action( 'woocommerce_before_shop_loop', 'kedrm_woocommerce_output_all_notice', 30 );
function kedrm_woocommerce_output_all_notice()
{
	echo '<div class="woocommerce-notices-wrapper">';
	wc_print_notices();
	echo '</div>';
}

// Убираем Количество товаров
remove_action( 'woocommerce_before_shop_loop', 'woocommerce_result_count', 20 );

// Убираем Сортировку
remove_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30 );


/**
 * Выводим обёртку всего контента и блока фильтров
 */
add_action( 'woocommerce_before_shop_loop', 'kedrm_catalog_before_content_wrapper', 10 );
function kedrm_catalog_before_content_wrapper()
{?>
	<div class="products__main-wrapper">
		<div class="products__filters filters">
			<p class="visually-hidden">Фильтр товаров</h2>
<?php }

/**
 * Выводим все фильтры
 */
add_action( 'woocommerce_before_shop_loop', 'kedrm_catalog_before_content_filters', 20 );
function kedrm_catalog_before_content_filters()
{?>

		<div class="filters__sticky-container">

			<div class="filters__filter">
				<h3 class="filters__headline">Категории</h3>
				<div class="filters__body">

					<?php
						get_template_part( 'template-parts/filters-product-categories.tpl' );
					?>

					<?php
						// kedrm_filters_menu('catalog');
					?>

				</div>
			</div>

			<?php get_template_part( 'template-parts/filters-product-tags.tpl' ); ?>

		</div>
	</div><!-- .products__filters -->
	<div class="products__catalog">

<?php }


/**
 * Выводим завершение обёртки всего контента
 */
add_action( 'woocommerce_after_main_content', 'kedrm_catalog_after_content_wrapper', 20 );
function kedrm_catalog_after_content_wrapper()
{
	if (is_shop()):?>
			<div class="description-products">
				<h2 class="description-products__title"><?php echo carbon_get_post_meta(7, 'kedrm_shop_bottom_title') ?></h2>
				<div class="description-products__text"><?php echo carbon_get_post_meta(7, 'kedrm_shop_bottom_text') ?></div>
			</div>

		</div><!-- .products__catalog -->
	</div><!-- .products__main-wrapper -->
<?php endif;
}


/**
 * Opening of products loop
 */
function kedrm_woocommerce_product_loop_start()
{?>

	<section class="catalog">
		<h2 class="visually-hidden">Каталог товаров</h2>

		<div class="catalog__all-container">
			<div class="catalog__switcher-wrapper">
				<button class="catalog__switcher catalog__switcher--horizontal" data-catalog="horizontal">
					<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
						<path fill-rule="evenodd" clip-rule="evenodd"
							d="M20 3C20.5523 3 21 3.44772 21 4L21 10C21 10.5523 20.5523 11 20 11L4 11C3.44772 11 3 10.5523 3 10L3 4C3 3.44771 3.44772 3 4 3L20 3ZM19 5L5 5L5 9L19 9L19 5Z"
							fill="#C4C4C4" />
						<path fill-rule="evenodd" clip-rule="evenodd"
							d="M20 13C20.5523 13 21 13.4477 21 14L21 20C21 20.5523 20.5523 21 20 21L4 21C3.44772 21 3 20.5523 3 20L3 14C3 13.4477 3.44772 13 4 13L20 13ZM19 15L5 15L5 19L19 19L19 15Z"
							fill="#C4C4C4" />
					</svg>
				</button>
				<button class="catalog__switcher catalog__switcher--tile js-active" data-catalog="vertical">
					<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
						<path fill-rule="evenodd" clip-rule="evenodd"
							d="M3 4C3 3.44772 3.44772 3 4 3L10 3C10.5523 3 11 3.44772 11 4L11 20C11 20.5523 10.5523 21 10 21H4C3.44772 21 3 20.5523 3 20L3 4ZM5 5L5 19H9L9 5L5 5Z"
							fill="#C4C4C4" />
						<path fill-rule="evenodd" clip-rule="evenodd"
							d="M13 4C13 3.44772 13.4477 3 14 3L20 3C20.5523 3 21 3.44772 21 4L21 20C21 20.5523 20.5523 21 20 21H14C13.4477 21 13 20.5523 13 20L13 4ZM15 5L15 19H19L19 5L15 5Z"
							fill="#C4C4C4" />
					</svg>
				</button>
			</div>
		</div>

		<ul class="catalog__list _no-slider">

<?php
}

/**
 * Closing of products loop
 */
function kedrm_woocommerce_product_loop_end()
{?>
		</ul>
	</section>
<?php
}

/**
 * Products list item
 */
add_action('woocommerce_shop_loop', 'kedrm_woocommerce_products_loop_item', 10);
function kedrm_woocommerce_products_loop_item()
{
	get_template_part( 'template-parts/archive-products-single.tpl' );
}

/**
 * Pagination
 */
remove_action( 'woocommerce_after_shop_loop', 'woocommerce_pagination', 10 );
add_action('woocommerce_after_shop_loop', 'kedrm_woocommerce_shop_pagination', 10 );
function kedrm_woocommerce_shop_pagination()
{?>

	<div class="pagination">
		<div class="container">
			<div class="pagination__flex-container">

				<?php
					global $wp_query;

					$paged = get_query_var('paged') ? get_query_var('paged') : 1;
					$max_pages = $wp_query->max_num_pages;

					if ($paged < $max_pages):?>

						<div id="loadmore">
							<a href="#" class="pagination__show-more" data-maxpages="<?php echo $max_pages ?>" data-paged="<?php echo $paged; ?>">Показать ещё товары</a>
						</div>

					<?php endif;?>

				<div class="pagination__pagination-wrapper">
					<?php echo kedrm_shop_pagination2($paged, $max_pages); ?>
				</div>

			</div>
		</div>
	</div>

<?php
}

remove_action( 'woocommerce_no_products_found', 'wc_no_products_found', 10 );