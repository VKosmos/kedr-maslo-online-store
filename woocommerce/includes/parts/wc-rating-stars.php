<?php
if (!defined('ABSPATH')) exit;

if ( ! wc_review_ratings_enabled() ) {
	return;
}

// $rel_product ДОлжен быть заранее задан в подулючающем файле

$rating_count = $rel_product->get_rating_count();
$review_count = $rel_product->get_review_count();
$average      = $rel_product->get_average_rating();

if ( $rating_count > 0 ) : ?>

	<div class="woocommerce-product-rating">

		<?php echo wc_get_rating_html( $average, $rating_count ); // WPCS: XSS ok. ?>

		<?php if ( comments_open() ) : ?>
			<?php //phpcs:disable ?>
				<?php
				$str =
					sprintf(
						_n('%s customer review', '%s customer reviews', $review_count, 'woocommerce' ),
						'<span><span class="count testimonials__text">' . esc_html( $review_count ) . '</span>&nbsp;'
					);
				echo preg_replace('=\s\S+$=', '', $str) . '</span>';
				?>
			<?php // phpcs:enable ?>
		<?php endif ?>
	</div>

<?php endif; ?>
