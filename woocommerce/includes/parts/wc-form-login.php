<?php
if (!defined('ABSPATH')) exit;
?>

<?php wc_print_notices(); ?>

<form id="logform" class="woocommerce-form woocommerce-form-login login  login-modal__form form-login" method="post">
	<h3 class="form-login__headline">Вход</h3>

		<p class="status"></p>

		<label class="form-login__label">
			<span>Электронная почта:</span>
			<input type="text" class="woocommerce-Input woocommerce-input--text input-text form-login__input" name="username" id="username" autocomplete="username" value="<?php echo ( ! empty( $_POST['username'] ) ) ? esc_attr( wp_unslash( $_POST['username'] ) ) : ''; ?>" />
		</label>

		<label class="form-login__label">
			<span>Введите свой пароль:</span>

			<input class="woocommerce-Input woocommerce-Input--text input-text  form-login__input" type="password" name="password" id="password" autocomplete="current-password" />
		</label>

		<div class="woocommerce-LostPassword lost_password  form-login__recovery-wrapper">
			<a href="<?php echo esc_url( wp_lostpassword_url() ); ?>" class="form-login__recovery">Восстановить пароль</a>
		</div>

		<?php wp_nonce_field( 'woocommerce-login', 'woocommerce-login-nonce' ); ?>

		<button type="submit" class="woocommerce-button button woocommerce-form-login__submit  form-login__submit-button" name="login" value="Войти">Войти</button>

		<a class="form-login__register" data-vkpath="register">Зарегистрироваться</a>

		<?php
		/*
		<label class="woocommerce-form__label woocommerce-form__label-for-checkbox woocommerce-form-login__rememberme">
			<input class="woocommerce-form__input woocommerce-form__input-checkbox" name="rememberme" type="checkbox" id="rememberme" value="forever" /> <span><?php esc_html_e( 'Remember me', 'woocommerce' ); ?></span>
		</label>
		*/
		?>

</form>


<?php do_action( 'woocommerce_after_customer_login_form' ); ?>