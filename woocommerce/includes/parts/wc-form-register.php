<?php
if (!defined('ABSPATH')) exit;
?>

<?php wc_print_notices(); ?>

<form id="regform" method="post" class="woocommerce-form woocommerce-form-register register" <?php do_action( 'woocommerce_register_form_tag' ); ?> >

<h3 class="form-register__headline">Регистрация</h3>

	<p class="status"></p>


	<?php
		/*
		if ( 'no' === get_option( 'woocommerce_registration_generate_username' ) ) :
	?>

		<label for="reg_username"><?php esc_html_e( 'Username', 'woocommerce' ); ?>&nbsp;<span class="required">*</span></label>
		<input type="text" class="woocommerce-Input woocommerce-Input--text input-text" name="username" id="reg_username" autocomplete="username" value="<?php echo ( ! empty( $_POST['username'] ) ) ? esc_attr( wp_unslash( $_POST['username'] ) ) : ''; ?>" />

	<?php
		endif;
		*/
	?>

	<label class="form-register__label">
		<span>Электронная почта:</span>
		<input type="email" class="woocommerce-Input woocommerce-Input--text input-text  form-register__input" name="email" id="reg_email" autocomplete="email" value="<?php echo ( ! empty( $_POST['email'] ) ) ? esc_attr( wp_unslash( $_POST['email'] ) ) : ''; ?>" >
	</label>

	<label class="form-register__label">
		<span>Пароль (минимум 8 символов):</span>
		<input type="password" class="woocommerce-Input woocommerce-Input--text input-text form-register__input" name="password" id="reg_password" autocomplete="new-password" />
	</label>

	<label class="form-register__label">
		<span>Подтвердите пароль:</span>
		<input type="password" class="form-register__input" name="password2">
	</label>

	<?php wp_nonce_field( 'woocommerce-register', 'woocommerce-register-nonce' ); ?>

	<button type="submit" class="woocommerce-Button woocommerce-button button woocommerce-form-register__submit  form-register__submit-button" name="register" value="Зарегистрироваться">Зарегистрироваться</button>

	<a href="#" class="form-register__register" data-vkpath="login">Уже зарегистрирован?</a>

	<p class="form-register__agreement">
		Регистрируясь, вы соглашаетесь с <a href="#">пользовательским соглашением</a>
	</p>

</form>
