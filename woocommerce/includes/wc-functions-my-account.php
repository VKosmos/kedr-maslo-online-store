<?php
if (!defined('ABSPATH')) exit;

add_action('woocommerce_before_account_navigation', 'kedrm_woocommerce_myaccount_wrapper_start', 10);
function kedrm_woocommerce_myaccount_wrapper_start()
{?>
	<div class="standard__flex-container">
	<?php
}

add_action('woocommerce_account_content', 'kedrm_woocommerce_myaccount_wrapper_end', 20);
function kedrm_woocommerce_myaccount_wrapper_end()
{?>
	</div>
<?php
}

/**
 *	Removing Downloads account menu item
 */
add_filter('woocommerce_account_menu_items', 'kedrm_woocommerce_account_menu_items', 10, 1);
function kedrm_woocommerce_account_menu_items($items)
{
	unset($items['downloads']);
	return $items;
}