<?php
if (!defined('ABSPATH')) exit;




/**
 * Sample implementation of the WooCommerce Mini Cart.
 *
 * You can add the WooCommerce Mini Cart to header.php like so ...
 *
 */

add_filter( 'woocommerce_add_to_cart_fragments', 'kedrm_woocommerce_cart_link_fragment' );
if ( ! function_exists( 'kedrm_woocommerce_cart_link_fragment' ) ) {
	/**
	 * Cart Fragments.
	 *
	 * Ensure cart contents update when products are added to the cart via AJAX.
	 *
	 * @param array $fragments Fragments to refresh via AJAX.
	 * @return array Fragments to refresh via AJAX.
	 */
	function kedrm_woocommerce_cart_link_fragment( $fragments ) {
		ob_start();
		kedrm_woocommerce_cart_link();
		$fragments['a.cart-contents'] = ob_get_clean();

		return $fragments;
	}
}

if ( ! function_exists( 'kedrm_woocommerce_cart_link' ) ) {
	/**
	 * Cart Link.
	 *
	 * Displayed a link to the cart including the number of items present and the cart total.
	 *
	 * @return void
	 */
	function kedrm_woocommerce_cart_link() {
		$class = (WC()->cart->get_cart_contents_count()) ? ' filled' : '';
		?>
		<a class="cart-contents header__user-nav-cart<?php echo $class?> xoo-wsc-cart-trigger" href="" title="<?php esc_attr_e( 'View your shopping cart', 'kedrm' ); ?>">
			<svg width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
				<path fill-rule="evenodd" clip-rule="evenodd" d="M1.5 2.25C1.5 1.83579 1.83579 1.5 2.25 1.5H3.75C4.16421 1.5 4.5 1.83579 4.5 2.25V11.25H13.692L15.492 5.25H6.75C6.33579 5.25 6 4.91421 6 4.5C6 4.08579 6.33579 3.75 6.75 3.75H16.5C16.7371 3.75 16.9602 3.86209 17.1017 4.0523C17.2432 4.24251 17.2865 4.48843 17.2184 4.71551L14.9684 12.2155C14.8732 12.5327 14.5812 12.75 14.25 12.75H3.75C3.33579 12.75 3 12.4142 3 12V3H2.25C1.83579 3 1.5 2.66421 1.5 2.25Z" fill="white"></path>
				<path fill-rule="evenodd" clip-rule="evenodd" d="M3 15.75C3 14.9216 3.67157 14.25 4.5 14.25C5.32843 14.25 6 14.9216 6 15.75C6 16.5784 5.32843 17.25 4.5 17.25C3.67157 17.25 3 16.5784 3 15.75Z" fill="white"></path>
				<path fill-rule="evenodd" clip-rule="evenodd" d="M12 15.75C12 14.9216 12.6716 14.25 13.5 14.25C14.3284 14.25 15 14.9216 15 15.75C15 16.5784 14.3284 17.25 13.5 17.25C12.6716 17.25 12 16.5784 12 15.75Z" fill="white"></path>
			</svg>
		</a>
		<?php
	}
}

if ( ! function_exists( 'kedrm_woocommerce_header_cart' ) ) {
	/**
	 * Display Header Cart.
	 *
	 * @return void
	 */
	function kedrm_woocommerce_header_cart() {
		if ( is_cart() ) {
			$class = 'current-menu-item';
		} else {
			$class = '';
		}
		?>
		<ul id="site-header-cart" class="site-header-cart">
			<li class="<?php echo esc_attr( $class ); ?>">
				<?php kedrm_woocommerce_cart_link(); ?>
			</li>
			<li>
				<?php
				$instance = array(
					'title' => '',
				);

				the_widget( 'WC_Widget_Cart', $instance );
				?>
			</li>
		</ul>
		<?php
	}
}


/*
	function kedrm_woocommerce_cart_link() {
		?>
		<a class="cart-contents" href="<?php echo esc_url( wc_get_cart_url() ); ?>" title="<?php esc_attr_e( 'View your shopping cart', 'kedrm' ); ?>">
			<?php
			$item_count_text = sprintf(
				_n( '%d item', '%d items', WC()->cart->get_cart_contents_count(), 'kedrm' ),
				WC()->cart->get_cart_contents_count()
			);
			?>
			<span class="amount"><?php echo wp_kses_data( WC()->cart->get_cart_subtotal() ); ?></span> <span class="count"><?php echo esc_html( $item_count_text ); ?></span>
		</a>
		<?php
	}
*/

/**
 * Убираем выбор способа доставки из корзины
 */
add_filter( 'woocommerce_cart_ready_to_calc_shipping', 'kedrm_disable_shipping_calc_on_cart', 99 );
function kedrm_disable_shipping_calc_on_cart( $show_shipping ) {
	if( is_cart() ) {
		 return false;
	}
	return $show_shipping;
}
