<?php
if (!defined('ABSPATH')) exit;



// Якорные ссылки
add_action('woocommerce_before_single_product_summary', 'kedrm_woocommerce_single_anchor_links', 3);
function kedrm_woocommerce_single_anchor_links()
{
	global $product;
	$review_count = $product->get_review_count();
	$review_count = ($review_count) ? strval($review_count) : '0';

	?>
	<div class="card-product__category">
		<div class="card-product__category-button card-product__category-button--active">
			<a href="#">Все о товаре</a>
		</div>
		<div class="card-product__category-button">
			<a href="#" data-anchor="single-description">Описание</a>
		</div>
		<div class="card-product__category-button">
			<a href="#" data-anchor="single-reviews">Отзывы (<?php echo $review_count;  ?>)</a>
		</div>
	</div>
	<div class="card-product__flex-wrapper">
<?php
}

// Убираем цену, перенесём её ниже
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_price', 10);

// Убираем краткое описание, т.к. не используется
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20);

add_action('woocommerce_single_product_summary', 'kedrm_woocommerce_single_attributes', 15);
function kedrm_woocommerce_single_attributes()
{
	global $product;

	$model = carbon_get_post_meta($product->get_id(), 'kedrm_product_model');
	$model = ($model) ? $model : '';

	$composition = carbon_get_post_meta($product->get_id(), 'kedrm_product_composition');
	$composition = ($composition) ? $composition : '';

	$length = $product->get_length();
	$width = $product->get_width();
	$height = $product->get_height();

	$weight = $product->get_weight();
	$weight = (strlen($weight) > 3) ? mb_substr( $weight, 0, strlen($weight) - 3 ) : $weight;

	$dimensions = '';
	if ($length) {
		$dimensions .= $product->get_length() .  __(esc_attr( get_option( 'woocommerce_dimension_unit' )), 'woocommerce');
	}

	if ($width) {
		if ($length) {
			$dimensions .= ' x ' . $product->get_width() .  __(esc_attr( get_option( 'woocommerce_dimension_unit' )), 'woocommerce');
		} else {
			$dimensions .= $product->get_width() .  __(esc_attr( get_option( 'woocommerce_dimension_unit' )), 'woocommerce');
		}
	}

	if ($height) {
		if ($length || $width) {
			$dimensions .= ' x ' . $product->get_height() .  __(esc_attr( get_option( 'woocommerce_dimension_unit' )), 'woocommerce');
		} else {
			$dimensions .= $product->get_height() .  __(esc_attr( get_option( 'woocommerce_dimension_unit' )), 'woocommerce');
		}
	}

	?>

	<div class="card-product__characteristics characteristics">
		<div class="characteristics__wrapper">
			<p class="characteristics__name">Модель:</p>
			<p class="characteristics__text"><?php echo $model; ?></p>
		</div>
		<div class="characteristics__wrapper">
			<p class="characteristics__name">Размер:</p>
			<p class="characteristics__text"><?php echo $dimensions; ?></p>
		</div>
		<div class="characteristics__wrapper">
			<p class="characteristics__name">Вес:</p>
			<p class="characteristics__text">
				<?php echo $weight . __(esc_attr( get_option( 'woocommerce_weight_unit' ) ), 'woocommerce'); ?>
			</p>
		</div>
		<div class="characteristics__wrapper">
			<p class="characteristics__name">Состав:</p>
			<p class="characteristics__text"><?php echo $composition; ?></p>
		</div>
	</div>

	<?php
}

// Есть или нет в наличии
add_action('woocommerce_single_product_summary', 'kedrm_woocommerce_single_available_before_buy', 20);
function kedrm_woocommerce_single_available_before_buy()
{
	$is_available = true;
	if ( get_post_meta(get_the_ID(), '_stock_status', true) == 'outofstock' ) {
		$is_available = false;
	}
	?>
	<div class="card-product__flex">
		<div class="card-product__button-wrapper">
			<?php
			if ($is_available):?>
				<div class="card-product__button-yes">Есть в наличии</div>
			<?php else: ?>
				<div class="card-product__button-no">Нету в наличии</div>
			<?php endif;
			?>
		</div>
		<div class="card-product__flex-block">
	<?php
}

// Включаем стандартный вывод цены
add_action('woocommerce_single_product_summary', 'woocommerce_template_single_price', 28);

add_action('woocommerce_single_product_summary', 'kedrm_woocommerce_single_after_buy', 35);
function kedrm_woocommerce_single_after_buy()
{ ?>
	</div><!-- .card-product__flex-block -->
<?php
}

// Убираем артикул, категории, теги
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40);

add_action('woocommerce_single_product_summary', 'kedrm_woocommerce_single_promote_text', 40);
function kedrm_woocommerce_single_promote_text()
{
	$sale_items = carbon_get_theme_option('kedrm_product_sale');
	if ($sale_items):?>
	<div class="card-product__sale sale">
		<?php foreach ($sale_items as $v):?>
			<p class="sale__text">
				<?php
					$img_id = $v['kedrm_product_sale_icon'];
					$img_url	= wp_get_attachment_image_url($img_id);
					$alt = get_post_meta($img_id, '_wp_attachment_image_alt', true);
				?>
					<img class="sale__icon" src="<?php echo $img_url;?>" alt="<?php echo $alt;?>">
					<?php	echo $v['kedrm_product_sale_text'];?>
			</p>
		<?php endforeach; ?>
	</div>
	<?php endif;
}

// Отрезаем всё лишнее от звёздочек рейтинга
add_filter('woocommerce_get_star_rating_html', 'kedrm_woocommerce_rating_html', 10, 2);
function kedrm_woocommerce_rating_html($html, $rating)
{
	if ($rating) {
		$temp = (float)$rating * 20;
		$html = "<span style='width:" . $temp . "%'></span>";
	}

	return $html;
}

// Добавляем кастомный класс для обертки цены
add_filter('woocommerce_product_price_class', 'kedrm_woocommerce_price_classes', 10, 1);
function kedrm_woocommerce_price_classes($classes)
{
	$classes .= ' card-product__price';
	return $classes;
}


/**
 * Ниже описание и отзывы. Бывшие табы
 */
remove_action('woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10);
remove_action('woocommerce_after_single_product_summary', 'woocommerce_upsell_display', 15);
remove_action('woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20);

add_action('woocommerce_after_single_product_summary', 'kedrm_woocommerce_single_description', 10);
function kedrm_woocommerce_single_description()
{ ?>
	<section class="page-card-product__info-product info-product">
		<div class="container">
			<h3 data-target="single-description" class="info-product__title">Описание</h3>
			<div class="info-product__text">
				<?php the_content(); ?>
			</div>
		</div>
	</section>


<?php
}

//Вывод отзывов - вместо табов
add_action('woocommerce_after_single_product_summary', 'kedrm_woocommerce_single_comments', 30);
function kedrm_woocommerce_single_comments()
{
	$product_tabs = apply_filters( 'woocommerce_product_tabs', array() );

	if ( ! empty( $product_tabs ) ) : ?>
		<?php foreach ( $product_tabs as $key => $product_tab ) :
			if ('comments_template' == $product_tab['callback']):?>

				<?php
				if ( isset( $product_tab['callback'] ) ) {
					call_user_func( $product_tab['callback'], $key, $product_tab );
				}
				?>

		<?php
			endif;
		endforeach; ?>

	<?php endif;
}

// Убираем вывод граватара
remove_action( 'woocommerce_review_before', 'woocommerce_review_display_gravatar', 10 );

// Меняем html текста отзыва
remove_action('woocommerce_review_comment_text', 'woocommerce_review_display_comment_text', 10);
add_action('woocommerce_review_comment_text', 'kedrm_woocommerce_display_comment_text', 10);
function kedrm_woocommerce_display_comment_text()
{
	echo '<p class="item-comment__user-text">';
		$temp = get_comment_text();
		echo $temp;
	echo '</p>';
}

// Убираем стандартный вывод звёздного рейтинга
remove_action( 'woocommerce_review_before_comment_meta', 'woocommerce_review_display_rating', 10 );
// Меняем html звездного рейтинга в списке отзывов
add_action( 'woocommerce_review_before_comment_meta', 'kedrm_woocommerce_single_comments_list_star_rating', 10 );
function kedrm_woocommerce_single_comments_list_star_rating()
{
	global $comment;
	$rating = intval( get_comment_meta( $comment->comment_ID, 'rating', true ) );
	?>
		<div class="item-comment__testimonials testimonials testimonials--bottom">
			<div class="testimonials__stars-wrapper">
				<span class="testimonials__star"></span>
			</div>
			<div class="testimonials__bg testimonials__bg--two">
				<span class="testimonials__text"><?php echo number_format($rating, 1, '.', ''); ?></span>
			</div>
		</div>
	<?php
}

// Вывод формы комментариев - меняем порядок полей
add_filter('comment_form_fields', 'kedrm_woocommerce_reorder_comment_fields' );
function kedrm_woocommerce_reorder_comment_fields( $fields ){

	$new_fields = array(); // сюда соберем поля в новом порядке
	$myorder = array('author', 'email', 'comment'); // нужный порядок

	foreach( $myorder as $key ){
		$new_fields[ $key ] = $fields[ $key ];
		unset( $fields[ $key ] );
	}

	// если остались еще какие-то поля добавим их в конец
	if( $fields )
		foreach( $fields as $key => $val )
			$new_fields[ $key ] = $val;

	return $new_fields;
}

add_action( 'comment_form_top', 'kedrm_woocommerce_comment_form_before_fields' );
function kedrm_woocommerce_comment_form_before_fields(){
	echo '<div class="form-reviews__body">';
}

add_filter( 'comment_form_fields', 'kedrm_comment_form_hide_cookies_consent' );
function kedrm_comment_form_hide_cookies_consent( $fields ) {
	unset( $fields['cookies'] );
	return $fields;
}

// Кастомная ф-ия для задания dynamic-adapt форме комментариев
function kedrm_single_comment_form($args)
{
	ob_start();
	comment_form($args);
	echo str_replace('<form','<form data-da=".comment-modal__content,767,last" ', ob_get_clean());
}


/**
 * Вывод связанных c товаром товаров
 */
add_action('woocommerce_after_single_product_summary', 'kedrm_woocommerce_single_related_products', 40);
function kedrm_woocommerce_single_related_products()
{
	global $product;

	$rels2 = [];
	$rels2 = carbon_get_post_meta($product->get_id(), 'kedrm_products_recommendations');

	foreach ($rels2 as $k => $v) {
		if ('publish' !== get_post_status($v['id'])) {
			unset($rels2[$k]);
		}
	}

	$rels = $rels2;
	if (count($rels) > 4) {
		$rels = [];
		$temp = array_rand($rels2, 4);

		foreach ($temp as $v) {
			$rels[] = $rels2[$v];
		}
	}

	if (count($rels) > 0):
		?>
		<section class="page-card-product__product product-card">
			<div class="container">
				<h2 class="product-card__title">Рекомендуемые товары</h2>
				<div class="product-card__catalog catalog swiper-container catalog-slider">
					<ul class="catalog__list swiper-wrapper">

					<?php
					foreach ($rels as $v):
						get_template_part( 'template-parts/related-product-list-item', null, [$v['id']] );
					endforeach;?>

					</ul> <!-- .catalog__list -->
					<div class="catalog__slider-buttons-container"></div>
				</div><!-- .catalog -->

			</div>

		</section><!-- product-card -->

		<?php
	endif;
}

/**
 * Вывод статей, в которых указан текущий товар
 */
add_action('woocommerce_after_single_product_summary', 'kedrm_woocommerce_single_related_articles', 25);
function kedrm_woocommerce_single_related_articles()
{
	get_template_part('template-parts/single', 'related-articles');
}
