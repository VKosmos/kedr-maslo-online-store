<?php
if (!defined('ABSPATH')) exit;

remove_action('woocommerce_before_main_content', 'woocommerce_breadcrumb', 20);
add_action('woocommerce_before_main_content', 'kedrm_breadcrumbs', 20);
function kedrm_breadcrumbs()
{
	woocommerce_breadcrumb([
		'wrap_before' 	=> '<ul class="woocommerce-breadcrumb breadcrumbs">',
		'wrap_after'  	=> '</ul>',
		'before'      	=> '<li class="breadcrumbs__item">',
		'after'       	=> '</li>',
	]);
};

// Change Add to Cart button text for specific products in Woocommerce
add_filter( 'woocommerce_product_single_add_to_cart_text', 'kedrm_change_add_to_cart_title', 10, 3 );
function kedrm_change_add_to_cart_title() {
	return 'купить';
}

/**
 * Remove default WooCommerce wrapper.
 */
remove_action( 'woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10 );
remove_action( 'woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10 );

add_action( 'woocommerce_before_main_content', 'kedrm_woocommerce_wrapper_before', 10 );
if ( ! function_exists( 'kedrm_woocommerce_wrapper_before' ) ) {
	function kedrm_woocommerce_wrapper_before() {

		if (is_shop() || is_product_taxonomy()) {?>
			<section class="catalog"><div class="container">
			<?php
		} elseif(is_product()) {
			global $product;
			$is_available_class = ( get_post_meta(get_the_ID(), '_stock_status', true) == 'outofstock' ) ? ' _not-available' : '';
			$is_sale_class = ( $product->is_on_sale() ) ? ' _on-sale' : '';
			?>
				<section class="card-product <?php echo $is_available_class; echo $is_sale_class;?>"><div class="container">
			<?php
		}

	}
}

// add_action( 'woocommerce_after_main_content', 'kedrm_woocommerce_wrapper_after' );
add_action( 'woocommerce_single_product_summary', 'kedrm_woocommerce_wrapper_after', 70, 1 );
if ( !function_exists( 'kedrm_woocommerce_wrapper_after' ) ) {
	function kedrm_woocommerce_wrapper_after() {
		if (is_product()):?>
			</div><!-- .cart-product__flex-wrapper -->
		<?php endif;?>
			</div><!-- .container -->
		</section>
	<?php
	}
}

// /**
//  * Redirect for ajax filters
//  */
// add_action( 'template_redirect', 'custom_shop_page_redirect' );
// function custom_shop_page_redirect() {
// 	if( is_product_taxonomy() ){
// 		$category = get_queried_object();
// 		$cat_id = $category->term_id;
// 	 	wp_redirect( home_url( '/catalog/?cats=' . $cat_id ) );
// 		exit();
// 	}
// }



/**
 * Заправшиваем товары при установленных тегах (при перемещении по истории браузера)
 */
add_action( 'pre_get_posts', 'kedrm_query_catalog', 1 );
function kedrm_query_catalog( $query )
{
	if ($query->is_main_query()) {

		if (is_post_type_archive( 'product' ) || is_product_category()) {

			// $cat_id = (isset($_GET['cats'])) ? (int)(sanitize_text_field( $_GET['cats'])) : -1;
			// if ($cat_id > 0) {
			// 	$meta_query = [
			// 		[
			// 			'taxonomy' 	=> 'product_cat',
			// 			'terms'		=> $cat_id,
			// 		]
			// 	];
			// 	$query->set( 'tax_query', $meta_query );
			// }

			$tagsStr = (isset($_GET['tags'])) ? (sanitize_text_field( $_GET['tags'] )) : 'all';
			if ('all' === $tagsStr) {
				$tags_ids = 'all';
			} else {
				$tags_ids = explode(',', $tagsStr);
			}

			if ('all' <> $tags_ids) {
				$meta_query = [
					[
						'taxonomy'	=> 'product_tag',
						'terms'		=> $tags_ids,
					]
				];
				$query->set( 'tax_query', $meta_query );
			}

		}


	}
}



/**
 * Замена символа валюты
 */
add_filter('woocommerce_currency_symbol', 'kedrm_change_rub_currency_symbol', 10, 2);
function kedrm_change_rub_currency_symbol( $currency_symbol, $currency )
{
	switch( $currency ){
		// case 'RUB': $currency_symbol = ' руб.'; break;
		case 'RUB': $currency_symbol = ' ' . $currency_symbol; break;
	}
	return $currency_symbol;
}
