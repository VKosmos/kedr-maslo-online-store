<?php
if (!defined('ABSPATH')) exit;

// add_action('woocommerce_before_checkout_form', 'kedrm_checkout_before_content_wrapper', 5);
function kedrm_checkout_before_content_wrapper()
{?>
<div class="standard__flex-container">
	<div class="standard__flex-column-main">

<?php
}


/*
 * Добавляем часть формы к фрагменту
 */
// add_filter( 'woocommerce_update_order_review_fragments', 'awoohc_add_update_form_billing', 99 );
function awoohc_add_update_form_billing( $fragments ) {

	$checkout = WC()->checkout();
	$temp = array_values($checkout->get_checkout_fields('billing')['billing_city']['options']);
	$city = $temp[0];

	// if ( WC()->cart->needs_payment() ) {
	// 	$available_gateways = WC()->payment_gateways()->get_available_payment_gateways();
	// 	WC()->payment_gateways()->set_current_gateway( $available_gateways );
	// } else {
	// 	$available_gateways = array();
	// }

	if ( WC()->cart->needs_payment() ) : ?>
		<ul class="wc_payment_methods payment_methods methods">
			<?php
			if ( ! empty( $available_gateways ) ) {
				foreach ( $available_gateways as $gateway ) {

					echo "<pre>";
					print_r($gateway);
					echo "</pre>";
					die();
					if (true) {
						wc_get_template( 'checkout/payment-method.php', array( 'gateway' => $gateway ) );
					}

				}
			}
			?>
		</ul>
	<?php
	endif;

	// ob_start();
	// wc_get_template(
	// 	'checkout/payment-ul.php',
	// 	array(
	// 		'checkout'           => WC()->checkout(),
	// 		'available_gateways' => $available_gateways,
	// 		'order_button_text'  => apply_filters( 'woocommerce_order_button_text', __( 'Place order', 'woocommerce' ) ),
	// 	)
	// );
	// $temp = ob_get_clean();

	$fragments['.fortest'] = $temp;

	return $fragments;
}


/**
 * @snippet       Descriptions for delivery methods
 * @sourcecode    https://wpruse.ru/?p=4114
 * @testedwith    WooCommerce 3.9
 *
 * @param  object $method объект метода доставки
 * @param  int    $index  счетчик
 *
 * @author        Artem Abramovich
 */
add_action( 'woocommerce_after_shipping_rate', 'kedrm_action_after_shipping_rate', 20, 2 );
function kedrm_action_after_shipping_rate( $method, $index ) {

	// Если корзина, то ничего не выводим
	if ( is_cart() ) return;

	$style = '<style>
			.order-notice {
				display: flex;
				align-items: center;
				position: absolute;
				right: 0;
				width: 300px;
				height: 90px;
				text-align: left;
				z-index: 10;
			}
			li:has(.order-notice) {
				margin-bottom: 90px;
			}
			@media (max-width: 409.98px) {
				.order-notice {
					width: 212px;
					height: 125px;
				}
			}
		</style>';

	$script =
	'<script>
		if (jQuery(window).width() >= 410) {
			jQuery(".order-notice").parent().css("margin-bottom", 90);
		} else {
			jQuery(".order-notice").parent().css("margin-bottom", 125);
		}

	</script>';

	// Получаем выбранный метод
	$chosen_methods = WC()->session->get( 'chosen_shipping_methods' );

	if ( 'edostavka-package-stock:6:136' === $chosen_methods[0] && 'edostavka-package-stock:6:136' === $method->id ) {
		// Сообщение для конкретного способа доставки
		echo $style;
		echo $script;
		?>

		<div class="order-notice">Сумма доставки предварительная, точная сумма будет известная после упаковки и оформления на доставку. Доставка оплачивается при получении.</div>
		<?php

	}

	if ( 'alg_wc_shipping:8' === $chosen_methods[0] && 'alg_wc_shipping:8' === $method->id ) {
		echo $style;
		echo $script;
		?>

		<div class="order-notice">Стоимость доставки оплачивается вместе с заказом (в среднем 250-300 р). После оформления заказа с вами свяжется менеджер и сообщит точную стоимость </div>
		<?php

	}

}

/**
 *	Setting default order status
 */
add_action( 'woocommerce_thankyou', 'kedrm_woocommerce_thankyou_change_order_status', 10, 1 );
function kedrm_woocommerce_thankyou_change_order_status( $order_id )
{
	if( ! $order_id ) return;
	$order = wc_get_order( $order_id );
	$order->update_status( 'pending' );
}