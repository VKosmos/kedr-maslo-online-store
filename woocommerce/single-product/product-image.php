<?php
/**
 * Single Product Image
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/product-image.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.5.1
 */

defined( 'ABSPATH' ) || exit;

// Note: `wc_get_gallery_image_html` was added in WC 3.3.2 and did not exist prior. This check protects against theme overrides being used on older versions of WC.
if ( ! function_exists( 'wc_get_gallery_image_html' ) ) {
	return;
}

global $product;

$thumbnail_id = $product->get_image_id();
$images_ids = $product->get_gallery_image_ids();

if ($thumbnail_id) {
	array_unshift($images_ids, $thumbnail_id);
}


$video_url = carbon_get_post_meta($product->get_id(), 'kedrm_product_video-url');

$video_index = -1;
if ($video_url) {

	$video_index = carbon_get_post_meta($product->get_id(), 'kedrm_product_video-index');
	$video_index = ($video_index) ? ((int)$video_index - 1) : 1;
	$video_index = ($video_index <= count($images_ids)) ? $video_index : count($images_ids);

	$video_thumb = carbon_get_post_meta($product->get_id(), 'kedrm_product_video-thumb');
	$video_thumb = ($video_thumb) ? ((int)$video_thumb) : carbon_get_theme_option('kedrm_default_video_thumb');

	array_splice($images_ids, $video_index, 0, $video_url);
}

$i = 0;
$j = 0;

?>

<div class="card-product__slider slider-product">
	<?php if (count($images_ids) > 0):?>

		<div class="card-product-slider-big swiper-container">
			<div class="slider-product__big swiper-wrapper">

			<?php foreach($images_ids as $v):
					$alt = get_post_meta($v, '_wp_attachment_image_alt', true);
				?>
				<div class="slider-product__big-wrapper swiper-slide">
					<?php if ($j === $video_index): ?>
						<iframe src="<?php echo $video_url; ?>" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
					<?php else:?>
						<img src="<?php echo wp_get_attachment_image_url( $v, 'card-image' ) ?>" alt="<?php echo $alt;?>" class="slider-product__big-image">
					<?php endif; ?>
				</div>
			<?php
					$j++;
				endforeach; ?>

			</div>
			<div class="card-product-slider-big__controls-wrapper"></div>
		</div>

		<div class="card-product-slider-min swiper-container">
			<div class="slider-product__min swiper-wrapper">
			<?php foreach($images_ids as $v):
				$alt = get_post_meta($v, '_wp_attachment_image_alt', true);
				?>

					<?php if ($i === $video_index): ?>

						<div class="slider-product__min-wrapper slider-product__min-wrapper--video swiper-slide">

						<img src="<?php echo wp_get_attachment_image_url( $video_thumb, 'card-thumb' ) ?>" alt="<?php echo $alt ?>" class="slider-product__min-image">

						</div>
					<?php else:?>

						<div class="slider-product__min-wrapper swiper-slide">

						<img src="<?php echo wp_get_attachment_image_url( $v, 'card-thumb' ) ?>" alt="<?php echo $alt ?>" class="slider-product__min-image">

						</div>
					<?php endif; ?>

			<?php
					$i++;
				endforeach; ?>
			</div>
		</div>

	<?php endif; ?>
</div><!-- .slider-product -->



