<?php
/**
 * Customer on-hold order email
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/customer-on-hold-order.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates\Emails
 * @version 3.7.0
 */

defined( 'ABSPATH' ) || exit;

/*
 * @hooked WC_Emails::email_header() Output the email header
 */
do_action( 'woocommerce_email_header', $email_heading, $email ); ?>

<?php /* translators: %s: Customer first name */ ?>
<p><?php printf( esc_html__( 'Hi %s,', 'woocommerce' ), esc_html( $order->get_billing_first_name() ) ); ?></p>

<p>Спасибо за заказ. Пожалуйста прочтите внимательно пункт №3.</p>
<p>Посылку сегодня соберем. Всю информацию пришлю Вам по факту сборки.</p>
<p>Как мы работаем:</p>
<p>1. Упаковываем посылку. Нашей упаковки хватает, что бы посылка пришла в целости и сохранности.</p>
<p>2. Оформляем на доставку - по Вашему выбору почтой или сдэком. Боксберри, ДПД не отправляем. Транспортными компаниями в исключительных случаях.</p>
<p>3. Выставляем счет через ЯНДЕКС.ДЕНЬГИ от ООО"Радоград" с почты inform@yamoney.ru с номером отправления (сохраните его до оплаты заказа!!). Пожалуйста проверяйте папку СПАМ</p>
<p>4. Отправка после оплаты в течении суток. Оплату видим сразу, подтверждение присылать нет необходимости.</p>
<p>5. После оплаты, в течении суток Вам придет ответное письмо о том, что посылка направлена в отделение и на телефон в смс чек об оплате.</p>
<p>Чек Вы можете проверить на сайте налоговой.</p>
<p>6. Отслеживание посылки Вы осуществляете самостоятельно, но если возникли сложности или потеряли номер, то можете всегда запросить информацию.</p>
<p>Для справки.</p>
<p>1. Наложенным платежом не отправляем т.к. часты случаи, когда клиент не получает посылку вовремя, и она уезжает обратно к нам.
В таком случае мы платим за доставку туда и обратно, а также продукция "простаивает" и ее мог получить другой клиент более свежую.</p>
<p>Оплата заказа осуществляется на реквизиты ООО "Радоград"».</p>


<?php

/*
 * @hooked WC_Emails::order_details() Shows the order details table.
 * @hooked WC_Structured_Data::generate_order_data() Generates structured data.
 * @hooked WC_Structured_Data::output_structured_data() Outputs structured data.
 * @since 2.5.0
 */
do_action( 'woocommerce_email_order_details', $order, $sent_to_admin, $plain_text, $email );

/*
 * @hooked WC_Emails::order_meta() Shows order meta data.
 */
do_action( 'woocommerce_email_order_meta', $order, $sent_to_admin, $plain_text, $email );

/*
 * @hooked WC_Emails::customer_details() Shows customer details
 * @hooked WC_Emails::email_address() Shows email address
 */
do_action( 'woocommerce_email_customer_details', $order, $sent_to_admin, $plain_text, $email );

/**
 * Show user-defined additional content - this is set in each email's settings.
 */
if ( $additional_content ) {
	echo wp_kses_post( wpautop( wptexturize( $additional_content ) ) );
}

/*
 * @hooked WC_Emails::email_footer() Output the email footer
 */
do_action( 'woocommerce_email_footer', $email );
