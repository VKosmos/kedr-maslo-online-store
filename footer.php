<?php
/**
 * The template for displaying the footer
 *
 * @package KedrMaslo
 */

?>

</main>

<footer class="footer">
	<div class="container">
		<div class="footer__flex-wrapper">

			<div class="footer__flex-column footer__flex-column--one">
				<?php
					$logo_id = carbon_get_theme_option('kedrm_footer_logo_big');
					$logo_mid_id = carbon_get_theme_option('kedrm_footer_logo_middle');
					if ($logo_id && $logo_mid_id):
						$logo = wp_get_attachment_url( $logo_id );
						$logo_mid = wp_get_attachment_url( $logo_mid_id );

						if (is_front_page() && is_home()):?>

							<div class="footer__logo-link">
								<img src="<?php echo $logo ?>" alt="" class="footer__logo footer__logo--big" width="149"
								height="222">
								<img src="<?php echo $logo_mid; ?>" alt="" class="footer__logo footer__logo--min" width="73"
								height="108.77">
							</div>

						<?php else:?>

							<a href="<?php echo home_url('/'); ?>" class="footer__logo-link">
								<img src="<?php echo $logo; ?>" alt="" class="footer__logo footer__logo--big" width="149"
								height="222">
								<img src="<?php echo $logo_mid; ?>" alt="" class="footer__logo footer__logo--min" width="73"
								height="108.77">
							</a>

						<?php endif;
					endif; ?>
			</div>

			<div class="footer__flex-column footer__flex-column--two">
				<h3 class="footer__headline">информация</h3>
				<nav class="footer__nav nav-footer">
					<?php
						kedrm_footer_menu('main');
					?>
				</nav>

				<div class="footer__line line-delimeter">
					<span class="line-delimeter__line"></span>
					<span class="line-delimeter__rectangle line-delimeter__rectangle--one"></span>
					<span class="line-delimeter__rectangle line-delimeter__rectangle--two"></span>
					<span class="line-delimeter__rectangle line-delimeter__rectangle--three"></span>
					<span class="line-delimeter__rectangle line-delimeter__rectangle--four"></span>
				</div>

			</div>

			<div class="footer__flex-column footer__flex-column--three">
				<h3 class="footer__headline">Наша продукция</h3>
				<nav class="footer__nav nav-footer">
					<?php
						kedrm_footer_menu('catalog');
					?>
				</nav>
			</div>

			<div class="footer__flex-column footer__flex-column--four">
				<h3 class="footer__headline">контакты</h3>
				<div class="footer__contacts contacts-footer">
					<p class="contacts-footer contacts-footer__item">
						<img src="<?= get_template_directory_uri() . '/assets/img/icon/call-white.svg'?> " alt="" class="contacts-footer__icon" width="13.33"
							height="13.33">
						<a href="tel:<?php echo carbon_get_theme_option('kedrm_phonedigits'); ?>"><?php echo carbon_get_theme_option('kedrm_phone'); ?></a>
					</p>
					<p class="contacts-footer contacts-footer__item">
						<img src="<?= get_template_directory_uri() . '/assets/img/icon/smartphone-white.svg'?>" alt="" class="contacts-footer__icon" width="10.31"
							height="16">
						<a href="tel:<?php echo carbon_get_theme_option('kedrm_mobiledigits'); ?>"><?php echo carbon_get_theme_option('kedrm_mobile'); ?></a>
					</p>
					<p class="contacts-footer contacts-footer__item">
						<img src="<?= get_template_directory_uri() . '/assets/img/icon/clock-white.svg'?>" alt="" class="contacts-footer__icon" width="11.33"
							height="11.33">
						<?php echo carbon_get_theme_option('kedrm_worktime'); ?>
					</p>
					<p class="contacts-footer contacts-footer__item">
						<img src="<?= get_template_directory_uri() . '/assets/img/icon/mail-white.svg'?>" alt="" class="contacts-footer__icon" width="16" height="16">
						<a href="mailto:<?php echo carbon_get_theme_option('kedrm_email'); ?>"><?php echo carbon_get_theme_option('kedrm_email'); ?></a>
					</p>
					<p class="contacts-footer contacts-footer__item">
						<img src="<?= get_template_directory_uri() . '/assets/img/icon/map-white.svg'?>" alt="" class="contacts-footer__icon" width="11.33"
							height="13.33">
						<?php echo carbon_get_theme_option('kedrm_address'); ?>
					</p>
				</div>
				<div class="footer__social social-footer">
					<a href="<?php echo carbon_get_theme_option('kedrm_vk'); ?>" class="social-footer__link">
						<img src="<?= get_template_directory_uri() . '/assets/img/icon/vk-white.svg'?>" alt="VKontakte" width="20" height="20">
					</a>
					<a href="<?php echo carbon_get_theme_option('kedrm_youtube'); ?>" class="social-footer__link">
						<img src="<?= get_template_directory_uri() . '/assets/img/icon/youtube-white.svg'?>" alt="Youtube" width="20" height="20">
					</a>
				</div>

			</div>


		</div>
	</div>
</footer>

<div class="ornament fix-block-margin"></div>
<div class="scroll-up"></div>

<div class="modal">

	<?php if (is_front_page()):?>
	<div class="modal__table" data-vktarget="main-video">
		<div class="modal__table-cell">
			<div class="modal__container main-video">
				<div class="modal__content main-video__content">
					<button class="modal__close main-video__close">Закрыть</button>

					<?php
						$video_url = carbon_get_post_meta(get_the_ID(), 'kedrm_main_banner_video' );
						if ($video_url):?>

						<iframe src="https://www.youtube.com/embed/<?php echo $video_url;?>" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

					<?php endif; ?>

				</div>
			</div>
		</div>
	</div>
	<?php endif;?>

	<div class="modal__table" data-vktarget="login">
		<div class="modal__table-cell">
			<div class="modal__container login-modal">
				<div class="modal__content login-modal__content">
					<button class="modal__close login-modal__close">Закрыть</button>

					<h2 class="login-modal__title">Добро пожаловать <span>на ЛадоЯр</span></h2>

					<?php get_template_part('/woocommerce/includes/parts/wc-form', 'login'); ?>

				</div>
			</div>
		</div>
	</div>

	<div class="modal__table" data-vktarget="register">
		<div class="modal__table-cell">
			<div class="modal__container register-modal">
				<div class="modal__content register-modal__content">
					<button class="modal__close register-modal__close">Закрыть</button>

					<h2 class="register-modal__title">Добро пожаловать <span>на ЛадоЯр</span></h2>

					<?php get_template_part('/woocommerce/includes/parts/wc-form', 'register'); ?>

				</div>
			</div>
		</div>
	</div>

	<div class="modal__table" data-vktarget="catalog">
		<div class="modal__table-cell modal__table-cell--top">
			<div class="modal__container catalog-modal">
				<div class="modal__content catalog-modal__content">

					<div class="catalog-modal__title-wrapper">
						<h3 class="catalog-modal__title">Каталог товаров</h3>
						<button class="modal__close catalog-modal__close">Закрыть</button>
					</div>

					<?php
						kedrm_header_menu('catalog');
					?>

				</div>
			</div>
		</div>
	</div>

	<div class="modal__table" data-vktarget="nav">
		<div class="modal__table-cell">
			<div class="modal__container nav-modal">
				<div class="modal__content nav-modal__content">
					<button class="modal__close nav-modal__close">Закрыть</button>

					<nav class="nav-modal__nav">

						<?php
							kedrm_modal_menu('main');
						?>

						<a href="tel:<?php echo carbon_get_theme_option('kedrm_mobiledigits'); ?>" class="nav-modal__call">
							<svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
								<path
									d="M12.1109 9.58605C11.6631 9.48972 11.3088 9.69765 10.9951 9.87919C10.6739 10.0663 10.0631 10.5619 9.71301 10.435C7.92041 9.69692 6.23443 8.12792 5.5046 6.32817C5.37597 5.97057 5.86917 5.35586 6.0549 5.03083C6.23512 4.71621 6.43873 4.35861 6.34586 3.90745C6.26194 3.50203 5.17649 2.12085 4.79266 1.74315C4.53952 1.49366 4.2802 1.35645 4.01399 1.33427C3.01314 1.2913 1.89536 2.62675 1.69931 2.94623C1.20817 3.62747 1.21092 4.53394 1.70756 5.63307C2.90446 8.58532 7.43134 13.0408 10.3947 14.2827C10.9415 14.5384 11.4416 14.6666 11.8907 14.6666C12.3303 14.6666 12.7217 14.5439 13.0581 14.3007C13.3119 14.1545 14.7021 12.9812 14.6656 11.9534C14.6436 11.6915 14.5067 11.4295 14.2605 11.1759C13.8856 10.7885 12.5133 9.67059 12.1109 9.58605Z"
									fill="white" />
							</svg>

							<?php echo carbon_get_theme_option('kedrm_mobile'); ?>
						</a>
					</nav>

				</div>
			</div>
		</div>
	</div>


	<div class="modal__table" data-vktarget="comment">
			<div class="modal__table-cell">
				<div class="modal__container comment-modal">
					<div class="modal__content comment-modal__content">
						<button class="modal__close comment-modal__close">Закрыть</button>
					</div>
				</div>
			</div>
		</div>
</div>

<div class="modal2"></div>

<?php wp_footer(); ?>

</body>
</html>
