<?php

$args = array(
	'post_type'              => array( 'product' ),
	'post_status'            => array( 'publish' ),
	'nopaging'               => true,
);

$query = new WP_Query( $args );
if($query->have_posts()){

	while ($query->have_post()) {
		$quert->the_post();
		$ex = get_the_excerpt();
		carbon_set_post_meta( get_the_ID(), 'kedrm_product_model', $ex );
	}
}