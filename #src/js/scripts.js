$(document).ready(function () {

	/**
	 * Условие - временная заглушка для html верстки
	 * В WP использоваться будут специальные функции
	 */
	let windowWidth = window.innerWidth;

	/**
	 * Переключение представлений каталога
	 */
	$('.catalog__switcher').on('click', function () {
		if (('horizontal') === $(this).attr('data-catalog')) {
			$('.catalog__list').addClass('catalog__list--horizontal');
			$('.catalog__switcher.js-active').removeClass('js-active');
			$(this).addClass('js-active');
		} else if (('vertical') === $(this).attr('data-catalog')) {
			$('.catalog__list').removeClass('catalog__list--horizontal');
			$('.catalog__switcher.js-active').removeClass('js-active');
			$(this).addClass('js-active');
		}
	})

	/**
	 * Основной слайдер на главной странице О нас
	 */
	 let swiperMainMin = new Swiper(".main-min-sliderr", {
		slidesPerView: 3,
		spaceBetween: 32,
		// freeMode: true,
		watchSlidesVisibility: true,
		watchSlidesProgress: true,
		// allowTouchMove: false,
		breakpoints: {
			320: {
				spaceBetween: 16,
			},
			1360: {
				spaceBetween: 32,
			}
		}
	});

	let slideTextContainerEl = $('.main-slider__slide-content');
	let sliderBigEl = $('.main-big-slider');

	let swiperMainBig = new Swiper(".main-big-slider", {
		slidesPerView: 1,
		effect: 'fade',
		fadeEffect: {
			crossFade: true
		},
		thumbs: {
			swiper: swiperMainMin,
		},
		pagination: {
			el: ".main-slider__controls-wrapper .swiper-pagination-bullets",
			clickable: true,
		},
		navigation: {
			nextEl: '.slider-arrow--next',
			prevEl: '.slider-arrow--prev',
		},
		on: {
			init: function () {
				let curSlideContent = sliderBigEl.find('.big-slider__image-wrapper[data-slide-index=0]').find('.big-slider__body');
				if (curSlideContent) {
					slideTextContainerEl.html(curSlideContent.clone());
				}
			},
		 },
	});


	swiperMainBig.on('slideChange', function () {
		let curSlideContent = sliderBigEl.find('.big-slider__image-wrapper[data-slide-index=' + this.activeIndex + ']').find('.big-slider__body');
		if (curSlideContent) {
			slideTextContainerEl.html(curSlideContent.clone());
		}
	})

	/**
	* Слайдер на странице about us
	*/
	let swiperAboutMin = new Swiper(".about-us-slider-min", {
		slidesPerView: 3,
		spaceBetween: 32,
		watchSlidesVisibility: true,
		watchSlidesProgress: true,
		// allowTouchMove: false,
		breakpoints: {
			320: {
				spaceBetween: 16,
			},
			1360: {
				spaceBetween: 32,
			}
		}
	});

	// slideTextContainerEl = $('.main-slider__slide-content');
	let sliderAboutBigEl = $('.about-us-slider-big');

	let swiperAboutBig = new Swiper(".about-us-slider-big", {
		slidesPerView: 1,
		effect: 'fade',
		fadeEffect: {
			crossFade: true
		},
		thumbs: {
			swiper: swiperAboutMin,
		},
		pagination: {
			el: ".main-slider__controls-wrapper .swiper-pagination-bullets",
			clickable: true,
		},
		navigation: {
			nextEl: '.slider-arrow--next',
			prevEl: '.slider-arrow--prev',
		},
		on: {
			init: function () {
				let curSlideContent = sliderAboutBigEl.find('.big-slider__image-wrapper[data-slide-index=0]').find('.big-slider__body');
				if (curSlideContent) {
					slideTextContainerEl.html(curSlideContent.clone());
				}
			},
		 },
	});

	swiperAboutBig.on('slideChange', function () {
		let curSlideContent = sliderAboutBigEl.find('.big-slider__image-wrapper[data-slide-index=' + this.activeIndex + ']').find('.big-slider__body');
		if (curSlideContent) {
			slideTextContainerEl.html(curSlideContent.clone());
		}
	})

	/**
	 * Слайдер в карточке товара
	 */
	 let swiperCardMin = new Swiper(".card-product-slider-min", {
		slidesPerView: 4,
		spaceBetween: 16,
		freeMode: false,
		// breakpoints: {
		// 	320: {
		// 		spaceBetween: 16,
		// 	},
		// 	1360: {
		// 		spaceBetween: 32,
		// 	}
		// }
	});

	let swiperCardBig = new Swiper(".card-product-slider-big", {
		slidesPerView: 1,
		effect: 'fade',
		fadeEffect: {
			crossFade: true
		},
		thumbs: {
			swiper: swiperCardMin,
		},
		pagination: {
			el: ".card-product-slider-big__controls-wrapper",
			clickable: true,
		},
	});

	//--------------------------------------

	//
	// Прокрутка в верх страницы
	//
	const headerEl = $('.header');
	$(window).scroll(function () {


		if ($(this).scrollTop() > 400) {
			$('.scroll-up').addClass('active');
		} else {
			$('.scroll-up').removeClass('active');
		}

		if ($(this).scrollTop() > 0) {
			if (!headerEl.hasClass('js-sticky-header')) {
				let headerHeight = headerEl.height();
				headerEl.addClass('js-sticky-header');
				$('body:not(.main-page) main').css('padding-top', headerHeight + 'px');
			}
		} else {
			if (!headerEl.hasClass('js-modal-header')) {
				if (headerEl.hasClass('js-sticky-header')) {
					headerEl.removeClass('js-sticky-header');

					$('body:not(.main-page) main').css('padding-top', '0');
				}
			}
		}

	});

	// Прокрутка
	$('.scroll-up').click(function () {
		$('html, body').animate({
			scrollTop: 0
		}, 1000);
		return false;
	});
	// Прокрутка в верх страницы -------------------- КОНЕЦ

	// Карта
	if ($('body').hasClass('contacts')) {

		ymaps.ready(init);

		function init() {
			var myMap = new ymaps.Map("map", {
				// Координаты центра карты
				center: [56.876323, 60.612395],
				// Масштаб карты
				zoom: 16,
				// Выключаем все управление картой
				controls: []
			});

			var myGeoObjects = [];

			// Указываем координаты метки
			myGeoObjects = new ymaps.Placemark([56.876323, 60.612395], {
				balloonContentBody: 'Самое \'Полезное масло\'',
			}, {
				iconLayout: 'default#image',
				// Путь до нашей картинки
				iconImageHref: '../img/icon/map-marker-red.png',
				// Размеры иконки
				iconImageSize: [40, 54],
				// Смещение верхнего угла относительно основания иконки
				iconImageOffset: [-35, -35]
			});

			var clusterer = new ymaps.Clusterer({
				clusterDisableClickZoom: false,
				clusterOpenBalloonOnClick: false,
			});
			clusterer.add(myGeoObjects);
			myMap.geoObjects.add(clusterer);
		}
	}

	/**
	 * Смена визуала кнопки Каталог в шапке
	 */
	$('.header__catalog-open').on('click', function (ev) {
		ev.preventDefault();

		if (this.classList.contains('js-active')) {
			this.classList.remove('js-active');
		} else {
			this.classList.add('js-active');
		}

	})


	/**
	 * Фильтры на странице База Знаний
	 */
	document.addEventListener('click', (ev) => {
		const filter_opened = document.querySelector('.filters__filter.js-active');
		if (filter_opened && !ev.target.closest('.filters__filter')) {
			filter_opened.classList.remove('js-active');
			ev.preventDefault();
		}

		const catalogEl = document.querySelector('.header__catalog-open');
		if (!catalogEl) {
			return;
		}

		if (ev.target.classList.contains('header__catalog-open') || ev.target.closest('.header__catalog-open')) {
			if (!catalogEl.classList.contains('js-active')) {
				catalogEl.classList.removeClass('js-active');
			} else {
				catalogEl.classList.add('js-active');
			}
		} else {
			if (!ev.target.classList.contains('modal__content') && !ev.target.closest('.modal__content')) {
				catalogEl.classList.remove('js-active');
			}
		}

	})

	window.addEventListener('keydown', function (ev) {
		if (ev.keyCode == 27) {
			const filter_opened = document.querySelector('.filters__filter.js-active');
			if (filter_opened) {
				filter_opened.classList.remove('js-active');
			}

			if (document.querySelector('.header__catalog-open.js-active')) {
				document.querySelector('.header__catalog-open.js-active').classList.remove('js-active');
			}

		}
	})


	/**
	 * Filters for knowledge page
	 */
	$('.filters__selector').on('click', function () {
		if ($(this).parent('.filters__filter').hasClass('js-active')) {
			$(this).parent('.filters__filter').removeClass('js-active');
		} else {
			$('.filters__filter.js-active').removeClass('js-active');
			$(this).parent('.filters__filter').addClass('js-active');
		}
	})

});