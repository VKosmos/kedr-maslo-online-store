$(document).ready(function () {

	/**
	 * Условие - временная заглушка для html верстки
	 * В WP использоваться будут специальные функции
	 */
	let windowWidth = window.innerWidth;

	if (windowWidth < 768) {

		$('.catalog__list--horizontal').removeClass('catalog__list--horizontal');

		/**
		 * Swiper slider for Catalog page
		 */
		var swiperCategories = new Swiper(".scroll-list", {
			slidesPerView: 'auto',
			// centerMode: true,
			spaceBetween: 10,
			freeMode: true,
			pagination: {
				el: ".scroll-list__pagination",
				clickable: false,
			},
		});


		/**
		 *	Selecting categories in mobile filter in product archive page
		 */
		$('.scroll-list__item').on('click', function () {
			$(this).toggleClass('_active');
			$(this).find('.scroll-list__category-link').toggleClass('_selected');
		})

		let swiperCatalog = new Swiper(".catalog-slider", {
			slidesPerView: 1,
			spaceBetween: 10,
			pagination: {
				el: ".catalog__slider-buttons-container",
				clickable: true,
			},
		});

		let swiperKnowledge = new Swiper(".knowledge-slider", {
			slidesPerView: 1,
			spaceBetween: 10,
			pagination: {
				el: ".knowledge__slider-buttons-container",
				clickable: true,
			},
		});

		let swiperOurProducts = new Swiper(".our-products-slider", {
			slidesPerView: 2,
			slidesPerColumn: 2,
			slidesPerGroup: 2,
			spaceBetween: 15,
			pagination: {
				el: ".our-products__slider-buttons-container",
				clickable: true,
			},
		});
	}



});