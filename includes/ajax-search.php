<?php
if (!defined('ABSPATH')) exit;

add_action('wp_ajax_faq-ajax-search', 'kedrm_search_ajax_faq_callback');
add_action('wp_ajax_nopriv_faq-ajax-search', 'kedrm_search_ajax_faq_callback');
function kedrm_search_ajax_faq_callback() {

	if (!wp_verify_nonce( $_POST['nonce'], 'search-nonce' )) {
		wp_die('Данные отправлены с левого адреса.');
	}

	$query = new WP_Query([
		'post_type' 	=> 'page',
		'post_status' 	=> 'publish',
		'meta_query' 	=> [
			[
				'key' => 'kedrm_questions_products',
			]
		]
	]);

	if ($query->have_posts()){
		$query->the_post();

		$json_data['out'] = ob_start();

		// $faqs_products = carbon_get_post_meta( get_the_ID(), 'kedrm_questions_products' );

		$faqs_products = carbon_get_post_meta( get_the_ID(), 'kedrm_questions_products' );

		$json_data['out'] .= ob_get_clean();
		wp_send_json( $json_data );
	}

	wp_die();

}