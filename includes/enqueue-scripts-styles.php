<?php
if (!defined('ABSPATH')) exit;

add_action( 'wp_enqueue_scripts', 'kedrm_styles' );
function kedrm_styles() {
	wp_enqueue_style( 'kedrm-style', get_stylesheet_uri(), array(), _S_VERSION );
	wp_style_add_data( 'kedrm-style', 'rtl', 'replace' );

	wp_enqueue_style( 'swiper-slider', "https://unpkg.com/swiper/swiper-bundle.min.css", null, null, 'all');
	wp_enqueue_style( 'kedrm-main-styles', get_template_directory_uri() . '/assets/css/style.css', ['swiper-slider'], null, 'all' );
	wp_enqueue_style( 'kedrm-jquery-validate', get_template_directory_uri() . '/assets/css/jquery_validate.css', array('kedrm-main-styles'), null, 'all' );
}


add_action( 'wp_enqueue_scripts', 'kedrm_scripts' );
function kedrm_scripts() {
	wp_enqueue_script( 'kedrm-navigation', get_template_directory_uri() . '/assets/js/navigation.js', array(), _S_VERSION, true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	wp_enqueue_script ('jquery-validate', get_template_directory_uri() . '/assets/js/jquery.validate.min.js', array('jquery'), null, true );
	wp_enqueue_script ('swiper-slider', "https://unpkg.com/swiper/swiper-bundle.min.js", null, null, true );
	wp_enqueue_script ('kedrm-is-mobile', get_template_directory_uri() . '/assets/js/is_mobile.js', null, null, true );
	wp_enqueue_script ('kedrm-main-scripts', get_template_directory_uri(  ) . '/assets/js/scripts.js', array('jquery', 'kedrm-ymap'), null, true );
	wp_enqueue_script ('kedrm-modal', get_template_directory_uri(  ) . '/assets/js/modal.js', array('jquery'), null, true );
	wp_enqueue_script ('kedrm-swiper', get_template_directory_uri(  ) . '/assets/js/slider-swiper.js', array('jquery',
	'kedrm-is-mobile'), null, true );

	wp_enqueue_script ('kedrm-faq-search', get_template_directory_uri(  ) . '/assets/js/faq-search.js', array('jquery'), null, true);
	wp_enqueue_script ('kedrm-ymap', 'http://api-maps.yandex.ru/2.1/?lang=ru_RU', null, null, true);
	wp_enqueue_script ('dynamic-adaptiv', get_template_directory_uri(  ) . '/assets/js/dynamicAdapt.js', null, null, true);

}


