<?php
if (!defined('ABSPATH')) {
    exit;
}

remove_filter('authenticate', 'wp_authenticate_username_password', 20, 3);

add_filter( 'authenticate', 'kedrm_authenticate_username_email_password', 20, 3 );
function kedrm_authenticate_username_email_password( $user, $username, $password ) {

	if ( is_email( $username ) ) {

		if ( !empty( $username ) ) {
			$user = get_user_by( 'email', $username );
		}

		if ( isset( $user->user_login, $user ) ) {
			$username = $user->user_login;
		}
	}

	return wp_authenticate_username_password( NULL, $username, $password );
}


function ajax_auth_init(){
	wp_enqueue_script( 'kedrm-ajax-auth', get_template_directory_uri() . '/assets/js/ajax-auth-script.js', array('jquery'), null, true );
   wp_localize_script( 'kedrm-ajax-auth', 'ajax_auth_object', array(
      'ajaxurl' => admin_url( 'admin-ajax.php' ),
		'redirecturl' => home_url(),
		'loadingmessage' => 'Ожидайте...',
	));

	// Enable the user with no privileges to run ajax_login() in AJAX
	add_action( 'wp_ajax_nopriv_ajaxlogin', 'ajax_login' );
	// Enable the user with no privileges to run ajax_register() in AJAX
	add_action( 'wp_ajax_nopriv_ajaxregister', 'ajax_register' );
}

// Execute the action only if the user isn't logged in
if (!is_user_logged_in()) {
    add_action('init', 'ajax_auth_init');
}

function ajax_login()
{

	if (!wp_verify_nonce( $_POST['nonce'], 'woocommerce-login')){
		echo json_encode(array('loggedin'=>false, 'message' => 'Запрос отправлен с кривого адреса log'));
		die();
	}

   // Nonce is checked, get the POST data and sign user on
  	// Call auth_user_login
	auth_user_login($_POST['username'], $_POST['password'], 'Login');
   die();
}

function ajax_register()
{
	if (!wp_verify_nonce( $_POST['nonce'], 'woocommerce-register')){
		echo json_encode(array('loggedin'=>false, 'message' => 'Запрос отправлен с кривого адреса reg'));
		die();
	}

   // Nonce is checked, get the POST data and sign user on
   $info = array();
  	$info['user_nicename'] = $info['nickname'] = $info['display_name'] = $info['first_name'] = $info['user_login'] = sanitize_user($_POST['username']) ;
   $info['user_pass'] = sanitize_text_field($_POST['password']);
	$info['user_email'] = sanitize_email( $_POST['email']);

	// Register the user
   $user_register = wp_insert_user( $info );
 	if ( is_wp_error($user_register) ){
		$error  = $user_register->get_error_codes()	;

		if (in_array('empty_user_login', $error)) {

			echo json_encode(array('loggedin'=>false, 'message'=>__($user_register->get_error_message('empty_user_login'))));

		} elseif(in_array('existing_user_login', $error)) {

			echo json_encode(array('loggedin'=>false, 'message'=> 'Такой пользователь уже зарегистрирован.'));

		} elseif(in_array('existing_user_email', $error)) {

        echo json_encode(array('loggedin'=>false, 'message'=> 'Такой email уже зарегистрирован.'));

		}
   } else {
	  	auth_user_login($info['nickname'], $info['user_pass'], 'Registration');
   }

   die();
}

function auth_user_login($user_login, $password, $login)
{
	$info = array();
   $info['user_login'] = $user_login;
   $info['user_password'] = $password;
   $info['remember'] = true;

	$user_signon = wp_signon( $info, '' ); // From false to '' since v4.9

	if ( is_wp_error($user_signon) ){
		echo json_encode(array('loggedin'=>false, 'message' => 'Неверная почта или пароль'));
   } else {
		wp_set_current_user($user_signon->ID);
		if ('Login' === $login) {
			$message = 'Авторизация прошла успешно...';
		} else {
			$message = 'Вы успешно зарегистрированы...';
		}
		echo json_encode(array('loggedin'=>true, 'message' => $message));
   }

	die();
}