<?php
if (!defined('ABSPATH')) exit;

add_action( 'init', 'kedrm_register_post_types' );
function kedrm_register_post_types(){
	register_post_type( 'kedrm_partner', [
		'label'  => null,
		'labels' => [
			'name'               => 'Наши партнёры',
			'singular_name'      => 'Наш партнёр',
			'add_new'            => 'Добавить нового', // для добавления новой записи
			'add_new_item'       => 'Добавление нового', // заголовка у вновь создаваемой записи в админ-панели.
			'edit_item'          => 'Редактирование партнёра', // для редактирования типа записи
			'new_item'           => 'Новый партнёр', // текст новой записи
			'view_item'          => 'Смотреть партнёра', // для просмотра записи этого типа.
			'search_items'       => 'Искать партнёра', // для поиска по этим типам записи
			'not_found'          => 'Не найдено', // если в результате поиска ничего не было найдено
			'not_found_in_trash' => 'Не найдено в корзине', // если не было найдено в корзине
			'parent_item_colon'  => '', // для родителей (у древовидных типов)
			'menu_name'          => 'Наши партнёры', // название меню
		],
		'description'         => '',
		'public'              => true,
		'show_in_menu'        => true, // показывать ли в меню адмнки
		'show_in_rest'        => null, // добавить в REST API. C WP 4.7
		'rest_base'           => null, // $post_type. C WP 4.7
		'menu_position'       => null,
		'menu_icon'           => 'dashicons-businessman',
		'capability_type'   	 => 'post',
		//'capabilities'      => 'post', // массив дополнительных прав для этого типа записи
		//'map_meta_cap'      => null, // Ставим true чтобы включить дефолтный обработчик специальных прав
		'hierarchical'        => false,
		'supports'            => [ 'title', 'thumbnail' ], // 'title','editor','author','thumbnail','excerpt','trackbacks','custom-fields','comments','revisions','page-attributes','post-formats'
		'taxonomies'          => [],
		'has_archive'         => false,
		'rewrite'             => true,
		'query_var'           => true,
	] );

	register_post_type( 'kedrm_article', [
		'label'  => null,
		'labels' => [
			'name'               => 'База знаний',
			'singular_name'      => 'Статья',
			'add_new'            => 'Добавить новую', // для добавления новой записи
			'add_new_item'       => 'Добавление новой', // заголовка у вновь создаваемой записи в админ-панели.
			'edit_item'          => 'Редактирование статьи', // для редактирования типа записи
			'new_item'           => 'Новая статья', // текст новой записи
			'view_item'          => 'Смотреть статью', // для просмотра записи этого типа.
			'search_items'       => 'Искать статью', // для поиска по этим типам записи
			'not_found'          => 'Не найдено', // если в результате поиска ничего не было найдено
			'not_found_in_trash' => 'Не найдено в корзине', // если не было найдено в корзине
			'parent_item_colon'  => '', // для родителей (у древовидных типов)
			'menu_name'          => 'Статьи', // название меню
		],
		'description'         => '',
		'public'              => true,
		'show_in_menu'        => true, // показывать ли в меню адмнки
		'show_in_rest'        => true, // добавить в REST API. C WP 4.7
		'rest_base'           => null, // $post_type. C WP 4.7
		'menu_position'       => null,
		'menu_icon'           => 'dashicons-format-aside',
		'capability_type'   	 => 'post',
		//'capabilities'      => 'post', // массив дополнительных прав для этого типа записи
		//'map_meta_cap'      => null, // Ставим true чтобы включить дефолтный обработчик специальных прав
		'hierarchical'        => false,
		'supports'            => [ 'title', 'thumbnail', 'editor', ],
		'taxonomies'          => [ 'product_cat', 'product_tag' ],
		'has_archive'         => true,
		'rewrite'             => true,
		'query_var'           => true,
	] );


}

