<?php
if (!defined('ABSPATH')) exit;

use Carbon_Fields\Container;
use Carbon_Fields\Field;

Container::make( 'theme_options', 'Theme options' )
->set_page_menu_title( 'Настройки темы' )
->set_icon( 'dashicons-hammer')
->add_tab('Логотипы', array(
	Field::make( 'separator', 'crb_separator_1', 'Логотипы для шапки' ),
	Field::make( 'image', 'kedrm_header_logo_big', 'Основной логотип для шапки' )
		->set_width(34),
	Field::make( 'image', 'kedrm_header_logo_middle', 'Основной уменьшенный логотип для шапки' )
		->set_width(33),
	Field::make( 'image', 'kedrm_header_logo_little', 'Маленький логотип для шапки' )
		->set_width(33),
	Field::make( 'separator', 'crb_separator_2', 'Логотипы для футера' ),
	Field::make( 'image', 'kedrm_footer_logo_big', 'Основной логотип для футера' )
		->set_width(50),
	Field::make( 'image', 'kedrm_footer_logo_middle', 'Основной уменьшенный логотип для футера' )
		->set_width(50),
))
->add_tab('Промо текст в карточке товара', array(
	Field::make( 'complex', 'kedrm_product_sale', 'Список слоганов')
		->add_fields([  
			Field::make( 'image', 'kedrm_product_sale_icon', 'Иконка' )
				->set_width(40),
			Field::make( 'text', 'kedrm_product_sale_text', 'Текст (html)' )
				->set_width(60),
		])
))
->add_tab('Контакты, социальные сети', array(
	Field::make( 'separator', 'crb_separator_3', 'Телефоны' ),
	Field::make( 'text', 'kedrm_phone', 'Городской телефон' )
		->set_width(50),
	Field::make( 'text', 'kedrm_phonedigits', 'Городской телефон (только цифры)' )
		->set_width(50),
	Field::make( 'text', 'kedrm_mobile', 'Мобильный' )
		->set_width(50),
	Field::make( 'text', 'kedrm_mobiledigits', 'Мобильный (только цифры)' )
		->set_width(50),
	Field::make( 'separator', 'crb_separator_4', 'Контакты' ),
	Field::make( 'text', 'kedrm_email', 'E-Mail' )
		->set_width(50),
	Field::make( 'text', 'kedrm_email_opt', 'E-Mail для опта' )
		->set_width(50),
	Field::make( 'text', 'kedrm_worktime', 'Время работы' )
		->set_width(50),
	Field::make( 'text', 'kedrm_address', 'Адрес' )
		->set_width(50),
	Field::make( 'separator', 'crb_separator_5', 'Социальные сети' ),
	Field::make( 'text', 'kedrm_vk', 'ВКонтакте' )
		->set_width(33),
	Field::make( 'text', 'kedrm_youtube', 'YouTube' )
		->set_width(34),
	Field::make( 'text', 'kedrm_instagram', 'Instagram' )
		->set_width(33),
))
->add_tab('Реквизиты', array(
	Field::make( 'text', 'kedrm_company_name', 'Название' ),
	Field::make( 'text', 'kedrm_inn', 'ИНН' )
		->set_width(50),
	Field::make( 'text', 'kedrm_kpp', 'КПП' )
		->set_width(50),
))
->add_tab('Изображения по умолчанию', array(
	Field::make( 'image', 'kedrm_default_video_thumb', 'Изображение заглушка для видео в галерее товара' )
		->set_width(50),
	Field::make( 'image', 'kedrm_default_catalog_default_thumb', 'Изображение, когда у товара нет картинки' )
		->set_width(50),
));
