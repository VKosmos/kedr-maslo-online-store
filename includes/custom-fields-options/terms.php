<?php
if (!defined('ABSPATH')) exit;

use Carbon_Fields\Container;
use Carbon_Fields\Field;

Container::make( 'term_meta', 'Category Properties', 'Дополнительные настройки' )
	->where( 'term_taxonomy', '=', 'product_cat' )
   ->add_fields( array(
      Field::make( 'text', 'kedrm_category_svg', 'SVG иконка' ),
   ) );
