<?php
if (!defined('ABSPATH')) exit;

use Carbon_Fields\Container;
use Carbon_Fields\Field;

Container::make( 'post_meta', 'kedrm_product_additional', 'Дополнительные данные' )
	->where('post_type', '=', 'product')
	->add_fields([
		Field::make('text', 'kedrm_product_model', 'Модель')
			->set_width(50),
		Field::make('text', 'kedrm_product_composition', 'Состав')
			->set_width(50),
		Field::make('text', 'kedrm_product_video-url', 'Видео для галереи')
			->set_width(50),
		Field::make('text', 'kedrm_product_video-index', 'Номер по порядку видео в галерее')
			->set_width(25),
		Field::make('image', 'kedrm_product_video-thumb', 'Изображение для превью видео')
			->set_width(25),
		Field::make('association', 'kedrm_products_recommendations', 'Рекомендуемые товары')
			->set_types([
				[
					'type' 		=> 'post',
					'post_type' => 'product',
				]
			])
	]);
