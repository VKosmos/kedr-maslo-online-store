<?php
if (!defined('ABSPATH')) exit;

use Carbon_Fields\Container;
use Carbon_Fields\Field;

/**
 * Страница Вопросы и ответы
 */
Container::make( 'post_meta', 'FAQ', 'Вопросы и ответы' )
	->where( 'post_type', '=', 'page' )
	->where( 'post_template', '=', 'templates/template-faq.php' )
	->add_tab('Наши товары', [
		Field::make( 'complex', 'kedrm_questions_products', 'Вопросы и ответы' )
			->add_fields( array(
				Field::make( 'text', 'kedrm_questions_products_title', 'Вопрос' )
					->set_width(60),
				Field::make( 'checkbox', 'kedrm_questions_products_check', 'Вывести в списке наиболее частых?' )
					->set_option_value( 'yes' )
					->set_width(40),
				Field::make( 'textarea', 'kedrm_questions_products_answer', 'Ответ' ),
			) )
	])
	->add_tab('Оформление заказа', [
		Field::make( 'complex', 'kedrm_questions_order', 'Вопросы и ответы' )
			->add_fields( array(
				Field::make( 'text', 'kedrm_questions_order_title', 'Вопрос' )
					->set_width(60),
				Field::make( 'checkbox', 'kedrm_questions_order_check', 'Вывести в списке наиболее частых?' )
					->set_option_value( 'yes' )
					->set_width(40),
				Field::make( 'text', 'kedrm_questions_order_answer', 'Ответ' ),
			) )
	]);

/**
 * Страница Клиенты
 */
Container::make( 'post_meta', 'kedrm_clients_additional', 'Долнительные настройки' )
	->where('post_template', '=', 'templates/template-clients.php')
	->add_tab('Система накопления скидок', [
		Field::make('complex', 'kedrm_clients_discounts', 'Скидки')
			->add_fields([
				Field::make('text', 'kedrm_clients_discounts_price', 'Сумма от')
					->set_width(50),
				Field::make('text', 'kedrm_clients_discounts_value', 'Процент скидки')
					->set_width(50),
			])
	])
	->add_tab('Прочее', [
		Field::make('text', 'kedrm_clients_banner_text', 'Текст баннера со скидкой')
	]);

Container::make( 'post_meta', 'kedrm_partners_data', 'Данные партнёра' )
	->where('post_type', '=', 'kedrm_partner')
	->add_fields([
		Field::make('complex', 'kedrm_partners_city', 'Города')
			->add_fields([
				Field::make('text', 'kedrm_partners_city_name', 'Название города')
					->set_width(50),
				Field::make('text', 'kedrm_partners_timework', 'Время работы')
					->set_width(50),
			Field::make('complex', 'kedrm_partners_address', 'Адреса')
				->add_fields([
					Field::make('text', 'kedrm_partnrrs_address_address', 'Адрес')
				])
				->set_width(34),
			Field::make('complex', 'kedrm_partners_phones', 'Телефоны')
				->add_fields([
					Field::make('text', 'kedrm_partnrrs_phones_phone', 'Телефон')
				])
				->set_width(33),
			Field::make('complex', 'kedrm_partners_emails', 'Почты')
				->add_fields([
					Field::make('text', 'kedrm_partnrrs_emails_email', 'e-mail')
				])
				->set_width(33),

			])

	]);

/**
 * Страница О нас
 */
Container::make( 'post_meta', 'kedrm_about_additional', 'Дополнительные настройки' )
	->where('post_template', '=', 'templates/template-about.php')
	->add_tab('Баннеры', [
		Field::make('image', 'kedrm_about_banner_top', 'Верхний баннер')
			->set_width(50),
		Field::make('image', 'kedrm_about_banner_bottom', 'Нижний баннер')
			->set_width(50),
	])
	->add_tab('Блок Дело начинается с идеи', [
		Field::make('textarea', 'kedrm_about_idea', 'Текст в блоке Дело начинается с идеи'),
	])
	->add_tab('Блок Чтобы радовать', [
		Field::make('complex', 'kedrm_about_please', 'Элементы')
			->add_fields([
				Field::make('image', 'kedrm_about_please_image', 'Иконка')
					->set_width(40),
				Field::make('textarea', 'kedrm_about_please_text', 'Текст')
					->set_width(60),
			])
	])
	->add_tab('Блок История', [
		Field::make('textarea', 'kedrm_about_history', 'Текст в блоке История')
	])
	->add_tab('Блок Наши реквизиты', [
		Field::make('textarea', 'kedrm_about_requisites', 'Текст в блоке Наши реквизиты')
	])
	->add_tab('Слайдер', [
		Field::make('complex', 'kedrm_about_slider', 'Слайды')
			->add_fields([
				Field::make('text', 'kedrm_about_slider_title', 'Заголовок слайда')
					->set_width(50),
				Field::make('image', 'kedrm_about_slider_image', 'Изображение слайда')
					->set_width(50),
				Field::make('complex', 'kedrm_about_slider_text', 'Тексты слайда')
					->add_fields([
						Field::make('text', 'kedrm_about_slider_text_item', 'Строка'),
					]),
			])

	]);


/**
 *	Главная страница
 */
Container::make( 'post_meta', 'main_page_additional', 'Дополнительные настройки' )
	->where( 'post_type', '=', 'page' )
	->where( 'post_template', '=', 'templates/template-main.php' )
	->add_tab('Верхний баннер', [
		Field::make( 'text', 'kedrm_main_banner_title', 'Заголовок')
			->set_width(50),
		Field::make( 'text', 'kedrm_main_banner_video', 'Ссылка на Youtube видео')
			->set_width(50),
		Field::make( 'complex', 'kedrm_main_banner_list', 'Список пунктов')
			->add_fields([
				Field::make('text', 'kedrm_main_banner_item', 'Текст'),
			]),
	])
	->add_tab('Наши преимущества', [
		Field::make( 'complex', 'kedrm_main_advantages', 'Список преимуществ' )
			->add_fields( array(
				Field::make( 'text', 'kedrm_main_advantages_title', 'Заголовок' )
					->set_width(60),
				Field::make( 'image', 'kedrm_main_advantages_image', 'Изображение' )
					->set_width(40),
			) )
	])
	->add_tab('Блок Натуральные продукты', [
		Field::make( 'text', 'kedrm_main_natural_title', 'Заголовок' )
			->set_width(50),
		Field::make( 'textarea', 'kedrm_main_natural_text', 'Текст блока' )
			->set_width(50),
	])
	->add_tab('Блок Популярные товары', [
		Field::make( 'text', 'kedrm_main_popular_title', 'Заголовок' )
			->set_width(50),
		Field::make( 'text', 'kedrm_main_popular_link', 'Текст ссылки Смотреть все' )
			->set_width(50),
		Field::make( 'association', 'kedrm_main_popular_products', 'Популярные товары')
			->set_types([
				[
					'type'		=> 'post',
					'post_type'	=> 'product',
				]
			])
	])
	->add_tab('Блок База знаний', [
		Field::make( 'text', 'kedrm_main_knowledge_title', 'Заголовок' )
			->set_width(50),
		Field::make( 'text', 'kedrm_main_knowledge_link', 'Текст ссылки Смотреть все' )
			->set_width(50),
		Field::make( 'association', 'kedrm_main_knowledge_articles', 'Связанные статьи')
			->set_types([
				[
					'type'		=> 'post',
					'post_type'	=> 'kedrm_article',
				]
			])
	])
	->add_tab('Слайдер', [
		Field::make('complex', 'kedrm_main_slider', 'Слайды')
			->add_fields([
				Field::make('text', 'kedrm_main_slider_title', 'Заголовок слайда')
					->set_width(60),
				Field::make('image', 'kedrm_main_slider_image', 'Изображение слайда')
					->set_width(40),
				Field::make('textarea', 'kedrm_main_slider_text', 'Текст слайда')
					->set_width(50),
				Field::make('text', 'kedrm_main_slider_link', 'Ссылка')
					->set_width(50),
			])

	]);


/**
 * post_type Article
 */
Container::make('post_meta', 'kedrm_article_additionals', 'Дополнительные настройки')
	->where('post_type', '=', 'kedrm_article')
	->add_tab('Список связанных товаров', [
		Field::make('association', 'kedrm_article_products', 'Связанные товары')
			->set_types([
				[
					'type' 		=> 'post',
					'post_type' => 'product',
				]
			])
	])
	->add_tab('Список связанных статей', [
		Field::make('association', 'kedrm_article_articles', 'Связанные статьи')
			->set_types([
				[
					'type' 		=> 'post',
					'post_type' => 'kedrm_article',
				]
			])
	])
	->add_tab('Подзаголовок', [
		Field::make('text', 'kedrm_article_headline', 'Текст подзаголовка статьи'),
	]);


/**
 * Shop page
 */
Container::make( 'post_meta', 'kedrm_shop_additional', 'Дополнительные настройки' )
	->where( 'post_type', '=', 'page' )
	->where( 'post_id', '=', '7' )
	->add_tab('Описание в нижней части страницы', [
		Field::make( 'text', 'kedrm_shop_bottom_title', 'Заголовок')
			->set_width(50),
		Field::make( 'textarea', 'kedrm_shop_bottom_text', 'Текст')
			->set_width(50),
		]
	);

