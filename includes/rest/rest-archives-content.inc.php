<?php
if (!defined('ABSPATH')) exit;

add_action( 'rest_api_init', 'kedrm_register_filter_articles_route' );
function kedrm_register_filter_articles_route()
{
	register_rest_route( 'articles/v1', 'filter', array(
		'methods' => WP_REST_SERVER::READABLE,
		'callback' => 'kedrm_archive_articles_get',
		'permission_callback' => '__return_true',
	) );
}

add_action( 'rest_api_init', 'kedrm_register_filter_product_route' );
function kedrm_register_filter_product_route()
{
	register_rest_route( 'catalog/v1', 'filter', array(
		'methods' => WP_REST_SERVER::READABLE,
		'callback' => 'kedrm_archive_products_get',
		'permission_callback' => '__return_true',
	) );
}


add_action( 'wp_enqueue_scripts', 'kedrm_enqueue_archive_articles_scripts' );
function kedrm_enqueue_archive_articles_scripts() {

 	wp_enqueue_script(
		'kedrm-archives-content',
		get_template_directory_uri() . '/assets/js/ajax-archive-content.js',
		array( 'jquery' ),
		time() // не кэшируем файл, убираем эту строчку после завершение разработки
	);

	wp_localize_script(
		'kedrm-archives-content',
		'JScontent',
		[
			'root_url'  => get_site_url(),
			'nonce'		=>	wp_create_nonce( 'yabba-kedrm-4-content' ),
		]
	);

}

/**
 * All for articles catalog
 */
function kedrm_archive_articles_get($args) {

	// check_ajax_referer('yabba-kedrm-4-content', 'wpnonce' );

	$cat_ids = (isset($args['cats'])) ? sanitize_text_field($args['cats']) : '';
	$cat_ids = ('all' !== $cat_ids) ? explode(',', $cat_ids) : $cat_ids;
	$tag_ids = (isset($args['tags'])) ? sanitize_text_field($args['tags']) : '';
	$tag_ids = ('all' !== $tag_ids) ? explode(',', $tag_ids) : $tag_ids;
	$paged = (isset($args['page'])) ? (int)sanitize_text_field( $args['page'] ) : 1;

	$tax_query = [ 'relation' => 'AND' ];

	if ('all' !== $cat_ids) {
		array_push($tax_query, [
			'taxonomy'	=> 'product_cat',
			'terms'		=> $cat_ids,
		]);
	}

	if ('all' !== $tag_ids) {
		array_push($tax_query, [
			'taxonomy'	=> 'product_tag',
			'terms'		=> $tag_ids,
		]);
	}

	$args = [
		'post_type'		=> 'kedrm_article',
		'post_status'	=> 'publish',
		'orderby'		=> 'title',
		'order'			=> 'ASC',
		'paged'			=> $paged,
		'tax_query'		=> $tax_query,
	];

	// return $args;

	$articles = new WP_Query($args);

	$max_pages = $articles->max_num_pages;

	$html = '';

	while ($articles->have_posts()) {
		$articles->the_post();

		ob_start();
		get_template_part( 'template-parts/archive-articles-single.tpl' );
		$html .= ob_get_clean();
	};

	$pagination = kedrm_archive_article_pagination2($paged, $max_pages);

	$data = [
		'html' 			=> $html,
		'pagination'	=> $pagination,
		'paged' 			=> $paged,
		'maxpages'		=> $max_pages,
	];

	return json_encode($data);
	die;

}

/**
 * All for products catalog
 */
function kedrm_archive_products_get($args) {

	// check_ajax_referer('yabba-kedrm-4-content', 'wpnonce' );

	$cat_ids = (isset($args['cats'])) ? sanitize_text_field($args['cats']) : '';
	$cat_ids = ('all' !== $cat_ids) ? explode(',', $cat_ids) : $cat_ids;
	$tag_ids = (isset($args['tags'])) ? sanitize_text_field($args['tags']) : '';
	$tag_ids = ('all' !== $tag_ids) ? explode(',', $tag_ids) : $tag_ids;
	$paged = (isset($args['page'])) ? (int)sanitize_text_field( $args['page'] ) : 1;

	$tax_query = [ 'relation' => 'AND' ];

	if ('all' !== $cat_ids) {
		array_push($tax_query, [
			'taxonomy'	=> 'product_cat',
			'terms'		=> $cat_ids,
		]);
	}

	if ('all' !== $tag_ids) {
		array_push($tax_query, [
			'taxonomy'	=> 'product_tag',
			'terms'		=> $tag_ids,
		]);
	}

	$args = [
		'post_type'		=> 'product',
		'post_status'	=> 'publish',
		'orderby'		=> 'title',
		'order'			=> 'ASC',
		'paged'			=> $paged,
		'tax_query'		=> $tax_query,
	];

	$products = new WP_Query($args);

	$max_pages = $products->max_num_pages;

	$html = '';

	while ($products->have_posts()) {
		$products->the_post();

		ob_start();
		get_template_part( 'template-parts/archive-products-single.tpl' );
		$html .= ob_get_clean();
	};

	$pagination = kedrm_shop_pagination2($paged, $max_pages);

	$data = [
		'html' 			=> $html,
		'pagination'	=> $pagination,
		'paged' 			=> $paged,
		'maxpages'		=> $max_pages,
	];

	return json_encode($data);
	die;
}