<?php
if (!defined('ABSPATH')) exit;

add_shortcode( 'article-linked-products', 'kedrm_shortcode_article_linked_products' );
function kedrm_shortcode_article_linked_products() {

	$article_id = get_the_ID();

	if (!$article_id) {
		return;
	}

	$products = carbon_get_post_meta($article_id, 'kedrm_article_products');

	if (!$products) return;

	foreach ($products as $k => $v) {
		if ('publish' !== get_post_status($v['id'])) {
			unset($products[$k]);
		}
	}

	$res = [];
	if (count($products) > 4) {
		$res = array_rand($products, 4);
	} else {
		$res = array_keys($products);
	}

	$low_height_class = (count($res) < 3) ? ' _low-height' : '';

	ob_start();
?>

	<div class="catalog__shortcode-wrapper <?php echo $low_height_class?>">

		<div class="catalog__full-width <?php echo $low_height_class?>">
			<div class="container">
				<h2 class="blog-product__title">Статья по товарам</h2>
				<div class="article__catalog catalog swiper-container catalog-slider">
					<ul class="catalog__list swiper-wrapper">

						<?php
						foreach($res as $v):
							$product_id = $products[$v]['id'];
							$product = wc_get_product($products[$v]['id']);
							$rel_product = $product;
							$price = $product->get_price();
							$short_description = get_the_excerpt( $product_id );

							$is_not_available = get_post_meta($product_id, '_stock_status', true) == 'outofstock';
							$is_available_class = ( $is_not_available ) ? ' product-body--no' : '';
							$is_sale_class = ($product->is_on_sale()) ? ' product-body--sale' : '';
							$add_to_cart_text = ( $is_not_available ) ? 'Сообщить о появлении' : 'Купить';

							$img_url = get_the_post_thumbnail_url($product_id, 'catalog-thumb');
							$img_url = ($img_url) ? $img_url : wp_get_attachment_image_url(carbon_get_theme_option('kedrm_default_catalog_default_thumb'), 'catalog-thumb');
							$alt = get_post_meta($product_id, '_wp_attachment_image_alt', true);

						?>

							<li class="catalog__item swiper-slide">
								<article class="catalog__product product-body <?php echo $is_available_class; echo $is_sale_class;?>">
									<div class="product-body__image-wrapper">
										<a href="<?php the_permalink($product_id); ?>">
											<img src="<?php echo $img_url;?>" alt="<?php echo $alt;?>" class="product-body__image">
										</a>
									</div>
									<div class="product-body__column">
										<?php
											include( get_template_directory() . '/woocommerce/includes/parts/wc-rating-stars.php');
										?>
										<h3 class="product-body__title">
											<a href="<?php the_permalink($product_id); ?>"><?php echo $product->get_title(); ?></a>
										</h3>
										<p class="product-body__text"><?php echo $short_description; ?></p>
										<div class="product-body__buy-wrapper">
											<span class="product-body__price"><?php echo $price . get_woocommerce_currency_symbol()?></span>

											<?php if (!$is_not_available):?>

												<a href="?add-to-cart=<?php echo $product_id; ?>" data-quantity="1" class="button product_type_simple add_to_cart_button ajax_add_to_cart product-body__buy" data-product_id="<?php echo $product_id; ?>" data-product_sku="<?php echo $product->get_sku()?>" aria-label="" rel="nofollow"><?php echo $add_to_cart_text?></a>

											<?php else:?>

												<div class="xoo-wl-btn-container xoo-wl-btc-simple xoo-wl-btc-popup ">
													<button type="button" data-product_id="<?php echo $id?>" class="xoo-wl-action-btn xoo-wl-open-form-btn button btn xoo-wl-btn-popup product-body__buy"><?php echo $add_to_cart_text;?></button>
												</div>

											<?php endif; ?>
										</div>
									</div>
								</article>
							</li>

						<?php endforeach; ?>

					</ul>

					<div class="catalog__slider-buttons-container"></div>

				</div>
			</div>
		</div>
	</div>

<?php
	$html = ob_get_clean();
	return $html;

}

