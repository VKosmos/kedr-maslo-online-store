<?php
/**
 * KedrMaslo functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package KedrMaslo
 */

if ( ! defined( '_S_VERSION' ) ) {
	// Replace the version number of the theme on each release.
	define( '_S_VERSION', '1.0.0' );
}

/**
 * Carbon field. Кастомные поля для темы, постов и таксономий
 */
use Carbon_Fields\Container;
use Carbon_Fields\Field;

add_action( 'carbon_fields_register_fields', 'crb_register_custom_fields' );
function crb_register_custom_fields() {
	require_once get_template_directory() . '/includes/custom-fields-options/theme-options.php';
	require_once get_template_directory() . '/includes/custom-fields-options/metaboxes.php';
	require_once get_template_directory() . '/includes/custom-fields-options/metaboxes-products.php';
	require_once get_template_directory() . '/includes/custom-fields-options/terms.php';
}

add_action( 'after_setup_theme', 'crb_load' );
function crb_load() {
   require_once get_template_directory() . '/includes/carbon-fields/vendor/autoload.php';
   \Carbon_Fields\Carbon_Fields::boot();
}

/**
 * Подключение настроек темы
 */
require_once get_template_directory() . '/includes/theme-settings.php';

/**
 * Подключение виджетов
 */
require_once get_template_directory() . '/includes/widget-areas.php';

/**
 * Подключение стилей и скриптов
 */
require_once get_template_directory() . '/includes/enqueue-scripts-styles.php';

/**
 * Создание кастомных типов постов
 */
require_once get_template_directory() . '/includes/register_post_types.php';

/**
 * Создание шорткодов
 */
require_once get_template_directory() . '/includes/register_shortcodes.php';

/**
 * Всё для меню
 */
require_once get_template_directory() . '/includes/navigation.inc.php';

/**
 * Всё для пагинации
 */
require_once get_template_directory() . '/includes/pagination.inc.php';

/**
 * Использование e-mail для регистрации пользователя
 */
require_once get_template_directory() . '/includes/register.php';

/**
 * Поиск ajax
 */
require_once get_template_directory() . '/includes/ajax-search.php';

/**
 *	Фильтрация статей
 */
require_once get_template_directory() . '/includes/rest/rest-archives-content.inc.php';

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/includes/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/includes/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/includes/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/includes/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/includes/jetpack.php';
}

/**
 * Load WooCommerce compatibility file.
 */
if ( class_exists( 'WooCommerce' ) ) {
	require get_template_directory() . '/includes/woocommerce.php';

	require get_template_directory() . '/woocommerce/includes/wc-functions-remove.php';
	require get_template_directory() . '/woocommerce/includes/wc-functions.php';
	require get_template_directory() . '/woocommerce/includes/wc-functions-single.php';
	require get_template_directory() . '/woocommerce/includes/wc-functions-catalog.php';
	require get_template_directory() . '/woocommerce/includes/wc-functions-cart.php';
	require get_template_directory() . '/woocommerce/includes/wc-functions-checkout.php';
	require get_template_directory() . '/woocommerce/includes/wc-functions-my-account.php';
}
