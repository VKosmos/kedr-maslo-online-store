<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package KedrMaslo
 */

get_header(null, ['page-standard']);
?>

<section class="standard">
	<div class="container">
		<?php kedrm_breadcrumbs(); ?>

		<h1 class="standard__title"><?php the_title(); ?></h1>

		<?php the_content(); ?>

	</div>
</section>

<?php

get_footer();
