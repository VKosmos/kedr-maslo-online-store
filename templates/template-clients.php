<?php
/**
 * The Contacts page template
 *
 * Template Name: Клиенты
 *
 * @package KedrMaslo
 */

get_header(null, ['clients']);
?>

		<section class="clients">
			<div class="container">

				<?php kedrm_breadcrumbs(); ?>

				<div class="clients__wrapper">
					<h1 class="clients__title"><?php the_title(); ?></h1>
					<p class="clients__headline">Добро пожаловать на официальный сайт компании “ЛадоЯр”</p>

					<div class="clients__list-wrapper list-clients">
						<h2 class="list-clients__title">Заказ продукции "ЛадоЯр"</h2>
						<ol class="list-clients__list">
							<li class="list-clients__item">
								<p>Добавьте товар в "Корзину покупок";</p>
							</li>
							<li class="list-clients__item">
								<p>Перейдите в "Корзину покупок" и выберите "Оформить заказ";</p>
							</li>
							<li class="list-clients__item">
								<p>Оформите заказ, обязательно заполнив все поля;</p>
							</li>
							<li class="list-clients__item">
								<p>Убедитесь, что все данные заполнены правильно и выберите "Подтвердить заказ";</p>
							</li>
							<li class="list-clients__item">
								<p>После оформления заказа, по почте с Вами свяжется наш менеджер для уточнения деталей
									заказа, способа оплаты и доставки;</p>
							</li>
							<li class="list-clients__item">
								<p>Далее Ваш заказ поступает в сборку, упаковку и оформления на доставку.</p>
							</li>
							<li class="list-clients__item">
								<p>Вам придёт письмо от платежного агрегатора <b>"Яндекс.касса" с реквизитами ООО "Радоград"</b>, где необходимо будет нажать на кнопку - <b>заплатить</b> и после перехода на платёжную страницу сможете оплатить с любой карты без комиссии.</p>
							</li>
							<li class="list-clients__item">
								<p>Письмо приходит с почты <a
										href="mailto:inform@money.yandex.ru."><i>inform@money.yandex.ru.</i></a>
									Отвечать на
									это письмо не нужно.</p>
							</li>
							<li class="list-clients__item">
								<p>Подтверждать оплату нет необходимости т.к. оплату мы сразу же увидим. Выставление
									счета так же возможно в СМС на номер сотового телефона.</p>
							</li>
							<li class="list-clients__item">
								<p><b>Номер отслеживания посылки</b>, а так же стоимость доставки <b>указан в счете.</b>
								</p>
							</li>
							<li class="list-clients__item">
								<p>После оплаты Вам придет на телефон чек.</p>
							</li>
							<li class="list-clients__item">
								<p>Отправка посылки происходит как правило в день оплаты либо на следующий, но если Вы
									оплатили в пятницу во второй половине дня то есть вероятность отправки только после
									выходных.</p>
							</li>
							<li class="list-clients__item">
								<p>Статус доставки Вы можете отслеживать по номеру отправки.</p>
							</li>
						</ol>
					</div>

					<div class="clients-sale">
						<h2 class="clients-sale__title">Розничная система накопления скидок</h2>
						<p class="clients-sale__text">В накоплении участвуют любые суммы заказов с учетом действующей
							скидки.</p>

						<?php
							$discs = carbon_get_post_meta(get_the_ID(), 'kedrm_clients_discounts');
							if ($discs):?>
								<div class="clients-sale__flex-wrapper">

								<?php foreach ($discs as $v):?>
									<div class="clients-sale__flex-element">
										<span class="clients-sale__price"><?= $v['kedrm_clients_discounts_price']?> p.</span>
										<span class="clients-sale__sale">-<?= $v['kedrm_clients_discounts_value']?>%</span>
									</div>
								<?php endforeach;?>

								</div>
							<?php
							endif;
						?>

						<p class="clients-sale__text">Подтверждение накопленной скидки - 1 заказ в течении 100 дней на
							сумму не менее 1000 руб. </p>
						<p class="clients-sale__text"> На нашем сайте реализован <span>автоматический</span> расчет
							накопительной скидки но если он поставил неверную скидку, не переживайте - менеджер
							обязательно все учтет и поставит верную скидку!))</p>

						<div class="clients-sale__bg">
							<p class="clients-sale__title-two"><?=carbon_get_post_meta(get_the_ID(), 'kedrm_clients_banner_text');?></p>
						</div>

						<p class="clients-sale__text clients-sale__text--mobile">Запросить оптовые цены и условия
							сотрудничества Вы можете написав на эл.почту <a href="mailto:<?php echo carbon_get_theme_option('kedrm_email_opt') ?>"><?php echo carbon_get_theme_option('kedrm_email_opt') ?></a></p>
					</div>


					<div class="clients__delivery">
						<p>Наложенным платежом не отправляем, т.к. часто клиент не приходит за посылкой и компания платит за доставку в оба конца, а также данная продукция могла бы быть отправлена другому клиенту.</p>
						<p>Все заказы отправляются только после 100% предоплаты.</p>
						<p>Бесплатной доставки нет но так как заключен договор со СДЭКом цена на доставку минимально допустимая.
						Способы доставки:</p>
						<p><b>Способы доставки:</b></p>
						<ul>
							<li>1. «СДЭК» (услуги перевозки оплачиваются при получении посылки). Во многих городах России действуют пункты выдачи заказов (ПВЗ). Наш менеджер может подобрать ближайший ПВЗ к указанному адресу. Как правило выбирается доставка склад-склад  (С-С) но возможен вариант доставки до двери (С-Д). Зачастую сдеком дешеле чем почтой но бывают исключения.</li>
							<li>2. Почтой России (услуги перевозки оплачиваются ВМЕСТЕ с заказом). При получении ни чего платить не надо.</li>
							<li>3. Транспортными компаниями по выбору покупателя (подходит для международных отправок и для крупных посылок)
						ВНИМАНИЕ: мы не работаем с компаниями "DPD" и "Boxberry"!</li>
						</ul>
						<p>Доставка по Екатеринбургу  для розничных и оптовых клиентов  осуществляется только силами курьерской службы «СДЭК»:</p>
						<p>При получении заказа в присутствии посыльного проверьте соответствие продукции перечню накладной, целостность упаковки каждого продукта.</p>
						<p>В случае несоответствия содержимого посылки накладной или повреждений тары вы вправе отказаться от заказа, вернув его курьеру.</p>
						<p><b>Что такое ЖУ?</b></p>
						<p>Жесткая упаковка - используется при отправке оптовых заказов по просьбе клиента но только при отправке через ТРАНСПОРТНЫЕ КОМПАНИИ.</p>
						<p>Напомним, что "СДЭК" не является таковой а позиционируется как курьерская служба.</p>
						<p>Вы можете не переживать т.к. каждая посылка тщательно упаковывается в картонную коробку, вся стеклянная и хрупкая тара завернута в специальный мягкий материал и завернута в стрейч-пленку. Дно так же защищено, а между стенкой коробки и продукцией обязательно проложена упаковочная бумага слоем не менее 1 см.</p>
						<p>Если клиент не оплачивает заказ в течение недели с момента отправки счета по электронной почте, заказ считается недействительным и расформировывается.</p>
						<p><b>Самовывоз:</b></p>
						<p>Возможен самовывоз с нашего склада в Екатеринбурге, расположенного по адресу:</p>
						<p>пр-т Космонавтов, д. 20, здание "Часовой Молл", 3 этаж, метро Машиностроителей,
						выход в сторону завода имени "Калинина" (ЗиК).</p>
						<p>Самовывоз возможен только по предварительной договорённости с менеджером.</p>
					</div>


				</div><!-- .clients__wrapper -->


			</div><!-- .container -->


		</section><!-- .clients -->

		<section class="our-partners">
			<div class="container">
				<h2 class="our-partners__title">Наши партнеры</h2>

				<?php
					$partners = get_posts( array(
						'numberposts' => -1,
						'orderby'     => 'date',
						'order'       => 'ASC',
						'post_type'   => 'kedrm_partner',
						'suppress_filters' => true,
					) );

					if ($partners):?>

					<ul class="our-partners__list">

					<?php foreach( $partners as $v ):?>

						<li class="our-partners__item">
							<div class="our-partners__image-wrapper">
								<img src="<?php echo get_the_post_thumbnail_url( $v->ID, 'partners-thumb')?>" alt="" class="our-partners__image">
							</div>
							<h3 class="our-partners__headline"><?php echo $v->post_title; ?></h3>
							<div class="our-partners__adress-wrapper">
								<?php
									$cities = carbon_get_post_meta($v->ID, 'kedrm_partners_city');

									if ($cities):
										foreach ($cities as $w): ?>
											<p class="our-partners__adress"><?=$w['kedrm_partners_city_name']?></p>

											<?php
												// Адреса
												if ($w['kedrm_partners_address']):
													foreach ($w['kedrm_partners_address'] as $adr):?>

												<div class="our-partners__map-wrapper">
													<img src="<?= get_template_directory_uri() . '/assets/img/icon/partners-icon-map.svg' ?>" alt="" class="our-partners__map">
													<p class="our-partners__street"><?=$adr['kedrm_partnrrs_address_address']?></p>
												</div>

											<?php	endforeach;
												endif; ?>

											<?php
												// Время работы
												if ($w['kedrm_partners_timework']):?>

												<div class="our-partners__time-wrapper">
													<img src="<?= get_template_directory_uri() . '/assets/img/icon/icon-time.svg' ?>" alt="" class="our-partners__map">
													<p class="our-partners__work"><?=$w['kedrm_partners_timework']?></p>
												</div>

												<?php endif; ?>

											<?php
												// Телефоны
												if ($w['kedrm_partners_phones']):
													foreach ($w['kedrm_partners_phones'] as $ph):?>

												<div class="our-partners__call-wrapper">
													<img src="<?= get_template_directory_uri() . '/assets/img/icon/partners-icon-call.svg' ?>" alt="" class="our-partners__call">
													<p class="our-partners__phone"><?=$ph['kedrm_partnrrs_phones_phone']?></p>
												</div>

											<?php	endforeach;
												endif; ?>

											<?php
												// E-Mail
												if ($w['kedrm_partners_emails']):
													foreach ($w['kedrm_partners_emails'] as $em):?>

												<div class="our-partners__mail-icon-wrapper">
													<img src="<?= get_template_directory_uri() . '/assets/img/icon/partners-icon-mail.svg' ?>" alt="" class="our-partners__mail-icon">
													<p class="our-partners__mail"><?=$em['kedrm_partnrrs_emails_email']?></p>
												</div>

											<?php	endforeach;
												endif; ?>

										<?php
										endforeach;
									endif;

								?>

							</div>
						</li>

					<?php endforeach;?>
					</ul>
					<?php endif;?>

<?php

get_footer();
