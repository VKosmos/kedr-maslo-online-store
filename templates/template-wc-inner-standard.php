<?php
/**
 * The woocommerce inner pages template
 *
 * Template Name: Шаблон для внутренних страниц Woocommerce
 *
 * @package KedrMaslo
 */

get_header(null, ['page-standard']);
?>

<section class="standard">
	<div class="container">
		<?php kedrm_breadcrumbs(); ?>

		<?php
			if (is_wc_endpoint_url('lost-password')):
				$title = 'Восстановление пароля';
			else:
				$title = get_the_title();
			endif;
		?>

		<h1 class="standard__title standard__title--left"><?php echo $title; ?></h1>


		<?php the_content();?>

	</div>
</section>

<?php

get_footer();
