<?php
/**
 * The Contacts page template
 *
 * Template Name: Ч.А.В.О.
 *
 * @package KedrMaslo
 */

get_header(null, ['faq']);
?>

	<section class="questions">
		<div class="container">

			<?php kedrm_breadcrumbs(); ?>

			<h1 class="questions__title"><?php the_title(); ?></h1>
			<p class="questions__headline">Часто задаваемые вопросы и наши ответы</p>

			<form action="<?php esc_url(home_url('/')); ?>" class="questions__search" method="post">
				<input type="text" class="questions__search-input" placeholder="Поиск по вопросам"
					aria-label="Текст для поиска по вопросам" name="s" value="<?php get_search_query(); ?>">
				<span class="questions__search-search"></span>
				<button class="questions__search-close" type="reset"></button>
				<button class="questions__search-button" type="submit">Найти</button>
			</form>



			<div class="questions__popular popular-questions">
				<h2 class="popular-questions__title">Наиболее часто задаваемые вопросы</h2>

				<div class="popular-questions__wrapper">
					<div class="popular-questions__block">
						<div class="popular-questions__img-text">
							<div class="popular-questions__image-wrapper">
								<img src="<?php echo get_template_directory_uri() . '/assets/img/icon/whiskey.svg'?>" alt="" class="popular-questions__image">
							</div>
							<h3 class="popular-questions__text">Наши продукты</h3>
						</div>
						<div class="popular-questions__list-wrapper">
							<?php

							$faqs_products = carbon_get_post_meta( get_the_ID(), 'kedrm_questions_products' );

							if ($faqs_products):
								$i = 1;?>

								<ul class="popular-questions__list">
									<?php
									foreach ($faqs_products as $v):
										if (1 == $v['kedrm_questions_products_check']): ?>
											<li class="popular-questions__item">
												<a data-anchor="qp<?php echo $i; ?>" class="popular-questions__link _faq-link"><?php echo $v['kedrm_questions_products_title']; ?></a>
											</li>
									<?php
										endif;
										$i++;
									endforeach; ?>

								</ul>

								<?php
							endif;
							?>
							</ul>
						</div>
					</div>

					<div class="popular-questions__block">
						<div class="popular-questions__img-text">
							<div class="popular-questions__image-wrapper">
								<img src="<?php echo get_template_directory_uri() . '/assets/img/icon/credit-card.svg'?>" alt="" class="popular-questions__image">
							</div>
							<h3 class="popular-questions__text">Оформление заказа</h3>
						</div>
						<?php
							$faqs_orders = carbon_get_post_meta( get_the_ID(), 'kedrm_questions_order' );
							if ($faqs_orders):
								$i = 1;?>

								<ul class="popular-questions__list">

									<?php
									foreach ($faqs_orders as $v):
										if (1 == $v['kedrm_questions_order_check']): ?>
											<li class="popular-questions__item">
												<a data-anchor="qo<?php echo $i;?>" class="popular-questions__link _faq-link"><?php echo $v['kedrm_questions_order_title']; ?></a>
											</li>
									<?php
										endif;
										$i++;
									endforeach; ?>

								</ul>

								<?php
							endif;
							?>
					</div>
				</div>
			</div>

			<div class="questions__answer answer">
				<h2 class="answer__title">Ответы на вопросы</h2>
				<div class="answer__flex-wrapper">
					<div class="answer__flex-element">
						<div class="answer__image-text">
							<div class="answer__image-wrapper">
								<img src="<?php echo get_template_directory_uri() . '/assets/img/icon/whiskey.svg'?>" alt="" class="answer__image">
							</div>
							<h3 class="answer__text">Наши продукты</h3>
						</div>

						<?php
							$i = 1;
							if ($faqs_products):
								foreach ($faqs_products as $k => $v):
									?>
										<div class="_faq-search">
											<h4 data-target="qp<?php echo $i; ?>" class="answer__headline"><?php echo $v['kedrm_questions_products_title'] ?></h4>
											<div class="answer__info">
												<p><?php echo $v['kedrm_questions_products_answer'] ?></p>
											</div>
										</div>
									<?php
									$i++;
								endforeach;
							endif;
						?>
					</div>
					<div class="answer__flex-element">
						<div class="answer__image-text">
							<div class="answer__image-wrapper">
								<img src="<?php echo get_template_directory_uri() . '/assets/img/icon/credit-card.svg'?>" alt="" class="answer__image">
							</div>
							<h3 class="answer__text">Оформление заказа</h3>
						</div>
						<?php
							$i = 1;
							if ($faqs_orders):
								foreach ($faqs_orders as $k => $v):
									?>
										<div class="_faq-search">
											<h4 data-target="qo<?php echo $i; ?>" class="answer__headline"><?php echo $v['kedrm_questions_order_title'] ?></h4>
											<div class="answer__info">
												<p><?php echo $v['kedrm_questions_order_answer'] ?></p>
											</div>
											</div>
										<?php
									$i++;
								endforeach;
							endif;
						?>
					</div>
				</div>
			</div>

			<div class="questions__contacts contacts-questions">
				<h2 class="contacts-questions__title">Не нашли ответа на свой вопрос? Мы с удовольствием поможем
					вам.</h2>
				<p class="contacts-questions__text">Свяжитесь с нашими консультантами!</p>
				<div class="contacts-questions__flex">
					<a href="tel:<?php echo carbon_get_theme_option('kedrm_phonedigits'); ?>" class="contacts-questions__link">
						<div class="contacts-questions__icon-wrapper">
							<img src="<?php echo get_template_directory_uri() . '/assets/img/icon/call-red.svg' ?>" alt=""
								class="contacts-questions__icon contacts-questions__icon--call">
						</div>
						<?php echo carbon_get_theme_option('kedrm_phone'); ?>
					</a>
					<a href="tel:<?php echo carbon_get_theme_option('kedrm_mobiledigits'); ?>" class="contacts-questions__link">
						<span class="contacts-questions__icon-wrapper">
							<img src="<?php echo get_template_directory_uri() . '/assets/img/icon/smartphone-red.svg' ?>" alt=""
								class="contacts-questions__icon contacts-questions__icon--phone">
						</span>
						<?php echo carbon_get_theme_option('kedrm_mobile'); ?>
					</a>
					<a href="mailto:<?php echo carbon_get_theme_option('kedrm_email'); ?>" class="contacts-questions__link">
						<span class="contacts-questions__icon-wrapper">
							<img src="<?php echo get_template_directory_uri() . '/assets/img/icon/mail-red.svg' ?>" alt=""
								class="contacts-questions__icon contacts-questions__icon--mail">
						</span>
						<?php echo carbon_get_theme_option('kedrm_email'); ?>
					</a>
				</div>
			</div>

			<div class="questions__connection connection">
				<h2 class="connection__title">Форма обратной связи</h2>
				<?php
					echo do_shortcode( '[wpforms id="106" title="false"]' );
				?>
			</div>
		</div>
	</section>


<?php

get_footer();
