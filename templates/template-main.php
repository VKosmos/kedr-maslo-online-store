<?php
/**
 * The Main page template
 *
 * Template Name: Главная страница
 *
 * @package KedrMaslo
 */

get_header(null, ['main-page']);
?>

<section class="top-bar">
	<div class="container">
		<div class="top-bar__background"></div>

		<div class="top-bar__info">
			<h1 class="top-bar__title">
				<?php echo carbon_get_post_meta(get_the_ID(), 'kedrm_main_banner_title');?>
			</h1>
			<ul class="top-bar__list">
				<?php
					$items = carbon_get_post_meta(get_the_ID(), 'kedrm_main_banner_list');
					if ($items):
						foreach($items as $v):?>
						<li class="top-bar__item"><?php echo $v['kedrm_main_banner_item'] ?></li>
						<?php
						endforeach;
					endif;
				?>
			</ul>
			<div class="top-bar__button-flex">
				<a href="<?php the_permalink(7); ?>" class="top-bar__show-product">Cмотреть продукцию</a>
				<button class="top-bar__video-play" data-vkpath="main-video" data-animation="fade" data-speed="300"></button>
			</div>
		</div>

	</div>
</section>

<section class="advantages">
	<div class="container">
		<h2 class="advantages__title">Наши преимущества</h2>
		<ul class="advantages__list">
			<?php
				$advs = carbon_get_post_meta(get_the_ID(), 'kedrm_main_advantages');
				if ($advs):
					foreach ($advs as $v):?>
				<li class="advantages__item">
					<div class="advantages__image-wrapper">
						<img src="<?php echo wp_get_attachment_image_url($v['kedrm_main_advantages_image']) ?>" alt="" class="advantages__image">
					</div>
					<p class="advantages__text"><?php echo $v['kedrm_main_advantages_title'] ?></p>
				</li>
			<?php endforeach;
				endif;
			?>
		</ul>
		<div class="line-delimeter">
			<div class="line-delimeter__body">
				<span class="line-delimeter__line"></span>
				<span class="line-delimeter__rectangle line-delimeter__rectangle--one"></span>
				<span class="line-delimeter__rectangle line-delimeter__rectangle--two"></span>
				<span class="line-delimeter__rectangle line-delimeter__rectangle--three"></span>
				<span class="line-delimeter__rectangle line-delimeter__rectangle--four"></span>
			</div>
		</div>
	</div>
</section>

<section class="natural">
	<div class="container">
		<h2 class="natural__title"><?php echo carbon_get_post_meta(get_the_ID(), 'kedrm_main_natural_title')?></h2>
		<div class="natural__flex-wrapper">
			<p class="natural__text"><?php echo carbon_get_post_meta(get_the_ID(), 'kedrm_main_natural_text')?></p>
		</div>
		<div class="line-delimeter">
			<div class="line-delimeter__body">
				<span class="line-delimeter__line"></span>
				<span class="line-delimeter__rectangle line-delimeter__rectangle--one"></span>
				<span class="line-delimeter__rectangle line-delimeter__rectangle--two"></span>
				<span class="line-delimeter__rectangle line-delimeter__rectangle--three"></span>
				<span class="line-delimeter__rectangle line-delimeter__rectangle--four"></span>
			</div>
		</div>
	</div>
</section>

<section class="our-products">
	<div class="container">
		<h2 class="our-products__title">Наша продукция</h2>
		<div class="our-products__flex-wrapper our-products-slider swiper-container">
			<ul class="our-products__list swiper-wrapper">
				<?php

					$args = [
						'taxonomy'		=> 'product_cat',
						'hide_empty' 	=> 'false',
						'exclude'		=> 15,
					];
					$cats = get_categories( $args );

					if ($cats):
						foreach ($cats as $v):
							$id = $v->term_id;
							$img_id = get_term_meta( $id, 'thumbnail_id', true );
							$img_url = wp_get_attachment_image_url( $img_id, 'category-list');
						?>

							<li class="our-products__item swiper-slide">
								<div class="our-products__body" style="background-image: url(<?php echo $img_url; ?>)">
									<a href="<?php echo get_category_link($id) ?>" class="our-products__link"></a>
									<h3 class="our-products__text"><?php echo $v->name; ?></h3>
								</div>
							</li>

						<?php
						endforeach;
					endif;
				?>

			</ul>

			<div class="our-products__slider-buttons-container"></div>
		</div><!-- .our-products__flex-wrapper .swiper-container -->

		<div class="line-delimeter">
			<div class="line-delimeter__body">
				<span class="line-delimeter__line"></span>
				<span class="line-delimeter__rectangle line-delimeter__rectangle--one"></span>
				<span class="line-delimeter__rectangle line-delimeter__rectangle--two"></span>
				<span class="line-delimeter__rectangle line-delimeter__rectangle--three"></span>
				<span class="line-delimeter__rectangle line-delimeter__rectangle--four"></span>
			</div>
		</div>

	</div>
</section>

<section class="catalog">
	<div class="container">
		<h2 class="catalog__title"><?php echo carbon_get_post_meta(get_the_ID(), 'kedrm_main_popular_title') ?></h2>

		<div class="catalog__all-container">
			<div class="catalog__switcher-wrapper">

				<button class="catalog__switcher catalog__switcher--horizontal" data-catalog="horizontal">
					<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
						<path fill-rule="evenodd" clip-rule="evenodd"
							d="M20 3C20.5523 3 21 3.44772 21 4L21 10C21 10.5523 20.5523 11 20 11L4 11C3.44772 11 3 10.5523 3 10L3 4C3 3.44771 3.44772 3 4 3L20 3ZM19 5L5 5L5 9L19 9L19 5Z"
							fill="#C4C4C4" />
						<path fill-rule="evenodd" clip-rule="evenodd"
							d="M20 13C20.5523 13 21 13.4477 21 14L21 20C21 20.5523 20.5523 21 20 21L4 21C3.44772 21 3 20.5523 3 20L3 14C3 13.4477 3.44772 13 4 13L20 13ZM19 15L5 15L5 19L19 19L19 15Z"
							fill="#C4C4C4" />
					</svg>
				</button>

				<button class="catalog__switcher catalog__switcher--tile js-active" data-catalog="vertical">
					<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
						<path fill-rule="evenodd" clip-rule="evenodd"
							d="M3 4C3 3.44772 3.44772 3 4 3L10 3C10.5523 3 11 3.44772 11 4L11 20C11 20.5523 10.5523 21 10 21H4C3.44772 21 3 20.5523 3 20L3 4ZM5 5L5 19H9L9 5L5 5Z"
							fill="#C4C4C4" />
						<path fill-rule="evenodd" clip-rule="evenodd"
							d="M13 4C13 3.44772 13.4477 3 14 3L20 3C20.5523 3 21 3.44772 21 4L21 20C21 20.5523 20.5523 21 20 21H14C13.4477 21 13 20.5523 13 20L13 4ZM15 5L15 19H19L19 5L15 5Z"
							fill="#C4C4C4" />
					</svg>
				</button>

			</div>
			<a href="<?php echo get_home_url() . '/shop/'; ?>" class="catalog__all-link"><?php echo carbon_get_post_meta(get_the_ID(), 'kedrm_main_popular_link') ?></a>
		</div>

		<div class="catalog__catalog-container swiper-container catalog-slider">

			<?php
			$products = carbon_get_post_meta(get_the_ID(), 'kedrm_main_popular_products');
			if ($products):

				if (count($products) > 12):
					$keys = array_rand($products, 12);
				else:
					$keys = array_keys($products);
				endif;

			?>
			<ul class="catalog__list swiper-wrapper">
				<?php foreach($keys as $v):
					$id = $products[$v]['id'];
					if ('publish' !== get_post_status($id)) continue;
					get_template_part( 'template-parts/related-product-list-item', null, [$id] );
				endforeach; ?>
			</ul>
			<?php endif;?>

			<div class="catalog__slider-buttons-container"></div>
		</div><!-- .catalog__catalog-container -->

		<div class="line-delimeter">
			<div class="line-delimeter__body">
				<span class="line-delimeter__line"></span>
				<span class="line-delimeter__rectangle line-delimeter__rectangle--one"></span>
				<span class="line-delimeter__rectangle line-delimeter__rectangle--two"></span>
				<span class="line-delimeter__rectangle line-delimeter__rectangle--three"></span>
				<span class="line-delimeter__rectangle line-delimeter__rectangle--four"></span>
			</div>
		</div>
	</div>
</section>

<section class="knowledge">
	<div class="container">

		<h2 class="knowledge__title"><?php echo carbon_get_post_meta(get_the_ID(), 'kedrm_main_knowledge_title')?></h2>

		<div class="knowledge__all-container">
			<a href="<?php echo get_post_type_archive_link('kedrm_article'); ?>" class="knowledge__all-link"><?php echo carbon_get_post_meta(get_the_ID(), 'kedrm_main_knowledge_link')?></a>
		</div>

		<div class="knowledge__slider-container swiper-container knowledge-slider">
			<?php
				$articles = carbon_get_post_meta(get_the_ID(), 'kedrm_main_knowledge_articles');
				if ($articles):

					if (count($articles) > 4):
						$keys = array_rand($articles, 4);
					else:
						$keys = array_keys($articles);
					endif; ?>

					<ul class="knowledge__list swiper-wrapper">
					<?php
						foreach ($keys as $v):
							$id = $articles[$v]['id'];

							$img_url = get_the_post_thumbnail_url($id, 'catalog-thumb');
							$img_url = ($img_url) ? $img_url : wp_get_attachment_image_url(carbon_get_theme_option('kedrm_default_catalog_default_thumb'), 'catalog-thumb');
							$alt = get_post_meta($id, '_wp_attachment_image_alt', true);
						?>
							<li class="knowledge__item swiper-slide">
								<article class="knowledge__article article-knowledge">
									<a href="<?php the_permalink($id);?>" class="article-knowledge__link"></a>
									<div class="article-knowledge__image-wrapper">
										<img src="<?php echo $img_url;?>" alt="<?php echo $alt;?>" class="article-knowledge__image">
									</div>
									<h3 class="article-knowledge__title"><?php echo get_the_title($id);?></h3>
									<p class="article-knowledge__text"><?php echo get_the_excerpt($id);?></p>
									<a href="<?php the_permalink($id);?>" class="article-knowledge__link"></a>
								</article>
							</li>
					<?php endforeach; ?>
					</ul>
			<?php
				endif; ?>
		</div>

		<div class="knowledge__slider-buttons-container"></div>

		<div class="line-delimeter">
			<div class="line-delimeter__body">
				<span class="line-delimeter__line"></span>
				<span class="line-delimeter__rectangle line-delimeter__rectangle--one"></span>
				<span class="line-delimeter__rectangle line-delimeter__rectangle--two"></span>
				<span class="line-delimeter__rectangle line-delimeter__rectangle--three"></span>
				<span class="line-delimeter__rectangle line-delimeter__rectangle--four"></span>
			</div>
		</div>
	</div>
</section>

<section class="main-slider">
	<div class="container">
		<div class="main-slider__flex-wrapper">

			<div class="main-slider__big-slider-wrapper swiper-container main-big-slider">

				<?php
					$slides = carbon_get_post_meta(get_the_ID(), 'kedrm_main_slider');
					if ($slides):
						$i = 0; ?>

						<div class="main-slider__big big-slider swiper-wrapper">
						<?php foreach($slides as $v):?>

							<div class="big-slider__image-wrapper swiper-slide" data-slide-index="<?=$i?>">
								<img src="<?php echo wp_get_attachment_image_url($v['kedrm_main_slider_image'], 'banner-image'); ?>" alt="" class="big-slider__image">
								<div class="big-slider__body">
									<h3><?php echo $v['kedrm_main_slider_title']; ?></h3>
									<div><?php echo $v['kedrm_main_slider_text']; ?></div>
									<a href="<?php echo $v['kedrm_main_slider_link']; ?>">Подробнее</a>
								</div>
							</div>

						<?php
								$i++;
							endforeach;?>
						</div>
					<?php endif; ?>
			</div><!-- .swiper-container -->

			<div class="main-slider__content-wrapper">

				<div thumbsSlider="" class="main-slider__min-slider-wrapper swiper-container main-min-sliderr">
					<?php if ($slides):?>
					<div class="swiper-wrapper">
						<?php foreach($slides as $v):
							$img_id = $v['kedrm_main_slider_image'];
							$alt = get_post_meta($img_id, '_wp_attachment_image_alt', true);
						?>

						<div class="main-slider__min-image-wrapper swiper-slide">
							<img src="<?php echo wp_get_attachment_image_url($img_id, 'banner-thumb'); ?>" alt="<?=$alt;?>" class="main-slider__min-image">
						</div>

						<?php endforeach;?>
					</div>
					<?php endif;?>
				</div><!-- .swiper-container -->

				<div class="main-slider__slide-content"></div>

				<div class="main-slider__controls-wrapper">

					<div class="swiper-pagination-bullets"></div>

					<div class="main-slider__min-arrows-wrapper">
						<button class="slider-arrow slider-arrow--prev" aria-label="Previous"
							type="button">
							<svg width="32" height="16" viewBox="0 0 32 16" fill="none"
								xmlns="http://www.w3.org/2000/svg">
								<path
									d="M8.9428 0.39052C8.42214 -0.130173 7.57787 -0.130173 7.0572 0.39052L0.390537 7.0572C0.140537 7.3072 3.8147e-06 7.6464 3.8147e-06 8C3.8147e-06 8.3536 0.140537 8.6928 0.390537 8.9428L7.0572 15.6095C7.57787 16.1301 8.42214 16.1301 8.9428 15.6095C9.46347 15.0888 9.46347 14.2445 8.9428 13.7239L4.55227 9.33333H30.6667C31.403 9.33333 32 8.7364 32 8C32 7.2636 31.403 6.66667 30.6667 6.66667H4.55227L8.9428 2.27615C9.46347 1.75544 9.46347 0.911227 8.9428 0.39052Z"
									fill="#212121" />
							</svg>
						</button>
						<button class="slider-arrow slider-arrow--next" aria-label="Next"
							type="button">
							<svg width="32" height="16" viewBox="0 0 32 16" fill="none"
								xmlns="http://www.w3.org/2000/svg">
								<path
									d="M23.0572 0.39052C23.5779 -0.130173 24.4221 -0.130173 24.9428 0.39052L31.6095 7.0572C31.8595 7.3072 32 7.6464 32 8C32 8.3536 31.8595 8.6928 31.6095 8.9428L24.9428 15.6095C24.4221 16.1301 23.5779 16.1301 23.0572 15.6095C22.5365 15.0888 22.5365 14.2445 23.0572 13.7239L27.4477 9.33333H1.33333C0.59696 9.33333 0 8.7364 0 8C0 7.2636 0.59696 6.66667 1.33333 6.66667H27.4477L23.0572 2.27615C22.5365 1.75544 22.5365 0.911227 23.0572 0.39052Z"
									fill="#212121" />
							</svg>
						</button>
					</div>

				</div>

			</div>


		</div>
	</div>
</section>

<?php

get_footer();
