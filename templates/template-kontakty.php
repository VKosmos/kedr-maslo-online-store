<?php
/**
 * The Contacts page template
 *
 * Template Name: Контакты
 *
 * @package KedrMaslo
 */

get_header(null, ['contacts']);
?>

<section class="contacts">
	<div class="container">
		<?php kedrm_breadcrumbs(); ?>

		<h1 class="contacts__title"><?php the_title(); ?></h1>

		<div class="contacts__flex-wrapper">
			<div class="contacts__flex-element">
				<div class="contacts__image-wrapper">
					<img src="<?= get_template_directory_uri() . '/assets/img/icon/telega-red.svg'?>" alt="" class="contacts__image">
				</div>
				<div class="contacts__icon-wrapper">

					<?php
						$vk = carbon_get_theme_option('kedrm_vk');
						if ($vk): ?>
							<a href="<?php echo $vk ?>">
								<img src="<?= get_template_directory_uri() . '/assets/img/icon/vk-red.svg'?>" alt="" class="contacts__icon">
							</a>
						<?php endif;
						$yt = carbon_get_theme_option('kedrm_youtube');
						if ($yt): ?>
							<a href="<?php echo $yt ?>">
								<img src="<?= get_template_directory_uri() . '/assets/img/icon/youtube-red.svg'?>" alt="" 		class="contacts__icon">
							</a>
						<?php endif; ?>
				</div>

				<?php
					$email = carbon_get_theme_option('kedrm_email');
					if ($email):?>
						<a href="mailto:<?php echo $email ?>" class="contacts__mail"><?php echo $email ?></a>
				<?php	endif;
				?>

				<p class="contacts__text">Напишите нам</p>
			</div>

			<div class="contacts__flex-element">
				<div class="contacts__image-wrapper">
					<img src="<?= get_template_directory_uri() . '/assets/img/icon/call-line-red.svg'?>" alt="" class="contacts__image">
				</div>
				<div class="contacts__phone-wrapper">
					<a href="tel:<?php echo carbon_get_theme_option('kedrm_phonedigits'); ?>" class="contacts__phone"><?php echo carbon_get_theme_option('kedrm_phone'); ?></a>
					<a href="tel:<?php echo carbon_get_theme_option('kedrm_mobiledigits'); ?>" class="contacts__phone"><?php echo carbon_get_theme_option('kedrm_mobile'); ?></a>
				</div>
				<p class="contacts__text contacts__text--time-one">Горячая линия работает  <?php echo carbon_get_theme_option('kedrm_worktime'); ?></p>
			</div>

			<div class="contacts__flex-element">
				<div class="contacts__image-wrapper">
					<img src="<?= get_template_directory_uri() . '/assets/img/icon/map-line-red.svg'?>" alt="" class="contacts__image">
				</div>
				<div class="contacts__text-wrapper">
					<p class="contacts__adress">г. Екатеринбург, пр-т Космонавтов, д. 20, здание "Часовой Молл",
						3 этаж</p>
					<p class="contacts__text contacts__text--metro">Метро Машиностроителей, выход в сторону завода
						имени "Калинина"
						(ЗиК).</p>
				</div>
				<p class="contacts__text contacts__text--time">График работы <?php echo carbon_get_theme_option('kedrm_worktime'); ?></p>
			</div>
		</div>

		<div class="contacts__flex-block">
			<div class="contacts__element">
				<div class="contacts__connection connection">
					<h2 class="connection__title">Написать нам</h2>

					<?php
						echo do_shortcode( '[wpforms id="106" title="false"]' );
					?>

				</div>
			</div>

			<div id="map" class="contacts__map-wrapper"></div>
		</div>
	</div>
</section>

<?php

get_footer();
