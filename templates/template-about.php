<?php
/**
 * The Contacts page template
 *
 * Template Name: О компании
 *
 * @package KedrMaslo
 */

get_header(null, ['page-about-us']);
?>

<div class="container">
			<section class="about-us">

				<?php kedrm_breadcrumbs(); ?>

				<div class="about-us__wrapper">
					<h1 class="about-us__title">О компании</h1>
					<p class="about-us__headline">Добро пожаловать на официальный сайт компании “ЛадоЯр”</p>
					<div class="about-us__content-wrap">

						<div class="about-us__bg" style="background-image: url(<?php echo wp_get_attachment_image_url(carbon_get_post_meta(get_the_ID(),'kedrm_about_banner_bottom'), 'about-banner'); ?>)"></div>

						<div class="about-us__text-wrapper idea-about">
							<?php echo carbon_get_post_meta(get_the_ID(), 'kedrm_about_idea');?>
						</div>
					</div>
				</div>
			</section>

			<section class="about-us__slider main-slider">
				<div class="main-slider__flex-wrapper">
					<div class="main-slider__big-slider-wrapper about-us-slider-big swiper-container">

						<?php
							$slider = carbon_get_post_meta(get_the_ID(), 'kedrm_about_slider');
							if ($slider):
								$i = 0;
							?>
								<div class="main-slider__big big-slider swiper-wrapper">
								<?php foreach($slider as $v): ?>
									<div class="big-slider__image-wrapper swiper-slide" data-slide-index="<?=$i;?>">
										<img src="<?php echo wp_get_attachment_image_url($v['kedrm_about_slider_image'], 'banner-image'); ?>" alt="" class="big-slider__image">

										<div class="big-slider__body">
											<h3><?php echo $v['kedrm_about_slider_title'];?></h3>

											<?php
												if ($v['kedrm_about_slider_text']):?>
													<ul class="big-slider__list">
													<?php foreach($v['kedrm_about_slider_text'] as $w):?>
														<li class="big-slider__item">
															<p><?php echo $w['kedrm_about_slider_text_item'];?></p>
														</li>
													<?php endforeach; ?>
													</ul>
												<?php endif; ?>
										</div>
									</div>
								<?php
									$i++;
								endforeach; ?>
								</div>
							<?php endif; ?>
					</div>

					<div class="main-slider__content-wrapper">

						<div class="main-slider__min-slider-wrapper about-us-slider-min swiper-container">
							<?php
								if ($slider):?>
									<div class="main-slider__min swiper-wrapper">
									<?php foreach($slider as $v):
										$img_id = $v['kedrm_about_slider_image'];
										$alt = get_post_meta($img_id, '_wp_attachment_image_alt', true);
									?>
										<div class="main-slider__min-image-wrapper swiper-slide">
											<img src="<?php echo wp_get_attachment_image_url($img_id, 'banner-thumb'); ?>" alt="<?=$alt;?>" class="main-slider__min-image">
										</div>
									<?php endforeach;?>
									</div>
								<?php endif;?>
						</div>

						<div class="main-slider__slide-content"></div>

						<div class="main-slider__controls-wrapper">
							<div class="swiper-pagination-bullets"></div>

							<div class="main-slider__min-arrows-wrapper">
								<button class="slider-arrow slider-arrow--prev" aria-label="Previous"
									type="button">
									<svg width="32" height="16" viewBox="0 0 32 16" fill="none"
										xmlns="http://www.w3.org/2000/svg">
										<path
											d="M8.9428 0.39052C8.42214 -0.130173 7.57787 -0.130173 7.0572 0.39052L0.390537 7.0572C0.140537 7.3072 3.8147e-06 7.6464 3.8147e-06 8C3.8147e-06 8.3536 0.140537 8.6928 0.390537 8.9428L7.0572 15.6095C7.57787 16.1301 8.42214 16.1301 8.9428 15.6095C9.46347 15.0888 9.46347 14.2445 8.9428 13.7239L4.55227 9.33333H30.6667C31.403 9.33333 32 8.7364 32 8C32 7.2636 31.403 6.66667 30.6667 6.66667H4.55227L8.9428 2.27615C9.46347 1.75544 9.46347 0.911227 8.9428 0.39052Z"
											fill="#212121" />
									</svg>
								</button>
								<button class="slider-arrow slider-arrow--next" aria-label="Next"
									type="button">
									<svg width="32" height="16" viewBox="0 0 32 16" fill="none"
										xmlns="http://www.w3.org/2000/svg">
										<path
											d="M23.0572 0.39052C23.5779 -0.130173 24.4221 -0.130173 24.9428 0.39052L31.6095 7.0572C31.8595 7.3072 32 7.6464 32 8C32 8.3536 31.8595 8.6928 31.6095 8.9428L24.9428 15.6095C24.4221 16.1301 23.5779 16.1301 23.0572 15.6095C22.5365 15.0888 22.5365 14.2445 23.0572 13.7239L27.4477 9.33333H1.33333C0.59696 9.33333 0 8.7364 0 8C0 7.2636 0.59696 6.66667 1.33333 6.66667H27.4477L23.0572 2.27615C22.5365 1.75544 22.5365 0.911227 23.0572 0.39052Z"
											fill="#212121" />
									</svg>
								</button>
							</div>

						</div>
					</div>
				</div>
			</section>

			<section class="about-us__product product-about-us">
				<div class="product-about-us__wrapper">
					<p class="product-about-us__text">Чтобы радовать вас качественной продукцией, мы:</p>

					<?php
						$els = carbon_get_post_meta(get_the_ID(), 'kedrm_about_please');
						if ($els):?>
							<div class="product-about-us__wrapper">
						<?php foreach ($els as $v): ?>
							<div class="product-about-us__flex">
								<div class="product-about-us__icon-wrapper">
									<img src="<?php echo wp_get_attachment_image_url($v['kedrm_about_please_image']); ?>" alt="" class="product-about-us__icon">
								</div>
								<p class="product-about-us__text"><?php echo $v['kedrm_about_please_text'];?></p>
							</div>
						<?php endforeach; ?>
							</div>
						<?php endif; ?>

					<div class="product-about-us__bg" style="background-image: url(<?php echo wp_get_attachment_image_url(carbon_get_post_meta(get_the_ID(),'kedrm_about_banner_bottom'), 'about-banner'); ?>)"></div>
				</div>

				<div class="product-about-us__history history">
					<?php echo carbon_get_post_meta(get_the_ID(), 'kedrm_about_history')?>
				</div>
			</section>

			<section class="about-us__requisites requisites">
				<h2 class="requisites__title">
					<?php echo carbon_get_post_meta(get_the_ID(), 'kedrm_about_requisites'); ?>
				</h2>
				<ul class="requisites__list">
					<li class="requisites__item">
						<p>Наши реквизиты <?php echo carbon_get_theme_option('kedrm_company_name') ?>"</p>
					</li>
					<li class="requisites__item">
						<p>ИНН <?php echo carbon_get_theme_option('kedrm_inn') ?></p>
					</li>
					<li class="requisites__item">
						<p>КПП <?php echo carbon_get_theme_option('kedrm_kpp') ?></p>
					</li>
				</ul>
			</section>
		</div>

<?php
get_footer();
