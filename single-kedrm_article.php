<?php
/**
 * The template for displaying one Article post
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package KedrMaslo
 */

get_header(null, ['article']);
?>

	<section class="article">
		<div class="container">
			<?php kedrm_breadcrumbs(); ?>
			<div class="article__wrapper">

				<h1 class="article__title"><?php echo the_title();?></h1>
				<p class="article__headline"><?php echo carbon_get_post_meta(get_the_ID(), 'kedrm_article_headline'); ?></p>

				<div class="article__button-wrapper">
					<?php

						$terms = get_terms( [
							'taxonomy' => 'product_tag',
							'object_ids'  => get_the_ID(),
						] );

						if ($terms):
							foreach($terms as $v):
					?>
						<span class="article__button"><?php echo $v->name;?></span>
					<?php
							endforeach;
						endif;
					?>
				</div>

				<?php
				while ( have_posts() ) :
					the_post();

					the_content();

				endwhile;
				?>
				<div class="line-delimeter">
					<div class="line-delimeter__body">
						<span class="line-delimeter__line"></span>
						<span class="line-delimeter__rectangle line-delimeter__rectangle--one"></span>
						<span class="line-delimeter__rectangle line-delimeter__rectangle--two"></span>
						<span class="line-delimeter__rectangle line-delimeter__rectangle--three"></span>
						<span class="line-delimeter__rectangle line-delimeter__rectangle--four"></span>
					</div>
				</div>

				<div class="blog-product__link-wrapper">
					<p class="blog-product__link">Поделиться:</p>
					<a href="#" class="blog-product__icon-wrapper">
						<img src="<?php echo get_template_directory_uri() . '/assets/img/icon/repost-link.svg'; ?>" alt="" class="blog-product__icon">
					</a>
					<a href="#" class="blog-product__icon-wrapper">
						<img src="<?php echo get_template_directory_uri() . '/assets/img/icon/repost-fb.svg'; ?>" alt="" 	class="blog-product__icon">
					</a>
					<a href="#" class="blog-product__icon-wrapper">
						<img src="<?php echo get_template_directory_uri() . '/assets/img/icon/repost-ok.svg'; ?>" alt="" 	class="blog-product__icon">
					</a>
					<a href="#" class="blog-product__icon-wrapper">
						<img src="<?php echo get_template_directory_uri() . '/assets/img/icon/repost-vk.svg'; ?>" alt="" 	class="blog-product__icon">
					</a>
				</div>
			</div>

		</div>
	</section>

	<?php
		$articles = carbon_get_post_meta(get_the_ID(), 'kedrm_article_articles');

		foreach ($articles as $k => $v) {
			if ('publish' !== get_post_status($v['id'])) {
				unset($articles[$k]);
			}
		}

		if ($articles):

			if (count($articles) > 4):
				$keys = array_rand($articles, 4);
			else:
				$keys = array_keys($articles);
			endif;

			?>
			<section class="article__knowledge knowledge">
				<div class="container">
					<h2 class="knowledge__title">Рекомендуемые статьи</h2>
					<div class="knowledge__slider-container swiper-container knowledge-slider" >
						<ul class="knowledge__list swiper-wrapper">

			<?php foreach ($keys as $v):
				$id = $articles[$v]['id'];
				$img_url = get_the_post_thumbnail_url($id, 'catalog-thumb');
				$img_url = ($img_url) ? $img_url : wp_get_attachment_image_url(carbon_get_theme_option('kedrm_default_catalog_default_thumb'), 'catalog-thumb');
				$alt = get_post_meta($id, '_wp_attachment_image_alt', true);
				?>

				<li class="knowledge__item swiper-slide">
					<article class="knowledge__article article-knowledge">
						<a href="<?php the_permalink($id);?>" class="article-knowledge__link"></a>
						<div class="article-knowledge__image-wrapper">
							<img src="<?php echo $img_url;?>" alt="<?php echo $alt;?>" class="article-knowledge__image">
						</div>
						<h3 class="article-knowledge__title"><?php echo get_the_title($id);?></h3>
						<p class="article-knowledge__text"><?php echo get_the_excerpt($id);?></p>
						<a href="<?php the_permalink($id);?>" class="article-knowledge__link"></a>
					</article>
				</li>

			<?php endforeach; ?>
						</ul>
					<div class="knowledge__slider-buttons-container"></div>
				</div>
			</div><!-- .knowledge__slider-container -->
		</section>

		<?php endif; ?>


<?php

get_footer();
