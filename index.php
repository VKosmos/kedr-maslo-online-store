<?php
/**
 * The main template file
 *
 * @package KedrMaslo
 */

get_header(null, ['main-page']);
?>

<section class="top-bar">
	<div class="container">
		<div class="top-bar__background"></div>

		<div class="top-bar__info">
			<h1 class="top-bar__title">
				<span class="top-bar__red">Натуральная</span>
				продукция<br> для здоровья
			</h1>
			<ul class="top-bar__list">
				<li class="top-bar__item">Укрепление иммунитета</li>
				<li class="top-bar__item">Очищение организма</li>
				<li class="top-bar__item">Красота</li>
			</ul>
			<div class="top-bar__button-flex">
				<a href="#" class="top-bar__show-product">Cмотреть продукцию</a>
				<button class="top-bar__video-play" data-vkpath="main-video" data-animation="fade"
					data-speed="300"></button>
			</div>
		</div>

	</div>
</section>

<section class="advantages">
	<div class="container">
		<h2 class="advantages__title">Наши преимущества</h2>
		<ul class="advantages__list">
			<li class="advantages__item">
				<div class="advantages__image-wrapper">
					<img src="assets/img/icon/fir.svg" alt="" class="advantages__image">
				</div>
				<p class="advantages__text">Полностью натуральный состав продуктов</p>
			</li>
			<li class="advantages__item">
				<div class="advantages__image-wrapper">
					<img src="assets/img/icon/Frame.svg" alt="" class="advantages__image">
				</div>
				<p class="advantages__text">Ручной отбор ореха</p>
			</li>
			<li class="advantages__item">
				<div class="advantages__image-wrapper">
					<img src="assets/img/icon/adventoil.svg" alt="" class="advantages__image">
				</div>
				<p class="advantages__text">Собственное производство</p>
			</li>
			<li class="advantages__item">
				<div class="advantages__image-wrapper">
					<img src="assets/img/icon/familiy.svg" alt="" class="advantages__image">
				</div>
				<p class="advantages__text">Тысячи довольных покупателей</p>
			</li>
		</ul>
		<div class="line-delimeter">
			<div class="line-delimeter__body">
				<span class="line-delimeter__line"></span>
				<span class="line-delimeter__rectangle line-delimeter__rectangle--one"></span>
				<span class="line-delimeter__rectangle line-delimeter__rectangle--two"></span>
				<span class="line-delimeter__rectangle line-delimeter__rectangle--three"></span>
				<span class="line-delimeter__rectangle line-delimeter__rectangle--four"></span>
			</div>
		</div>
	</div>
</section>

<section class="natural">
	<div class="container">
		<h2 class="natural__title">Только натурального продукта <br>
			наивысшего качества</h2>
		<div class="natural__flex-wrapper">
			<p class="natural__text">Главная идея, которой живет и дышит наша команда, – производство
				натурального продукта наивысшего качества. Стремление стать лучшими на рынке экопродукции
				вдохновляет на творчество и воспитывает ответственность перед покупателями! Наши технологии,
				рецепты и теоретические наработки до сих пор служат базой и эталоном для последователей и
				производителей аналогичной продукции.
			</p>
		</div>
		<div class="line-delimeter">
			<div class="line-delimeter__body">
				<span class="line-delimeter__line"></span>
				<span class="line-delimeter__rectangle line-delimeter__rectangle--one"></span>
				<span class="line-delimeter__rectangle line-delimeter__rectangle--two"></span>
				<span class="line-delimeter__rectangle line-delimeter__rectangle--three"></span>
				<span class="line-delimeter__rectangle line-delimeter__rectangle--four"></span>
			</div>
		</div>
	</div>
</section>

<section class="our-products">
	<div class="container">
		<h2 class="our-products__title">Наша продукция</h2>
		<div class="our-products__flex-wrapper our-products-slider swiper-container">
			<ul class="our-products__list swiper-wrapper">

				<li class="our-products__item swiper-slide">
					<div class="our-products__body" style="background-image: url(img/image/our-products/our-products-1-image.jpg)">
						<a href="#" class="our-products__link"></a>
						<h3 class="our-products__text">Кедровая аптека</h3>
					</div>
				</li>

				<li class="our-products__item swiper-slide">
					<div class="our-products__body" style="background-image: url(img/image/our-products/our-products-2-image.jpg)">
						<a href="#" class="our-products__link"></a>
						<p class="our-products__text">Масло холодного отжима</p>
					</div>
				</li>

				<li class="our-products__item swiper-slide">
					<div class="our-products__body" style="background-image: url(img/image/our-products/our-products-3-image.jpg)">
						<a href="#" class="our-products__link"></a>
						<p class="our-products__text">Натуральная косметика</p>
					</div>
				</li>

				<li class="our-products__item swiper-slide">
					<div class="our-products__body" style="background-image: url(img/image/our-products/our-products-4-image.jpg)">
						<a href="#" class="our-products__link"></a>
						<p class="our-products__text">Для ванны, душа и души</p>
					</div>
				</li>

				<li class="our-products__item swiper-slide">
					<div class="our-products__body" style="background-image: url(img/image/our-products/our-products-5-image.jpg)">
						<a href="#" class="our-products__link"></a>
						<p class="our-products__text">Уход за полостью рта</p>
					</div>
				</li>

				<li class="our-products__item swiper-slide">
					<div class="our-products__body" style="background-image: url(img/image/our-products/our-products-6-image.jpg)">
						<a href="#" class="our-products__link"></a>
						<p class="our-products__text">Натуральные моющие средства</p>
					</div>
				</li>

				<li class="our-products__item swiper-slide">
					<div class="our-products__body" style="background-image: url(img/image/our-products/our-products-7-image.jpg)">
						<a href="#" class="our-products__link"></a>
						<p class="our-products__text">Полезное питание</p>
					</div>
				</li>

				<li class="our-products__item swiper-slide">
					<div class="our-products__body" style="background-image: url(img/image/our-products/our-products-8-image.jpg)">
						<a href="#" class="our-products__link"></a>
						<p class="our-products__text">Иван чай и травы</p>
					</div>
				</li>

				<li class="our-products__item swiper-slide">
					<div class="our-products__body" style="background-image: url(img/image/our-products/our-products-9-image.jpg)">
						<a href="#" class="our-products__link"></a>
						<p class="our-products__text">Литература</p>
					</div>
				</li>
			</ul>

			<div class="our-products__slider-buttons-container"></div>
		</div><!-- .our-products__flex-wrapper .swiper-container -->

		<div class="line-delimeter">
			<div class="line-delimeter__body">
				<span class="line-delimeter__line"></span>
				<span class="line-delimeter__rectangle line-delimeter__rectangle--one"></span>
				<span class="line-delimeter__rectangle line-delimeter__rectangle--two"></span>
				<span class="line-delimeter__rectangle line-delimeter__rectangle--three"></span>
				<span class="line-delimeter__rectangle line-delimeter__rectangle--four"></span>
			</div>
		</div>

	</div>
</section>

<section class="catalog">
	<div class="container">
		<h2 class="catalog__title">Популярные товары</h2>

		<div class="catalog__all-container">
			<div class="catalog__switcher-wrapper">

				<button class="catalog__switcher catalog__switcher--horizontal" data-catalog="horizontal">
					<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
						<path fill-rule="evenodd" clip-rule="evenodd"
							d="M20 3C20.5523 3 21 3.44772 21 4L21 10C21 10.5523 20.5523 11 20 11L4 11C3.44772 11 3 10.5523 3 10L3 4C3 3.44771 3.44772 3 4 3L20 3ZM19 5L5 5L5 9L19 9L19 5Z"
							fill="#C4C4C4" />
						<path fill-rule="evenodd" clip-rule="evenodd"
							d="M20 13C20.5523 13 21 13.4477 21 14L21 20C21 20.5523 20.5523 21 20 21L4 21C3.44772 21 3 20.5523 3 20L3 14C3 13.4477 3.44772 13 4 13L20 13ZM19 15L5 15L5 19L19 19L19 15Z"
							fill="#C4C4C4" />
					</svg>
				</button>

				<button class="catalog__switcher catalog__switcher--tile js-active" data-catalog="vertical">
					<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
						<path fill-rule="evenodd" clip-rule="evenodd"
							d="M3 4C3 3.44772 3.44772 3 4 3L10 3C10.5523 3 11 3.44772 11 4L11 20C11 20.5523 10.5523 21 10 21H4C3.44772 21 3 20.5523 3 20L3 4ZM5 5L5 19H9L9 5L5 5Z"
							fill="#C4C4C4" />
						<path fill-rule="evenodd" clip-rule="evenodd"
							d="M13 4C13 3.44772 13.4477 3 14 3L20 3C20.5523 3 21 3.44772 21 4L21 20C21 20.5523 20.5523 21 20 21H14C13.4477 21 13 20.5523 13 20L13 4ZM15 5L15 19H19L19 5L15 5Z"
							fill="#C4C4C4" />
					</svg>
				</button>

			</div>
			<a href="#" class="catalog__all-link">Смотреть всю продукцию</a>
		</div>

		<div class="catalog__catalog-container swiper-container catalog-slider">

			<ul class="catalog__list swiper-wrapper">

				<li class="catalog__item swiper-slide">
					<article class="catalog__product product-body">
						<div class="product-body__image-wrapper">
							<a href="#">
								<img src="assets/img/product_png/catalog_product.png" alt="Кедровый спас масло кедровое 100мл"
									class="product-body__image">
							</a>
						</div>

						<div class="product-body__column">
							<div class="product-body__testimonials testimonials">
								<div class="testimonials__stars-wrapper">
									<span class="testimonials__star"></span>
									<span class="testimonials__star"></span>
									<span class="testimonials__star"></span>
									<span class="testimonials__star testimonials__star--half"></span>
									<span class="testimonials__star testimonials__star--grey"></span>
								</div>
								<span class="testimonials__text">4 отзыва</span>
							</div>

							<h3 class="product-body__title">
								<a href="#">"Кедровый спас"</a>
							</h3>
							<p class="product-body__text">Очищение организма</p>
							<div class="product-body__buy-wrapper">
								<span class="product-body__price">1 200.00 р.</span>
								<button class="product-body__buy">
									<span>Купить</span>
								</button>
							</div>
						</div>

					</article>
				</li>

				<li class="catalog__item swiper-slide">
					<article class="catalog__product product-body">
						<div class="product-body__image-wrapper">
							<a href="#">
								<img src="assets/img/product_png/catalog_product.png" alt="Кедровый спас масло кедровое 100мл"
									class="product-body__image">
							</a>
						</div>

						<div class="product-body__column">
							<div class="product-body__testimonials testimonials">
								<div class="testimonials__stars-wrapper">
									<span class="testimonials__star"></span>
									<span class="testimonials__star"></span>
									<span class="testimonials__star"></span>
									<span class="testimonials__star testimonials__star--half"></span>
									<span class="testimonials__star testimonials__star--grey"></span>
								</div>
								<span class="testimonials__text">4 отзыва</span>
							</div>

							<h3 class="product-body__title">
								<a href="#">"Кедровый спас"</a>
							</h3>
							<p class="product-body__text">Очищение организма</p>
							<div class="product-body__buy-wrapper">
								<span class="product-body__price">1 200.00 р.</span>
								<button class="product-body__buy">
									<span>Купить</span>
								</button>
							</div>
						</div>

					</article>
				</li>

				<li class="catalog__item swiper-slide">
					<article class="catalog__product product-body catalog__product product-body--no">
						<div class="product-body__image-wrapper">
							<a href="#">
								<img src="assets/img/product_png/catalog_product.png" alt="Кедровый спас масло кедровое 100мл"
									class="product-body__image">
							</a>
						</div>
						<div class="product-body__column">
							<div class="product-body__testimonials testimonials">
								<div class="testimonials__stars-wrapper">
									<span class="testimonials__star"></span>
									<span class="testimonials__star"></span>
									<span class="testimonials__star"></span>
									<span class="testimonials__star"></span>
									<span class="testimonials__star"></span>
								</div>
								<span class="testimonials__text">4 отзыва</span>
							</div>
							<h3 class="product-body__title"><a href="#">"Кедровый спас" масло кедровое 100 мл</a></h3>
							<p class="product-body__text">Очищение организма, укрепление иммунитета, для детей, для
								будущих мам, для спортсменов</p>
							<div class="product-body__buy-wrapper">
								<span class="product-body__price">1 200.00 р.</span>
								<button class="product-body__buy">
									<span>Сообщить о появлении</span>
								</button>
							</div>
						</div>
					</article>
				</li>

				<li class="catalog__item swiper-slide">
					<article class="catalog__product product-body catalog__product product-body--no">
						<div class="product-body__image-wrapper">
							<a href="#">
								<img src="assets/img/product_png/catalog_product.png" alt="Кедровый спас масло кедровое 100мл"
									class="product-body__image">
							</a>
						</div>
						<div class="product-body__column">
							<div class="product-body__testimonials testimonials">
								<div class="testimonials__stars-wrapper">
									<span class="testimonials__star"></span>
									<span class="testimonials__star"></span>
									<span class="testimonials__star"></span>
									<span class="testimonials__star"></span>
									<span class="testimonials__star"></span>
								</div>
								<span class="testimonials__text">4 отзыва</span>
							</div>
							<h3 class="product-body__title"><a href="#">"Кедровый спас" масло кедровое 100 мл</a></h3>
							<p class="product-body__text">Очищение организма, укрепление иммунитета, для детей, для
								будущих мам, для спортсменов</p>
							<div class="product-body__buy-wrapper">
								<span class="product-body__price">1 200.00 р.</span>
								<button class="product-body__buy">
									<span>Сообщить о появлении</span>
								</button>
							</div>
						</div>
					</article>
				</li>

				<li class="catalog__item swiper-slide">
					<article class="catalog__product product-body">
						<div class="product-body__image-wrapper">
							<a href="#">
								<img src="assets/img/product_png/catalog_product.png" alt="Кедровый спас масло кедровое 100мл"
									class="product-body__image">
							</a>
						</div>

						<div class="product-body__column">
							<div class="product-body__testimonials testimonials">
								<div class="testimonials__stars-wrapper">
									<span class="testimonials__star"></span>
									<span class="testimonials__star"></span>
									<span class="testimonials__star"></span>
									<span class="testimonials__star testimonials__star--half"></span>
									<span class="testimonials__star testimonials__star--grey"></span>
								</div>
								<span class="testimonials__text">4 отзыва</span>
							</div>

							<h3 class="product-body__title">
								<a href="#">"Кедровый спас" масло кедровое 100 мл</a>
							</h3>
							<p class="product-body__text">Очищение организма</p>
							<div class="product-body__buy-wrapper">
								<span class="product-body__price">1 200.00 р.</span>
								<button class="product-body__buy">
									<span>Купить</span>
								</button>
							</div>
						</div>

					</article>
				</li>

				<li class="catalog__item swiper-slide">
					<article class="catalog__product product-body">
						<div class="product-body__image-wrapper">
							<a href="#">
								<img src="assets/img/product_png/catalog_product.png" alt="Кедровый спас масло кедровое 100мл"
									class="product-body__image">
							</a>
						</div>

						<div class="product-body__column">
							<div class="product-body__testimonials testimonials">
								<div class="testimonials__stars-wrapper">
									<span class="testimonials__star"></span>
									<span class="testimonials__star"></span>
									<span class="testimonials__star"></span>
									<span class="testimonials__star testimonials__star--half"></span>
									<span class="testimonials__star testimonials__star--grey"></span>
								</div>
								<span class="testimonials__text">4 отзыва</span>
							</div>

							<h3 class="product-body__title">
								<a href="#">"Кедровый спас" масло кедровое 100 мл</a>
							</h3>
							<p class="product-body__text">Очищение организма</p>
							<div class="product-body__buy-wrapper">
								<span class="product-body__price">1 200.00 р.</span>
								<button class="product-body__buy">
									<span>Купить</span>
								</button>
							</div>
						</div>

					</article>
				</li>

				<li class="catalog__item swiper-slide">
					<article class="catalog__product product-body catalog__product product-body--no">
						<div class="product-body__image-wrapper">
							<a href="#">
								<img src="assets/img/product_png/catalog_product.png" alt="Кедровый спас масло кедровое 100мл"
									class="product-body__image">
							</a>
						</div>
						<div class="product-body__column">
							<div class="product-body__testimonials testimonials">
								<div class="testimonials__stars-wrapper">
									<span class="testimonials__star"></span>
									<span class="testimonials__star"></span>
									<span class="testimonials__star"></span>
									<span class="testimonials__star"></span>
									<span class="testimonials__star"></span>
								</div>
								<span class="testimonials__text">4 отзыва</span>
							</div>
							<h3 class="product-body__title"><a href="#">"Кедровый спас" масло кедровое 100 мл</a></h3>
							<p class="product-body__text">Очищение организма, укрепление иммунитета, для детей, для
								будущих мам, для спортсменов</p>
							<div class="product-body__buy-wrapper">
								<span class="product-body__price">1 200.00 р.</span>
								<button class="product-body__buy">
									<span>Сообщить о появлении</span>
								</button>
							</div>
						</div>
					</article>
				</li>

				<li class="catalog__item swiper-slide">
					<article class="catalog__product product-body">
						<div class="product-body__image-wrapper">
							<a href="#">
								<img src="assets/img/product_png/catalog_product.png" alt="Кедровый спас масло кедровое 100мл"
									class="product-body__image">
							</a>
						</div>

						<div class="product-body__column">
							<div class="product-body__testimonials testimonials">
								<div class="testimonials__stars-wrapper">
									<span class="testimonials__star"></span>
									<span class="testimonials__star"></span>
									<span class="testimonials__star"></span>
									<span class="testimonials__star testimonials__star--half"></span>
									<span class="testimonials__star testimonials__star--grey"></span>
								</div>
								<span class="testimonials__text">4 отзыва</span>
							</div>

							<h3 class="product-body__title">
								<a href="#">"Кедровый спас" масло кедровое 100 мл</a>
							</h3>
							<p class="product-body__text">Очищение организма</p>
							<div class="product-body__buy-wrapper">
								<span class="product-body__price">1 200.00 р.</span>
								<button class="product-body__buy">
									<span>Купить</span>
								</button>
							</div>
						</div>

					</article>
				</li>

			</ul>

			<div class="catalog__slider-buttons-container"></div>
		</div><!-- .catalog__catalog-container -->

		<div class="line-delimeter">
			<div class="line-delimeter__body">
				<span class="line-delimeter__line"></span>
				<span class="line-delimeter__rectangle line-delimeter__rectangle--one"></span>
				<span class="line-delimeter__rectangle line-delimeter__rectangle--two"></span>
				<span class="line-delimeter__rectangle line-delimeter__rectangle--three"></span>
				<span class="line-delimeter__rectangle line-delimeter__rectangle--four"></span>
			</div>
		</div>
	</div>
</section>

<section class="knowledge">
	<div class="container">

		<h2 class="knowledge__title">База знаний</h2>

		<div class="knowledge__all-container">
			<a href="#" class="knowledge__all-link">Смотреть все статьи</a>
		</div>

		<div class="knowledge__slider-container swiper-container knowledge-slider">
			<ul class="knowledge__list swiper-wrapper">
				<li class="knowledge__item swiper-slide">
					<article class="knowledge__article article-knowledge">
						<a href="#" class="article-knowledge__link"></a>
						<div class="article-knowledge__image-wrapper">
							<img src="assets/img/image/Rectangle_19.jpg" alt="" class="article-knowledge__image">
						</div>
						<h3 class="article-knowledge__title">1 Масло кедровое “Кедровый спас”</h3>
						<p class="article-knowledge__text">
							1 Lorem ipsum dolor sit amet consectetur
						</p>
						<a href="#" class="article-knowledge__link"></a>
					</article>
				</li>
				<li class="knowledge__item swiper-slide">
					<article class="knowledge__article article-knowledge">
						<a href="#" class="article-knowledge__link"></a>
						<div class="article-knowledge__image-wrapper">
							<img src="assets/img/image/Rectangle_19.jpg" alt="" class="article-knowledge__image">
						</div>
						<h3 class="article-knowledge__title">2 Масло кедровое “Кедровый спас ”Масло кедровое “Кедровый
							спас”</h3>
						<p class="article-knowledge__text">
							2 Lorem ipsum dolor sit amet consectetur adipisicing elit. Sit saepe rerum, numquam est a
							dolores, enim, accusamus architecto hic magnam repellat minus. Nam voluptate ea maxime atque
							sint quia quibusdam corrupti, cum quaerat explicabo. Laudantium, aliquid earum! Laudantium,
							temporibus beatae.
						</p>
					</article>
				</li>
				<li class="knowledge__item swiper-slide">
					<article class="knowledge__article article-knowledge">
						<a href="#" class="article-knowledge__link"></a>
						<div class="article-knowledge__image-wrapper">
							<img src="assets/img/image/Rectangle_19.jpg" alt="" class="article-knowledge__image">
						</div>
						<h3 class="article-knowledge__title">3 Масло кедровое “Кедровый спас ”Масло кедровое “Кедровый
							спас”</h3>
						<p class="article-knowledge__text">
							3 Lorem ipsum dolor sit amet consectetur adipisicing elit. Sit saepe rerum, numquam est a
							dolores, enim, accusamus architecto hic magnam repellat minus. Nam voluptate ea maxime atque
							sint quia quibusdam corrupti, cum quaerat explicabo. Laudantium, aliquid earum! Laudantium,
							temporibus beatae.
						</p>
					</article>
				</li>
				<li class="knowledge__item swiper-slide">
					<article class="knowledge__article article-knowledge">
						<a href="#" class="article-knowledge__link"></a>
						<div class="article-knowledge__image-wrapper">
							<img src="assets/img/image/Rectangle_19.jpg" alt="" class="article-knowledge__image">
						</div>
						<h3 class="article-knowledge__title">4 Масло кедровое “Кедровый спас ”Масло кедровое “Кедровый
							спас”</h3>
						<p class="article-knowledge__text">
							4 Lorem ipsum dolor sit amet consectetur adipisicing elit. Sit saepe rerum, numquam est a
							dolores, enim, accusamus architecto hic magnam repellat minus. Nam voluptate ea maxime atque
							sint quia quibusdam corrupti, cum quaerat explicabo. Laudantium, aliquid earum! Laudantium,
							temporibus beatae.
						</p>
					</article>
				</li>
			</ul>

		</div>

		<div class="knowledge__slider-buttons-container"></div>

		<div class="line-delimeter">
			<div class="line-delimeter__body">
				<span class="line-delimeter__line"></span>
				<span class="line-delimeter__rectangle line-delimeter__rectangle--one"></span>
				<span class="line-delimeter__rectangle line-delimeter__rectangle--two"></span>
				<span class="line-delimeter__rectangle line-delimeter__rectangle--three"></span>
				<span class="line-delimeter__rectangle line-delimeter__rectangle--four"></span>
			</div>
		</div>
	</div>
</section>

<section class="main-slider">
	<div class="container">
		<div class="main-slider__flex-wrapper">

			<div class="main-slider__big-slider-wrapper swiper-container main-big-slider">

				<div class="main-slider__big big-slider swiper-wrapper">
					<div class="big-slider__image-wrapper swiper-slide" data-slide-index="0">
						<img src="assets/img/image/slider-big.jpg" alt="" class="big-slider__image">
						<div class="big-slider__body">
							<h3>О нас</h3>
							<div>
								<p>1 Главная идея, которой живет и дышит наша команда, – производство натурального
									продукта наивысшего качества. Стремление стать лучшими на рынке экопродукции
									вдохновляет на творчество и воспитывает ответственность перед покупателями! Наши
									технологии, рецепты и теоретические наработки до сих пор служат базой и эталоном для
									последователей и производителей аналогичной продукции.</p>
							</div>
							<a href="#">Подробнее 1</a>
						</div>
					</div>

					<div class="big-slider__image-wrapper swiper-slide" data-slide-index="1">
						<img src="assets/img/image/Rectangle_19.jpg" alt="" class="big-slider__image">
						<div class="big-slider__body">
							<h3>О них</h3>
							<div>
								<p>Далеко-далеко за словесными горами в стране гласных и согласных живут рыбные тексты. Ее
									мир вдали там, переписывается лучше дорогу продолжил! Lorem он, напоивший деревни
									своего свою наш.</p>
							</div>
							<a href="#">Подробнее 2</a>
						</div>
					</div>

					<div class="big-slider__image-wrapper swiper-slide" data-slide-index="2">
						<img src="assets/img/image/slider-big.jpg" alt="" class="big-slider__image">
						<div class="big-slider__body">
							<h3>Об оно</h3>
							<div>
								<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Numquam nihil, veniam, tempore
									iste praesentium, distinctio autem facere labore ducimus sunt maiores magni fugiat sit?
									Sit fugit accusantium impedit aliquam accusamus quas, consequatur voluptatum? Eligendi,
									fugit?</p>
							</div>
							<a href="#">Подробнее 3</a>
						</div>
					</div>

				</div>
			</div>

			<div class="main-slider__content-wrapper">

				<div thumbsSlider="" class="main-slider__min-slider-wrapper swiper-container main-min-sliderr">
					<div class="swiper-wrapper">
						<div class="main-slider__min-image-wrapper swiper-slide">
							<img src="assets/img/image/slider-min.jpg" alt="" class="main-slider__min-image">
						</div>
						<div class="main-slider__min-image-wrapper swiper-slide">
							<img src="assets/img/image/Rectangle_19.jpg" alt="" class="main-slider__min-image">
						</div>
						<div class="main-slider__min-image-wrapper swiper-slide">
							<img src="assets/img/image/slider-min.jpg" alt="" class="main-slider__min-image">
						</div>
					</div>
				</div>

				<div class="main-slider__slide-content"></div>

				<div class="main-slider__controls-wrapper">

					<div class="swiper-pagination-bullets"></div>

					<div class="main-slider__min-arrows-wrapper">
						<button class="slider-arrow slider-arrow--prev" aria-label="Previous"
							type="button">
							<svg width="32" height="16" viewBox="0 0 32 16" fill="none"
								xmlns="http://www.w3.org/2000/svg">
								<path
									d="M8.9428 0.39052C8.42214 -0.130173 7.57787 -0.130173 7.0572 0.39052L0.390537 7.0572C0.140537 7.3072 3.8147e-06 7.6464 3.8147e-06 8C3.8147e-06 8.3536 0.140537 8.6928 0.390537 8.9428L7.0572 15.6095C7.57787 16.1301 8.42214 16.1301 8.9428 15.6095C9.46347 15.0888 9.46347 14.2445 8.9428 13.7239L4.55227 9.33333H30.6667C31.403 9.33333 32 8.7364 32 8C32 7.2636 31.403 6.66667 30.6667 6.66667H4.55227L8.9428 2.27615C9.46347 1.75544 9.46347 0.911227 8.9428 0.39052Z"
									fill="#212121" />
							</svg>
						</button>
						<button class="slider-arrow slider-arrow--next" aria-label="Next"
							type="button">
							<svg width="32" height="16" viewBox="0 0 32 16" fill="none"
								xmlns="http://www.w3.org/2000/svg">
								<path
									d="M23.0572 0.39052C23.5779 -0.130173 24.4221 -0.130173 24.9428 0.39052L31.6095 7.0572C31.8595 7.3072 32 7.6464 32 8C32 8.3536 31.8595 8.6928 31.6095 8.9428L24.9428 15.6095C24.4221 16.1301 23.5779 16.1301 23.0572 15.6095C22.5365 15.0888 22.5365 14.2445 23.0572 13.7239L27.4477 9.33333H1.33333C0.59696 9.33333 0 8.7364 0 8C0 7.2636 0.59696 6.66667 1.33333 6.66667H27.4477L23.0572 2.27615C22.5365 1.75544 22.5365 0.911227 23.0572 0.39052Z"
									fill="#212121" />
							</svg>
						</button>
					</div>

				</div>

			</div>


		</div>
	</div>
</section>

<?php
// get_sidebar();
get_footer();
