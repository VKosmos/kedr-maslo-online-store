jQuery(document).ready(function ($) {

	let faqLinks = $('._faq-link');
	let faqEls = $('._faq-search');

	$('.questions__search .questions__search-button').on('click', function (ev) {

		ev.preventDefault();

		let searchString = $.trim($(this).siblings('input[name="s"]').val()).toLowerCase();

		if ('' === searchString) {
			faqLinks.parent().show();
			faqEls.show();
			return;
		}

		faqEls.each(function () {

			const trgt = $(this).find('h4').attr('data-target');
			const tempEl = faqLinks.filter('[data-anchor=' + trgt + ']');
			let hide = true;

			$(this).filter(':contains("' + searchString + '")').each(function () {
				hide = false;
			});

			if (hide) {
				tempEl.parent().hide();
				$(this).hide();
			} else {
				tempEl.parent().show();
				$(this).show();
			}

		});
	})

	$('.questions__search .questions__search-close').on('click', function (ev) {
		ev.preventDefault();
		$(this).siblings('input[name="s"]').val('');
		faqLinks.parent().show();
		faqEls.show();
	})

});