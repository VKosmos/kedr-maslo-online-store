jQuery(document).ready(function ($) {

	// Perform AJAX login/register on form submit
	$('form#logform, form#regform').on('submit', function (e) {

		if (!$(this).valid()) return false;

		$('p.status', this).show().text(ajax_auth_object.loadingmessage);
		action = 'ajaxlogin';
		username = $('form#logform #username').val();
		password = $('form#logform #password').val();
		// email = '';
		security = $('form#logform #woocommerce-login-nonce').val();

		if ($(this).attr('id') == 'regform') {
			action = 'ajaxregister';
			username = $('#reg_email').val();
			password = $('#reg_password').val();
			// email = $('#email').val();
			security = $('#woocommerce-register-nonce').val();
		}

		let data = {
			action: action,
			username: username,
			password: password,
			// 'email': email,
			nonce: security,
		};

		ctrl = $(this);
		$.ajax({
			type: 'POST',
			dataType: 'json',
			url: ajax_auth_object.ajaxurl,
			data: data,
			success: function (data) {
				console.log('success');

				// debugger;

				$('p.status', ctrl).text(data.message);

				if (data.loggedin == true) {
					document.location.href = ajax_auth_object.redirecturl;
				}
			},
			error: function () {
				$('p.status', ctrl).text('Возникла непредвиденная ошибка. Попробуйте ещё раз.');
				console.log('error during auth');
			}
		});
		e.preventDefault();
	});

});