jQuery(document).ready(function ($){

	let curPage = null;
	if ($('body').hasClass('page-catalog')) {
		curPage = 'shop';
	} else if ($('body').hasClass('page-knowledge')) {
		curPage = 'articles';
	}

	let buttonShowMoreEl = $('#loadmore a');

	// let categoriesEls = $('.filters__checkbox');
	let categoriesEls = $('.filters__item a');

	let containerEl = $('ul.knowledge__list');
	let containerCatalogEl = $('ul.catalog__list')

	let tagsParentsEls = $('.filters__tag').parent();
	let tagsEls = $('.filters__tag');

	let inputTimer;

	// /**
	//  * Запуск получения данных при выделении категории
	//  */
	// categoriesEls.on('click', function () {

	// 	let temp = Number($(this).attr('data-cat-id'));

	// 	if ($(this).prop('checked')) {
	// 		$('.scroll-list__category-link[data-cat-id=' + temp + ']').addClass('_selected');
	// 		$('.scroll-list__category-link[data-cat-id=' + temp + ']').parent().addClass('_active');
	// 	} else {
	// 		$('.scroll-list__category-link[data-cat-id=' + temp + ']').removeClass('_selected');
	// 		$('.scroll-list__category-link[data-cat-id=' + temp + ']').parent().removeClass('_active');
	// 	}

	// 	clearTimeout(inputTimer);
	// 	switch (curPage) {
	// 		case 'articles':
	// 			inputTimer = setTimeout( requestFilteredArticles, 1000, 1, true );
	// 		break;
	// 		case 'shop':
	// 			inputTimer = setTimeout( requestFilteredProducts, 1000, 1, true );
	// 		break;
	// 	}
	// })

	// /**
	//  *	Selecting categories in mobile filter in product archive page
	//  */
	// $('.scroll-list__category-link').on('click', function () {
	// 	$(this).toggleClass('_selected');
	// 	$(this).parent().toggleClass('_active');

	// 	let temp = Number($(this).attr('data-cat-id'));

	// 	if ($(this).hasClass('_selected')) {
	// 		$('.filters__checkbox[data-cat-id=' + temp + ']').prop('checked', 'checked');
	// 	} else {
	// 		$('.filters__checkbox[data-cat-id=' + temp + ']').removeProp('checked');
	// 	}

	// 	clearTimeout(inputTimer);
	// 	switch (curPage) {
	// 		case 'articles':
	// 			inputTimer = setTimeout( requestFilteredArticles, 1000, 1, true );
	// 		break;
	// 		case 'shop':
	// 			inputTimer = setTimeout( requestFilteredProducts, 1000, 1, true );
	// 		break;
	// 	}

	// })


	// Выделение тегов (указываем, какие учитывать в поиске )
	// Запуск получания данных при выделении тега
	tagsEls.on('click', function() {
		$(this).toggleClass('_selected');

		clearTimeout(inputTimer);
		switch (curPage) {
			case 'articles':
				inputTimer = setTimeout( requestFilteredArticles, 1000, 1, true );
			break;
			case 'shop':
				inputTimer = setTimeout( requestFilteredProducts, 1000, 1, true );
			break;
		}
	})

	// Сброс фразы для фильтрации тегов
	$('.filters__search-close').on('click', function (ev) {
		ev.preventDefault();
		$(this).siblings('input[name="s"]').val('');
		tagsParentsEls.each(function () {
			$(this).show();
		})
	})

	// Фильтрация списка тегов
	$('.filters__search').on('input', function () {

		let searchString = $(this).val();
		if ('' == searchString) {
			tagsParentsEls.each(function () {
				$(this).show();
			})
			return;
		}

		tagsEls.each(function () {
			let hide = true;
			let str = $(this).val();
			if (-1 !== str.indexOf(searchString)) {
				hide = false;
			};

			if (hide) {
				$(this).parent().hide();
			} else {
				$(this).parent().show();
			}
		})
	})


	/**
	 * Show More button
	 */
	buttonShowMoreEl.on('click', function (ev) {
		ev.preventDefault();

		$(this).text('Загружаем...');
		let paged = ($(this).attr('data-paged')) ? $(this).attr('data-paged') : 1;
		paged = parseInt(paged) + 1;

		switch (curPage) {
			case 'articles':
				requestFilteredArticles(paged, false);
			break;
			case 'shop':
				requestFilteredProducts(paged, false);
			break;
		}

	});

	/**
	 * Pagination buttons
	 */
	$(document).on('click', '.page-numbers', function (ev) {
		ev.preventDefault();

		if ($(this).hasClass('dots')) return;

		let address = $(this).attr('href');
		if (!address) return;

		let pagedIndex = address.lastIndexOf('/');
		let pagedLength = address.length - pagedIndex - 1;
		let paged = address.substr(pagedIndex + 1, pagedLength);
		paged = ('' != paged) ? paged : 1;

		switch (curPage) {
			case 'articles':
				requestFilteredArticles(paged, true);
			break;
			case 'shop':
				requestFilteredProducts(paged, true);
			break;
		}
	})

	/**
	 *
	 * @param {num} paged - requested padination page
	 * @param {boolean} is_filter - if true, it replaces the content of list of articles
	 */
	function requestFilteredArticles(paged, is_filter) {

		let maxPages = ( buttonShowMoreEl.attr('data-maxpages')) ? buttonShowMoreEl.attr('data-maxpages') : 1;

		let categoriesIds = [];
		categoriesEls.filter(':checked').each(function () {
			categoriesIds.push($(this).attr('data-cat-id'));
		})
		if (0 == categoriesIds.length) {
			categoriesEls.each(function () {
				categoriesIds.push($(this).attr('data-cat-id'));
			})
		}

		if (categoriesIds.length == categoriesEls.length) {
			categoriesIds = 'all';
		} else {
			categoriesIds = categoriesIds.join(',');
		}

		let tagsIds = [];
		tagsEls.filter('._selected').each(function () {
			tagsIds.push($(this).attr('data-tag-id'));
		})
		if (0 == tagsIds.length) {
			tagsEls.each(function () {
				tagsIds.push($(this).attr('data-tag-id'));
			})
		}

		if (tagsIds.length == tagsEls.length) {
			tagsIds = 'all';
		} else {
			tagsIds = tagsIds.join(',');
		}

		$.getJSON(
			JScontent.root_url + "/wp-json/articles/v1/filter?cats=" + categoriesIds + '&tags=' + tagsIds + '&page=' + paged + '&wpnonce=' + JScontent.nonce,
			function (data) {
				data = JSON.parse(data);
				if (-1 == data) {
					return;
				}

				// debugger

				if (is_filter) {
					// В случае фильтрации
					containerEl.html(data.html);
				} else {
					// В случае Загрузить ещё
					containerEl.append(data.html);
				}

				let paged = parseInt(data.paged);
				let maxPages = parseInt(data.maxpages);

				$('.pagination__pagination-wrapper').html(data.pagination);

				buttonShowMoreEl.attr('data-paged', paged);
				buttonShowMoreEl.attr('data-maxpages', maxPages);
				buttonShowMoreEl.text('Показать ещё статьи');

				if (paged == maxPages) {
					buttonShowMoreEl.addClass('_hidden');
				} else {
					buttonShowMoreEl.removeClass('_hidden');
				}
			}
		);

	}/* requestFilteredArticles */

	/**
	 *
	 * @param {num} paged - requested padination page
	 * @param {boolean} is_filter - if true, it replaces the content of list of products
	 */
	function requestFilteredProducts(paged, is_filter) {

		let maxPages = ( buttonShowMoreEl.attr('data-maxpages')) ? buttonShowMoreEl.attr('data-maxpages') : 1;

		let categoriesIds = [];
		// categoriesEls.filter(':checked').each(function () {
		// 	categoriesIds.push($(this).attr('data-cat-id'));
		// })
		// if (0 == categoriesIds.length) {
		// 	categoriesEls.each(function () {
		// 		categoriesIds.push($(this).attr('data-cat-id'));
		// 	})
		// }
		categoriesEls.filter('._selected').each(function () {
			categoriesIds.push($(this).attr('data-cat-id'));
		})
		if (0 == categoriesIds.length) {
			categoriesEls.each(function () {
				categoriesIds.push($(this).attr('data-cat-id'));
			})
		}

		if (categoriesIds.length == categoriesEls.length) {
			categoriesIds = 'all';
		} else {
			categoriesIds = categoriesIds.join(',');
		}

		let tagsIds = [];
		tagsEls.filter('._selected').each(function () {
			tagsIds.push($(this).attr('data-tag-id'));
		})
		if (0 == tagsIds.length) {
			tagsEls.each(function () {
				tagsIds.push($(this).attr('data-tag-id'));
			})
		}

		if (tagsIds.length == tagsEls.length) {
			tagsIds = 'all';
		} else {
			tagsIds = tagsIds.join(',');
		}

		$.getJSON(
			JScontent.root_url + "/wp-json/catalog/v1/filter?cats=" + categoriesIds + '&tags=' + tagsIds + '&page=' + paged + '&wpnonce=' + JScontent.nonce,
			function (data) {
				data = JSON.parse(data);

				if (-1 == data) {
					return;
				}

				if (is_filter) {
					// В случае фильтрации
					containerCatalogEl.html(data.html);
				} else {
					// В случае Загрузить ещё
					containerCatalogEl.append(data.html);
				}

				let paged = parseInt(data.paged);
				let maxPages = parseInt(data.maxpages);

				$('.pagination__pagination-wrapper').html(data.pagination);

				buttonShowMoreEl.attr('data-paged', paged);
				buttonShowMoreEl.attr('data-maxpages', maxPages);
				buttonShowMoreEl.text('Показать ещё товары');

				if (paged >= maxPages) {
					buttonShowMoreEl.addClass('_hidden');
				} else {
					buttonShowMoreEl.removeClass('_hidden');
				}

				// Корректируем текущее состояние истории браузера
				let base_url = window.location.origin + window.location.pathname;
				history.replaceState('filter', '', base_url + '?tags=' + tagsIds);

			}
		);

	}/* requestFilteredProducts */


});