jQuery(document).ready(function ($) {

	let swiperCatalog = null;
	let swiperKnowledge = null;
	let swiperOurProducts = null;

	/**
	 *	Selecting categories in mobile filter in product archive page
		*/
	$('.scroll-list__item').on('click', function () {
		$(this).toggleClass('_active');
		$(this).find('.scroll-list__category-link').toggleClass('_selected');
	})

	let windowWidth = $(window).width();
	let currentScreen = 'desktop';// desktop, tablet, mobile

	initSwiperSliders();

	$(window).resize(function () {
		initSwiperSliders();
	})

	function initSwiperSliders() {
		windowWidth = $(window).width();

		if (windowWidth < 768) {
			currentScreen = 'mobile';
		} else if (windowWidth < 1024) {
			currentScreen = 'tablet';
		} else {
			currentScreen = 'desktop';
		}

		if ('desktop' !== currentScreen) {
			/**
			 * Swiper slider for Catalog page
			 */
			let swiperCategories = new Swiper(".scroll-list", {
				slidesPerView: 'auto',
				spaceBetween: 10,
				freeMode: true,
				pagination: {
					el: ".scroll-list__pagination",
					clickable: false,
				},
			});

		} else {
			if ('undefined' !== typeof swiperCategories ) {
				swiperCategories.destroy();
			}
		}


		if ( 'mobile' === currentScreen ) {

			$('.catalog__list--horizontal').removeClass('catalog__list--horizontal');

			if (null === swiperCatalog) {
				swiperCatalog = new Swiper(".catalog-slider", {
					slidesPerView: 1,
					spaceBetween: 10,
					pagination: {
						el: ".catalog__slider-buttons-container",
						clickable: true,
					},
				});
			}

			if (null === swiperKnowledge) {
				swiperKnowledge = new Swiper(".knowledge-slider", {
					slidesPerView: 1,
					spaceBetween: 10,
					pagination: {
						el: ".knowledge__slider-buttons-container",
						clickable: true,
					},
				});
			}

			if (null === swiperOurProducts) {
				swiperOurProducts = new Swiper(".our-products-slider", {
					slidesPerView: 2,
					slidesPerColumn: 2,
					slidesPerGroup: 2,
					spaceBetween: 15,
					pagination: {
						el: ".our-products__slider-buttons-container",
						clickable: true,
					},
				});
			}

		} else {

			if (null !== swiperCatalog ) {
				swiperCatalog.destroy();
				swiperCatalog = null;

				$('.catalog-slider').removeAttr('style');
				$('.catalog-slider').find('.swiper-slide').removeAttr('style');
				$('.catalog__slider-buttons-container').html('');
			}

			if (null !== swiperKnowledge) {
				swiperKnowledge.destroy(true, true);
				swiperKnowledge = null;

				$('.knowledge-slider').removeAttr('style');
				$('.knowledge-slider').find('.swiper-slide').removeAttr('style');
				$('.knowledge__slider-buttons-container').html('');
			}

			if (null !== swiperOurProducts) {
				swiperOurProducts.destroy();
				swiperOurProducts = null;

				$('.our-products-slider').removeAttr('style');
				$('.our-products-slider').find('.swiper-slide').removeAttr('style');
				$('.our-products__slider-buttons-container').html('');
			}
		}

	}

});