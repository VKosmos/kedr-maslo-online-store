<?php
/**
 * The template for displaying all Articles posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package KedrMaslo
 */

get_header(null, ['page-knowledge']);
?>

<section class="knowledge-base">
			<div class="container">
			<?php kedrm_breadcrumbs(); ?>

				<h1 class="knowledge-base__title"><?php the_archive_title() ?></h1>

				<div class="knowledge-base__filters filters">
					<h2 class="visually-hidden">Фильтр статей</h2>

					<div class="filters__filter">
						<button type="button" class="filters__selector">
							Выберите категорию
							<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
								<path d="M18 9L12 15L6 9" stroke="#212121" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
							</svg>
						</button>
						<div class="filters__body">
							<ul class="filters__list filters__list--articles">
								<?php
									$args = [
										'taxonomy'		=> 'product_cat',
										'hide_empty' 	=> 'false',
										'exclude'		=> 15,
									];
									$terms = get_terms( $args );
									if ($terms):
										foreach ( $terms as $v ):?>
											<li class="filters__item">
												<label>
													<input type="checkbox" class="filters__checkbox" data-cat-id="<?php echo $v->term_id;?>">
													<div class="filters__check"></div>
													<?php echo $v->name; ?>
												</label>
											</li>
										<?php endforeach;
									endif;
								?>
							</ul>
						</div>
					</div>

					<div class="filters__filter">
						<button type="button" class="filters__selector">
							Выберите теги
							<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
								<path d="M18 9L12 15L6 9" stroke="#212121" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
							</svg>
						</button>
						<div class="filters__body">
							<div class="filters__search-wrapper">
								<input type="text" class="filters__search" placeholder="Поиск по тегам"
								aria-label="Поиск по тегам" name="s">
								<button class="filters__search-close" type="reset"></button>
							</div>

							<ul class="filters__list filters__list--tags">
								<?php
									$args = [
										'taxonomy'		=> 'product_tag',
										'hide_empty'	=> 'false',
									];
									$terms = get_terms($args);
									if ($terms):
										foreach ($terms as $v):?>
											<li class="filters__item">
												<input type="button" class="filters__tag" data-tag-id="<?php echo $v->term_id;?>" value="<?php echo $v->name; ?>"></input>
											</li>
										<?php endforeach;
									endif;
								?>
							</ul>
						</div>
					</div>
				</div>
			</div><!-- .container -->
		</section><!-- .knowledge-base -->

		<section class="knowledge">
			<div class="container">
				<?php if (have_posts()):?>
					<ul class="knowledge__list _no-filter">
					<?php while (have_posts()):
						the_post();

						ob_start();
						get_template_part( 'template-parts/archive-articles-single.tpl' );
						echo ob_get_clean();
					?>

					<?php endwhile;?>
						</ul>
						<div class="knowledge__slider-buttons-container"></div>
				<?php endif; ?>

			</div>
		</section>


		<div class="pagination">
			<div class="container">
				<div class="pagination__flex-container">

					<!-- <button class="pagination__show-more" type="button">Показать еще 12 статей</button> -->
					<?php
						global $wp_query;

						$paged = get_query_var('paged') ? get_query_var('paged') : 1;
						$max_pages = $wp_query->max_num_pages;

						if ($paged < $max_pages):?>

							<div id="loadmore">
								<a href="#" class="pagination__show-more" data-maxpages="<?php echo $max_pages ?>" data-paged="<?php echo $paged; ?>">Показать ещё статьи</a>
							</div>

						<?php endif;?>

					<div class="pagination__pagination-wrapper">
						<?php echo kedrm_archive_article_pagination2($paged, $max_pages); ?>
					</div>

				</div>
			</div>
		</div>


<?php

get_footer();
