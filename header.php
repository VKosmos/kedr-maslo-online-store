<?php
/**
 * The header for our theme
 *
 * @package KedrMaslo
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php (isset($args[0])) ? body_class($args[0]): body_class(); ?>>
<?php wp_body_open(); ?>

	<header class="header fix-block">
		<div class="header__flex-container">

			<?php
				$logo_id = carbon_get_theme_option('kedrm_header_logo_big');
				$logo_mid_id = carbon_get_theme_option('kedrm_header_logo_middle');
				$logo_min_id = carbon_get_theme_option('kedrm_header_logo_little');

				if ($logo_id && $logo_mid_id && $logo_min_id):
					$logo = wp_get_attachment_url( $logo_id );
					$logo_mid = wp_get_attachment_url( $logo_mid_id );
					$logo_min = wp_get_attachment_url( $logo_min_id );

					if (is_front_page() && is_home()):?>
						<div href="<?php echo home_url(); ?>" class="header__logo-link">
							<img class="header__logo header__logo--desktop" src="<?php echo $logo; ?>">
							<img class="header__logo header__logo--desktop-min" src="<?php echo $logo_mid; ?>">
							<img class="header__logo header__logo--mobile" src="<?php echo $logo_min; ?>">
						</div>
					<?php else: ?>
						<a href="<?php echo home_url(); ?>" class="header__logo-link">
							<img class="header__logo header__logo--desktop" src="<?php echo $logo; ?>">
							<img class="header__logo header__logo--desktop-min" src="<?php echo $logo_mid; ?>">
							<img class="header__logo header__logo--mobile" src="<?php echo $logo_min; ?>">
						</a>
					<?php endif;
				endif;
			?>

			<div class="header__catalog-wrapper header__button-modal-wrapper">

				<a class="header__catalog-open" href="<?php echo get_permalink( woocommerce_get_page_id( 'shop' ) );?>">
					<span class="header__catalog-open-text">Каталог</span>
					<span class="header__catalog-open-icon" alt="#"></span>
				</a>

			</div>

			<nav class="header__main-nav main-nav">

				<?php kedrm_header_menu('main'); ?>

				<a href="tel:<?php echo carbon_get_theme_option('kedrm_phonedigits'); ?>" class="main-nav__call">
					<svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
						<path
							d="M12.1109 9.58605C11.6631 9.48972 11.3088 9.69765 10.9951 9.87919C10.6739 10.0663 10.0631 10.5619 9.71301 10.435C7.92041 9.69692 6.23443 8.12792 5.5046 6.32817C5.37597 5.97057 5.86917 5.35586 6.0549 5.03083C6.23512 4.71621 6.43873 4.35861 6.34586 3.90745C6.26194 3.50203 5.17649 2.12085 4.79266 1.74315C4.53952 1.49366 4.2802 1.35645 4.01399 1.33427C3.01314 1.2913 1.89536 2.62675 1.69931 2.94623C1.20817 3.62747 1.21092 4.53394 1.70756 5.63307C2.90446 8.58532 7.43134 13.0408 10.3947 14.2827C10.9415 14.5384 11.4416 14.6666 11.8907 14.6666C12.3303 14.6666 12.7217 14.5439 13.0581 14.3007C13.3119 14.1545 14.7021 12.9812 14.6656 11.9534C14.6436 11.6915 14.5067 11.4295 14.2605 11.1759C13.8856 10.7885 12.5133 9.67059 12.1109 9.58605Z"
							fill="white" />
					</svg>
					<?php echo carbon_get_theme_option('kedrm_phone'); ?>
				</a>

			</nav>

			<div class="header__user-wrapper">
				<div class="header__user-nav">

					<div class="header__button-modal-wrapper">
						<button class="header__catalog-open-mobile" type="button" data-vkpath="catalog" data-vkcontainer="1">
							<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
								<path d="M10 4H4V10H10V4Z" stroke="black" stroke-width="2" stroke-linecap="round"
									stroke-linejoin="round" />
								<path d="M20 4H14V10H20V4Z" stroke="black" stroke-width="2" stroke-linecap="round"
									stroke-linejoin="round" />
								<path d="M20 14H14V20H20V14Z" stroke="black" stroke-width="2" stroke-linecap="round"
									stroke-linejoin="round" />
								<path d="M10 14H4V20H10V14Z" stroke="black" stroke-width="2" stroke-linecap="round"
									stroke-linejoin="round" />
							</svg>
						</button>

					</div>

					<div class="header__button-modal-wrapper">
						<?php
							$data_vkpath = 'cabinet';
							if (is_user_logged_in()) {
								$data_vkpath = 'cart';
							}
						?>

						<a href="#" class="header__user-nav-cab" data-vkpath="<?php echo $data_vkpath; ?>" data-vkcontainer="2">
							<svg width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
								<path fill-rule="evenodd" clip-rule="evenodd"
									d="M5.28769 3.03769C6.27226 2.05312 7.60761 1.5 9 1.5C10.3924 1.5 11.7277 2.05312 12.7123 3.03769C13.6969 4.02226 14.25 5.35761 14.25 6.75C14.25 8.14239 13.6969 9.47775 12.7123 10.4623C11.7277 11.4469 10.3924 12 9 12C7.60761 12 6.27226 11.4469 5.28769 10.4623C4.30312 9.47775 3.75 8.14239 3.75 6.75C3.75 5.35761 4.30312 4.02226 5.28769 3.03769ZM9 10.5C9.99456 10.5 10.9484 10.1049 11.6517 9.40165C12.3549 8.69839 12.75 7.74456 12.75 6.75C12.75 5.75544 12.3549 4.80161 11.6517 4.09835C10.9484 3.39509 9.99456 3 9 3C8.00544 3 7.05161 3.39509 6.34835 4.09835C5.64509 4.80161 5.25 5.75544 5.25 6.75C5.25 7.74456 5.64509 8.69839 6.34835 9.40165C7.05161 10.1049 8.00544 10.5 9 10.5Z"
									fill="white" />
								<path fill-rule="evenodd" clip-rule="evenodd"
									d="M8.99946 13.5C7.76353 13.4989 6.54766 13.8125 5.46639 14.4112L5.46569 14.4115C4.40559 14.997 3.51807 15.8509 2.89217 16.8876C2.67809 17.2422 2.21708 17.3562 1.86248 17.1421C1.50788 16.928 1.39397 16.467 1.60805 16.1124C2.36983 14.8506 3.44998 13.8113 4.74018 13.0987C6.04403 12.3769 7.51013 11.9988 9.00044 12C10.4944 12.0001 11.9637 12.3784 13.2611 13.0992C14.5591 13.8197 15.6402 14.8591 16.3932 16.1141C16.6063 16.4693 16.4912 16.93 16.136 17.1431C15.7808 17.3562 15.3201 17.2411 15.107 16.8859C14.492 15.8609 13.6052 15.0058 12.5331 14.4108L12.5328 14.4106C11.4604 13.8146 10.2419 13.5 9.00011 13.5L8.99946 13.5ZM9.00044 12C9.00055 12 9.00065 12 9.00076 12L9.00011 12.75V12C9.00022 12 9.00033 12 9.00044 12Z"
									fill="white" />
							</svg>
						</a>

						<div class="modal__content modal2__wrapper cabinet-modal" data-vktarget="cabinet">
							<div class="cabinet-modal__content">
								<button class="cabinet-modal__button" type="button" data-vkpath="login">Войти</button>
								<button class="cabinet-modal__button" type="button" data-vkpath="register">Регистрация</button>
							</div>
						</div>

						<div class="modal__content modal2__wrapper cart-modal" data-vktarget="cart">
							<?php
								kedrm_header_menu('user');
							?>
						</div>

					</div>

					<div class="header__button-modal-wrapper">
						<?php
							kedrm_woocommerce_cart_link();
						?>
					</div>

					<div class="header__button-modal-wrapper">
						<button class="header__open-menu" data-vkpath="nav">
							<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
								<path d="M4 5H20" stroke="#212121" stroke-width="2" stroke-linecap="round"
									stroke-linejoin="round" />
								<path d="M4 12H14" stroke="#212121" stroke-width="2" stroke-linecap="round"
									stroke-linejoin="round" />
								<path d="M4 19H18" stroke="#212121" stroke-width="2" stroke-linecap="round"
									stroke-linejoin="round" />
							</svg>
						</button>
					</div>

				</div>
			</div>

		</div>
	</header>

<main>
